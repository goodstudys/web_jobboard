<?php

namespace App\Http\Controllers;

use App\Models\CandidateEducation;
use Illuminate\Http\Request;

class CandidateEducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CandidateEducation  $candidateEducation
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateEducation $candidateEducation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CandidateEducation  $candidateEducation
     * @return \Illuminate\Http\Response
     */
    public function edit(CandidateEducation $candidateEducation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CandidateEducation  $candidateEducation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateEducation $candidateEducation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CandidateEducation  $candidateEducation
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateEducation $candidateEducation)
    {
        //
    }
}
