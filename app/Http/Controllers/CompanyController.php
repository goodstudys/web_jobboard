<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Job;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = User::where('level', 3)->get();

        return view('dashboard.companies.index', ['companies' => $companies]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|max:40000',
            'type' => 'required',
            'description' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);

        $imageName = time() . '.' . request()->image->getClientOriginalExtension();

        request()->image->move(public_path() . '/upload/users/company/', $imageName);

        Company::insert([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'image' => $imageName,
            'type' => $request->type,
            'description' => $request->description,
            'address' => $request->address,
            'phone' => $request->phone,
            'website' => $request->website,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'google' => $request->google,
            'instagram' => $request->instagram,
            'linkedin' => $request->linkedin,
            'rowPointer' => Uuid::uuid4()->getHex(),
            'created_at' => Carbon::now(),
        ]);

        return redirect()->route('profile.show', Auth::user()->id)->with('success', 'Company profile created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = Company::where('user_id', $id)->first();

        $check = Company::where('user_id', $id)->count();
        $jobs = Job::where('user_id', $id)->get();
        $jobCheck = Job::where('user_id', $id)->count();

        return view('company.show', [
            'data' => $data,
            'check' => $check,
            'jobs' => $jobs,
            'jobCheck' => $jobCheck,
        ]);
    }

    public function showProfile($id)
    {
        $company = Company::where('user_id', $id)->first();
        $user = User::where('id', $id)->first();
        if ($company != null) {
            return view('dashboard.companies.show', [
                'company' => $company,
                'user' => $user,
            ]);
        } else {
            return redirect()->route('company.index')->with('error', 'user has not set the profile.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::where('id', $id)->first();

        return view('company.edit', ['company' => $company]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:40000',
            'type' => 'required',
            'description' => 'required',
            'address' => 'required',
            'phone' => 'required',
        ]);

        $companyDt = Company::where('id', $id)->first();
        $company = Company::where('id', $id);

        if ($request->image == null) {
            $imageName = $companyDt->image;
        } else {
            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/company/', $imageName);
        }

        $company->update([
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'image' => $imageName,
            'type' => $request->type,
            'description' => $request->description,
            'address' => $request->address,
            'phone' => $request->phone,
            'website' => $request->website,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'google' => $request->google,
            'instagram' => $request->instagram,
            'linkedin' => $request->linkedin,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('profile.show', Auth::user()->id)->with('success', 'Company profile created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        //
    }
}
