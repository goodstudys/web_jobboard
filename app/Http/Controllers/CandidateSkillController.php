<?php

namespace App\Http\Controllers;

use App\Models\CandidateSkill;
use Illuminate\Http\Request;

class CandidateSkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CandidateSkill  $candidateSkill
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateSkill $candidateSkill)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CandidateSkill  $candidateSkill
     * @return \Illuminate\Http\Response
     */
    public function edit(CandidateSkill $candidateSkill)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CandidateSkill  $candidateSkill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateSkill $candidateSkill)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CandidateSkill  $candidateSkill
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateSkill $candidateSkill)
    {
        //
    }
}
