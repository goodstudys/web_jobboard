<?php

namespace App\Http\Controllers;

use App\Models\PrivacyPolicy;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class PrivacyPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $privacypolices = PrivacyPolicy::get();

        return view('dashboard.privacyPolice.index', ['privacypolices' => $privacypolices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.privacyPolice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required',
            'location' => 'required',
            'phone' => 'required'
        ]);

        PrivacyPolicy::insert([
            'text' => $request->text,
            'location' => $request->location,
            'phone' => $request->phone,
            'created_at' => Carbon::now()
        ]);

        return redirect()->route('privacyPolice.index')->with('success','Privacy Policy Content created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PrivacyPolicy  $privacyPolicy
     * @return \Illuminate\Http\Response
     */
    public function show(PrivacyPolicy $privacyPolicy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PrivacyPolicy  $privacyPolicy
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $privacypolice = PrivacyPolicy::where('id', $id)->first();

        return view('dashboard.privacyPolice.edit', ['privacypolice' => $privacypolice]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PrivacyPolicy  $privacyPolicy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text' => 'required',
            'location' => 'required',
            'phone' => 'required'
        ]);

        $privacyPolicy = PrivacyPolicy::where('id', $id);

        $privacyPolicy->update([
            'text' => $request->text,
            'location' => $request->location,
            'phone' => $request->phone,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('privacyPolice.index')->with('success','Privacy Policy Content updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PrivacyPolicy  $privacyPolicy
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $privacyPolicy = PrivacyPolicy::where('id', $id);

        $privacyPolicy->delete();

        return redirect()->route('privacyPolice.index')->with('success','Privacy Policy Content deleted successfully.');
    }
}
