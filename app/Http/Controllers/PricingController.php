<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Pricing;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;
use App\Models\PricingFeature;

class PricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pricings = Pricing::get();

        return view('dashboard.pricing.index', ['pricings' => $pricings]);
    }

    public function checkout(Request $request)
    {
        $pricing = Pricing::where('rowPointer', $request->orderId)->first();

        $order = Order::where('user_id', Auth::user()->id)
            ->where('status', 'PENDING')
            ->first();

        $user = User::where('id', Auth::user()->id)->first();

        return view('pricing.checkout.index', [
            'pricing' => $pricing,
            'order' => $order,
            'user' => $user
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.pricing.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'balance' => 'required',
            'price' => 'required',
            'features'    => "required|array|min:1",
            'features.*'  => "required|string|distinct|min:1",
        ]);

        $id = Pricing::max('id') + 1;

        Pricing::insert([
            'id' => $id,
            'name' => $request->name,
            'balance' => $request->balance,
            'price' => $request->price,
            'rowPointer' => Uuid::uuid4()->getHex(),
            'created_at' => Carbon::now()
        ]);

        $feeder = [];
        $features = $request->get('features');
        for($i=0;$i<count($features);$i++){
            array_push($feeder, [
                'pricing_id' => $id,
                'name' => $features[$i]
            ]);
        }

        PricingFeature::insert($feeder);

        return redirect()->route('pricing.index')->with('success','Pricing created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function show(Pricing $pricing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function edit(Pricing $pricing)
    {
        $features = PricingFeature::where('pricing_id', $pricing->id)->get();

        return view('dashboard.pricing.edit', [
            'pricing' => $pricing,
            'features' => $features
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pricing $pricing)
    {
        $request->validate([
            'name' => 'required',
            'balance' => 'required',
            'price' => 'required',
            'features'    => "required|array|min:1",
            'features.*'  => "required|string|distinct|min:1",
        ]);

        $pricing->update([
            'name' => $request->name,
            'balance' => $request->balance,
            'price' => $request->price,
            'update_at' => Carbon::now()
        ]);

        PricingFeature::where('pricing_id', $pricing->id)->delete();

        $feeder = [];
        $features = $request->get('features');
        for($i=0;$i<count($features);$i++){
            array_push($feeder, [
                'pricing_id' => $pricing->id,
                'name' => $features[$i]
            ]);
        }

        PricingFeature::insert($feeder);

        return redirect()->route('pricing.index')->with('success','Pricing updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pricing  $pricing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pricing $pricing)
    {
        $pricing->delete();

        PricingFeature::where('pricing_id', $pricing->id)->delete();

        return redirect()->route('pricing.index')->with('success','Pricing deleted successfully.');
    }
}
