<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $aboutus = AboutUs::get();

        return view('dashboard.aboutUs.index', ['aboutus' => $aboutus]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('dashboard.aboutUs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required',
            'video' => 'mimes:mp4,mkv,mov|max:200000',
        ]);

        $videoName = time() . '.' . request()->video->getClientOriginalExtension();

        request()->video->move(public_path() . '/upload/aboutus/', $videoName);

        AboutUs::insert([
            'text' => $request->text,
            'video' => $videoName,
            'rowPointer' => Uuid::uuid4()->getHex(),
            'created_at' => Carbon::now()
        ]);

        return redirect()->route('aboutUs.index')->with('success','About Us Content created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AboutUs  $aboutUs
     * @return \Illuminate\Http\Response
     */
    public function show(AboutUs $aboutUs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AboutUs  $aboutUs
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aboutUs = AboutUs::where('id', $id)->first();

        return view('dashboard.aboutUs.edit', ['aboutus' => $aboutUs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AboutUs  $aboutUs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'text' => 'required',
            'video' => 'mimes:mp4,mkv,mov|max:200000',
        ]);

        $aboutUsDt = AboutUs::where('id', $id)->first();
        $aboutUs = AboutUs::where('id', $id);

        if ($request->video == null) {
            $videoName = $aboutUsDt->video;
        } else {
            $videoName = time() . '.' . request()->video->getClientOriginalExtension();

            request()->video->move(public_path() . '/upload/aboutus/', $videoName);
        }

        $aboutUs->update([
            'text' => $request->text,
            'video' => $videoName,
            'updated_at' => Carbon::now()
        ]);

        return redirect()->route('aboutUs.index')->with('success','About Us Content updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AboutUs  $aboutUs
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $aboutUs = AboutUs::where('id', $id);
        $aboutUs->delete();

        return redirect()->route('aboutUs.index')->with('success','About Us Content deleted successfully.');
    }
}
