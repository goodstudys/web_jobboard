<?php

namespace App\Http\Controllers;

use App\Models\PaymentConfirmation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class PaymentConfirmationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bank' => 'required',
            'cardholders_name' => 'required',
            'total' => 'required',
        ]);

        $id = PaymentConfirmation::max('id') + 1;

        PaymentConfirmation::insert([
            'id' => $id,
            'order_id' => $request->order,
            'bank' => $request->bank,
            'cardholders_name' => $request->cardholders_name,
            'total' => $request->total,
            'rowPointer' => Uuid::uuid4()->getHex(),
            'created_at' => Carbon::now()
        ]);

        return redirect()->back()->with('success','Your confirmation of payment is successful, please wait for confirmation from the admin.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PaymentConfirmation  $paymentConfirmation
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentConfirmation $paymentConfirmation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PaymentConfirmation  $paymentConfirmation
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentConfirmation $paymentConfirmation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PaymentConfirmation  $paymentConfirmation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaymentConfirmation $paymentConfirmation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PaymentConfirmation  $paymentConfirmation
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentConfirmation $paymentConfirmation)
    {
        //
    }
}
