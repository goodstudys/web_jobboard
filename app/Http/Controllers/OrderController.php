<?php

namespace App\Http\Controllers;

use App\Helpers\JobBoard;
use App\Models\Balance;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\PaymentConfirmation;
use App\Models\Pricing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::get();

        return view('dashboard.order.index', [
            'orders' => $orders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bill_country' => 'required',
            'bill_first_name' => 'required',
            'bill_last_name' => 'required',
            'bill_company_name' => 'required',
            'bill_address' => 'required',
            'bill_city' => 'required',
        ]);

        $id = Order::max('id') + 1;

        $date = date('dmY', strtotime(Carbon::now()));

        $code = JobBoard::code('orders', 'order_code', 7, 'JB'.$date);

        Order::insert([
            'id' => $id,
            'user_id' => Auth::user()->id,
            'order_code' => $code,
            'bill_country' => $request->bill_country,
            'bill_first_name' => $request->bill_first_name,
            'bill_last_name' => $request->bill_last_name,
            'bill_company_name' => $request->bill_company_name,
            'bill_address' => $request->bill_address,
            'bill_city' => $request->bill_city,
            'quantity' => 1,
            'subtotal' => $request->subtotal,
            'shipping' => $request->shipping,
            'total' => $request->total,
            'payment' => $request->payment,
            'date_created' => Carbon::now(),
            'status' => 'PENDING',
            'is_balance' => 1,
            'rowPointer' => Uuid::uuid4()->getHex(),
        ]);

        $product = Pricing::where('rowPointer', $request->product)->first();

        OrderDetail::insert([
            'order_id' => $id,
            'product_id' => $product->rowPointer,
            'name' => $product->name,
            'quantity' => 1,
        ]);

        return redirect()->back()->with('success','Order created successfully, please confirm your payment.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::where('id', $id)->first();
        $detail = OrderDetail::where('order_id', $order->id)->first();
        $product = Pricing::where('rowPointer', $detail->product_id)->first();
        $confirmation = PaymentConfirmation::where('order_id', $order->rowPointer)->first();

        return view('dashboard.order.show', [
            'order' => $order,
            'detail' => $detail,
            'product' => $product,
            'confirmation' => $confirmation
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    public function accept($id)
    {
        $order = Order::where('id', $id);
        $orderDt = Order::where('id', $id)->first();
        $detail = OrderDetail::where('order_id', $orderDt->id)->first();
        $product = Pricing::where('rowPointer', $detail->product_id)->first();

        $order->update([
            'status' => 'SUCCESS'
        ]);

        $check = Balance::where('user_id', $orderDt->user_id)->count();
        $balance = Balance::where('user_id', $orderDt->user_id);
        $balanceDt = Balance::where('user_id', $orderDt->user_id)->first();

        if ($check == 0) {
            Balance::insert([
                'user_id' => $orderDt->user_id,
                'balance' => $product->balance
            ]);
        } else {
            $sum = (int)$balanceDt->balance + (int)$product->balance;

            $balance->update([
                'balance' => $sum
            ]);
        }

        return redirect()->route('order.index')->with('success','Order updated successfully');
    }

    public function cancel($id)
    {
        $order = Order::where('id', $id);

        $order->update([
            'status' => 'CANCEL'
        ]);

        return redirect()->route('order.index')->with('success','Order updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
