<?php

namespace App\Http\Controllers;

use App\Category;
use App\Resume;
use App\ResumeEducation;
use App\ResumeExperience;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResumeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::get();
        $resume = Resume::where('user_id', Auth::user()->id)->first();
        $check = Resume::where('user_id', Auth::user()->id)->count();

        if ($resume != null) {
            $educations = ResumeEducation::where('resume_id', $resume->id)->get();
            $experiences = ResumeExperience::where('resume_id', $resume->id)->get();
        } else {
            $educations = null;
            $experiences = null;
        }

        return view('resume.index', [
            'categories' => $categories,
            'resume' => $resume,
            'educations' => $educations,
            'experiences' => $experiences,
            'check' => $check,
        ]);
    }

    public function indexDashboard()
    {
        $resume = Resume::get();
        return view('dashboard.resume.index', [
            'resume' => $resume,
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'profession' => 'required',
            'addressid' => 'required',
            'currentaddress' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|required|max:40000',
            'category' => 'required',
            'contents' => 'required',
            'skills' => 'required',
            'place' => "required|array|min:1",
            'place.*' => "required|string|min:1",
            'program_study' => "required|array|min:1",
            'program_study.*' => "required|string|min:1",
            'degree' => "required|array|min:1",
            'degree.*' => "required|string|min:1",
            'datein' => "required|array|min:1",
            'datein.*' => "required|string|min:1",
            'dateout' => "required|array|min:1",
            'dateout.*' => "required|string|min:1",
            'description' => "required|array|min:1",
            'description.*' => "required|string|min:1",
            'workplace' => "required|array|min:1",
            'workplace.*' => "required|string|min:1",
            'profession1' => "required|array|min:1",
            'profession1.*' => "required|string|min:1",
            'datein1' => "required|array|min:1",
            'datein1.*' => "required|string|min:1",
            'dateout1' => "required|array|min:1",
            'dateout1.*' => "required|string|min:1",
            'description1' => "required|array|min:1",
            'description1.*' => "required|string|min:1",
        ]);

        $imageName = time() . '.' . request()->photo->getClientOriginalExtension();

        request()->photo->move(public_path() . '/upload/users/photo/', $imageName);

        $id = Resume::max('id') + 1;

        Resume::insert([
            'id' => $id,
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'email' => $request->email,
            'profession' => $request->profession,
            'addressID' => $request->addressid,
            'current_address' => $request->currentaddress,
            'photo' => $imageName,
            'video' => $request->video,
            'category_id' => $request->category,
            'content' => $request->contents,
            'skills' => $request->skills,
            'created_at' => Carbon::now(),
        ]);

        $feeder1 = [];
        $place = $request->get('place');
        $program_study = $request->get('program_study');
        $degree = $request->get('degree');
        $datein = $request->get('datein');
        $dateout = $request->get('dateout');
        $description = $request->get('description');
        for ($i = 0; $i < count($place); $i++) {
            array_push($feeder1, [
                'resume_id' => $id,
                'place' => $place[$i],
                'program_study' => $program_study[$i],
                'degree' => $degree[$i],
                'date_start' => $datein[$i],
                'date_end' => $dateout[$i],
                'description' => $description[$i],
            ]);
        }

        ResumeEducation::insert($feeder1);

        $feeder2 = [];
        $workplace = $request->get('workplace');
        $profession1 = $request->get('profession1');
        $datein1 = $request->get('datein1');
        $dateout1 = $request->get('dateout1');
        $description1 = $request->get('description1');
        for ($i = 0; $i < count($workplace); $i++) {
            array_push($feeder2, [
                'resume_id' => $id,
                'work' => $workplace[$i],
                'profession' => $profession1[$i],
                'date_start' => $datein1[$i],
                'date_end' => $dateout1[$i],
                'description' => $description1[$i],
            ]);
        }

        ResumeExperience::insert($feeder2);

        return redirect()->route('resume.index')->with('success', 'Resume created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function show(Resume $resume)
    {
        //
    }
    public function showDashboard($id)
    {
        $resume = Resume::where('id',$id)->first();
        $educations = ResumeEducation::where('resume_id', $resume->id)->get();
        $experiences = ResumeExperience::where('resume_id', $resume->id)->get();
        return view('dashboard.resume.show', [
            'resume' => $resume,
            'educations' => $educations,
            'experiences' => $experiences,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function edit(Resume $resume)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'profession' => 'required',
            'addressid' => 'required',
            'currentaddress' => 'required',
            'photo' => 'mimes:jpeg,jpg,png|max:40000',
            'category' => 'required',
            'contents' => 'required',
            'skills' => 'required',
            'place' => "required|array|min:1",
            'place.*' => "required|string|min:1",
            'program_study' => "required|array|min:1",
            'program_study.*' => "required|string|min:1",
            'degree' => "required|array|min:1",
            'degree.*' => "required|string|min:1",
            'datein' => "required|array|min:1",
            'datein.*' => "required|string|min:1",
            'dateout' => "required|array|min:1",
            'dateout.*' => "required|string|min:1",
            'description' => "required|array|min:1",
            'description.*' => "required|string|min:1",
            'workplace' => "required|array|min:1",
            'workplace.*' => "required|string|min:1",
            'profession1' => "required|array|min:1",
            'profession1.*' => "required|string|min:1",
            'datein1' => "required|array|min:1",
            'datein1.*' => "required|string|min:1",
            'dateout1' => "required|array|min:1",
            'dateout1.*' => "required|string|min:1",
            'description1' => "required|array|min:1",
            'description1.*' => "required|string|min:1",
        ]);

        $resumeDt = Resume::where('id', $id)->first();
        $resume = Resume::where('id', $id);

        if ($request->photo == null) {

            $imageName = $resumeDt->photo;

        } else {

            $imageName = time() . '.' . request()->photo->getClientOriginalExtension();

            request()->photo->move(public_path() . '/upload/users/photo/', $imageName);

        }

        $resume->update([
            'name' => $request->name,
            'email' => $request->email,
            'profession' => $request->profession,
            'addressID' => $request->addressid,
            'current_address' => $request->currentaddress,
            'photo' => $imageName,
            'video' => $request->video,
            'category_id' => $request->category,
            'content' => $request->contents,
            'skills' => $request->skills,
            'updated_at' => Carbon::now(),
        ]);

        ResumeEducation::where('resume_id', $resumeDt->id)->delete();

        $feeder1 = [];
        $place = $request->get('place');
        $program_study = $request->get('program_study');
        $degree = $request->get('degree');
        $datein = $request->get('datein');
        $dateout = $request->get('dateout');
        $description = $request->get('description');
        for ($i = 0; $i < count($place); $i++) {
            array_push($feeder1, [
                'resume_id' => $id,
                'place' => $place[$i],
                'program_study' => $program_study[$i],
                'degree' => $degree[$i],
                'date_start' => $datein[$i],
                'date_end' => $dateout[$i],
                'description' => $description[$i],
            ]);
        }

        ResumeEducation::insert($feeder1);

        ResumeExperience::where('resume_id', $resumeDt->id)->delete();

        $feeder2 = [];
        $workplace = $request->get('workplace');
        $profession1 = $request->get('profession1');
        $datein1 = $request->get('datein1');
        $dateout1 = $request->get('dateout1');
        $description1 = $request->get('description1');
        for ($i = 0; $i < count($workplace); $i++) {
            array_push($feeder2, [
                'resume_id' => $id,
                'work' => $workplace[$i],
                'profession' => $profession1[$i],
                'date_start' => $datein1[$i],
                'date_end' => $dateout1[$i],
                'description' => $description1[$i],
            ]);
        }

        ResumeExperience::insert($feeder2);

        return redirect()->route('resume.index')->with('success', 'Resume updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Resume  $resume
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resume $resume)
    {
        $resume->delete();
        ResumeExperience::where('resume_id', $resume->id)->delete();
        ResumeEducation::where('resume_id',$resume->id)->delete();

        return redirect()->route('resume.indexDashboard')->with('success','Resume deleted successfully.');
    }
}
