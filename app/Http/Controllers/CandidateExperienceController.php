<?php

namespace App\Http\Controllers;

use App\Models\CandidateExperience;
use Illuminate\Http\Request;

class CandidateExperienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CandidateExperience  $candidateExperience
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateExperience $candidateExperience)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CandidateExperience  $candidateExperience
     * @return \Illuminate\Http\Response
     */
    public function edit(CandidateExperience $candidateExperience)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CandidateExperience  $candidateExperience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateExperience $candidateExperience)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CandidateExperience  $candidateExperience
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateExperience $candidateExperience)
    {
        //
    }
}
