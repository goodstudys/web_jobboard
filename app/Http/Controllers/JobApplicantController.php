<?php

namespace App\Http\Controllers;

use App\Models\Candidate;
use App\Models\JobApplicant;
use App\Resume;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $check = JobApplicant::where('user_id', Auth::user()->id)
            ->where('job_id', $request->job)
            ->count();

        $checkResume = Resume::where('user_id', Auth::user()->id)->count();

        $checkProfile = Candidate::where('user_id', Auth::user()->id)->count();

        if ($check == 0) {

            if ($checkResume != 0 && $checkProfile != 0) {

                JobApplicant::insert([
                    'company_id' => $request->company,
                    'job_id' => $request->job,
                    'user_id' => Auth::user()->id,
                    'created_at' => Carbon::now()
                ]);

                return back()->with('success', 'Successfully applied for a job.');

            } elseif ($checkResume == 0 && $checkProfile == 0) {

                return back()->with('error', 'You have not set the profile and resume');

            } elseif ($checkProfile == 0) {

                return back()->with('error', 'You have not set the profile.');

            } elseif ($checkResume == 0) {

                return back()->with('error', 'You have not set the resume.');

            } else {

                return back()->with('error', 'You have not set the profile and resume');

            }
        } else {

            return back()->with('error', 'Already applied for this job.');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function show(JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobApplicant $jobApplicant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobApplicant  $jobApplicant
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobApplicant $jobApplicant)
    {
        //
    }
}
