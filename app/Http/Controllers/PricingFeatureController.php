<?php

namespace App\Http\Controllers;

use App\Models\PricingFeature;
use Illuminate\Http\Request;

class PricingFeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PricingFeature  $pricingFeature
     * @return \Illuminate\Http\Response
     */
    public function show(PricingFeature $pricingFeature)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PricingFeature  $pricingFeature
     * @return \Illuminate\Http\Response
     */
    public function edit(PricingFeature $pricingFeature)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PricingFeature  $pricingFeature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PricingFeature $pricingFeature)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PricingFeature  $pricingFeature
     * @return \Illuminate\Http\Response
     */
    public function destroy(PricingFeature $pricingFeature)
    {
        //
    }
}
