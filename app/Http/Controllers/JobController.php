<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use App\Job;
use App\Models\Balance;
use App\Models\Company;
use App\Models\JobApplicant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = User::join('jobs', 'user_id', 'users.id')->get();

        return view('dashboard.job.index', [
            'jobs' => $jobs
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();
        $balance = Balance::where('user_id', Auth::user()->id)->first();

        return view('job.create', [
            'categories' => $categories,
            'balance' => $balance,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'type' => 'required',
            'title' => 'required',
            'type' => 'required',
            'category_id' => 'required',
            'applicant_email_or_website' => 'required',
            'header_image' => 'mimes:jpeg,jpg,png|max:40000',
            'company_name' => 'required',
            'company_logo' => 'mimes:jpeg,jpg,png|max:40000',
        ]);

        if ($request->header_image == null) {
            $header_image_name = null;
        } else {
            $header_image_name = time() . '.' . request()->header_image->getClientOriginalExtension();

            request()->header_image->move(public_path() . '/upload/users/company/job/header/', $header_image_name);
        }

        if ($request->company_logo == null) {
            $company_logo_name = null;
        } else {
            $company_logo_name = time() . '.' . request()->company_logo->getClientOriginalExtension();

            request()->company_logo->move(public_path() . '/upload/users/company/job/logo/', $company_logo_name);
        }

        $id = Job::max('id') + 1;

        $balanceCheck = Balance::where('user_id', Auth::user()->id)->count();
        $profileCheck = Company::where('user_id', Auth::user()->id)->count();

        if ($balanceCheck == 0) {

            return redirect()->route('job.create')->with('error', 'Your balance is empty');

        } else {

            $balance = Balance::where('user_id', Auth::user()->id);
            $balanceDt = Balance::where('user_id', Auth::user()->id)->first();
            $newBalance = (int) $balanceDt->balance - 10;

            if ($balanceDt->balance == 0) {

                return redirect()->route('job.create')->with('error', 'Your balance is empty');

            } else {

                if ($newBalance < 0) {

                    return redirect()->route('job.create')->with('error', 'Your balance is not enough');

                } else {

                    if ($profileCheck == 0) {

                        return redirect()->route('job.create')->with('error', 'You have not set the profile.');

                    } else {

                        $insert = Job::insert([
                            'id' => $id,
                            'user_id' => Auth::user()->id,
                            'email' => $request->email,
                            'type' => $request->type,
                            'title' => $request->title,
                            'location' => $request->location,
                            'type' => $request->type,
                            'category_id' => $request->category_id,
                            'tags' => $request->tags,
                            'description' => $request->description,
                            'applicant_email_or_website' => $request->applicant_email_or_website,
                            'min_rate' => $request->min_rate,
                            'max_rate' => $request->max_rate,
                            'min_salary' => $request->min_salary,
                            'max_salary' => $request->max_salary,
                            'ext_link' => $request->ext_link,
                            'header_image' => $header_image_name,
                            'company_ext_link' => $request->company_ext_link,
                            'company_name' => $request->company_name,
                            'company_website' => $request->company_website,
                            'company_tagline' => $request->company_tagline,
                            'company_video' => $request->company_video,
                            'company_twitter' => $request->company_twitter,
                            'company_logo' => $company_logo_name,
                            'rowPointer' => Uuid::uuid4()->getHex(),
                            'is_edit' => 0,
                            'created_at' => Carbon::now(),
                            'updated_at' => null,
                        ]);

                        if ($insert) {

                            $balance->update([
                                'balance' => $newBalance,
                            ]);

                        }
                        return redirect()->route('job.create')->with('success', 'Job created successfully.');

                    }

                }
            }
        }
        

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job, $id)
    {
        $jobs = $job->where('id', $id)->first();

        $profile = Company::where('user_id', $jobs->user_id)->first();
        $related = Job::where('category_id', $jobs->category_id)->limit(5)->get();

        return view('job.show', [
            'job' => $jobs,
            'profile' => $profile,
            'related' => $related,
        ]);
    }

    public function listCandidate($id)
    {
        $applicants = JobApplicant::where('job_id', $id)
            ->join('candidates', 'candidates.user_id', 'job_applicants.user_id')
            ->select('job_applicants.id as jobapplId', 'job_applicants.company_id as jobapplcompany',
                'job_applicants.job_id as jobappljob', 'job_applicants.user_id as jobappluser',
                'candidates.id as canddId', 'name', 'expected_salary', 'address', 'profession', 'image')
            ->paginate(5);

        return view('job.applicant.index', [
            'applicants' => $applicants,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job, $id)
    {
        $data = $job->where('id', $id)->first();
        $categories = Category::get();
        $balance = Balance::where('user_id', Auth::user()->id)->first();

        return view('job.edit', [
            'job' => $data,
            'categories' => $categories,
            'balance' => $balance,
        ]);
    }

    public function editDashboard(Job $job, $id)
    {
        $data = $job->where('id', $id)->first();
        $categories = Category::get();
        $balance = Balance::where('user_id', Auth::user()->id)->first();

        return view('dashboard.job.edit', [
            'job' => $data,
            'categories' => $categories,
            'balance' => $balance,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job, $id)
    {
        $data = $job->where('id', $id);

        $jobDt = $job->where('id', $id)->first();

        if ($request->header_image == null) {
            $header_image_name = $jobDt->header_image;
        } else {
            $header_image_name = time() . '.' . request()->header_image->getClientOriginalExtension();

            request()->header_image->move(public_path() . '/upload/users/company/job/header/', $header_image_name);
        }

        if ($request->company_logo == null) {
            $company_logo_name = $jobDt->company_logo;
        } else {
            $company_logo_name = time() . '.' . request()->company_logo->getClientOriginalExtension();

            request()->company_logo->move(public_path() . '/upload/users/company/job/logo/', $company_logo_name);
        }

        $data->update([
            'email' => $request->email,
            'type' => $request->type,
            'title' => $request->title,
            'location' => $request->location,
            'type' => $request->type,
            'category_id' => $request->category_id,
            'tags' => $request->tags,
            'description' => $request->description,
            'applicant_email_or_website' => $request->applicant_email_or_website,
            'min_rate' => $request->min_rate,
            'max_rate' => $request->max_rate,
            'min_salary' => $request->min_salary,
            'max_salary' => $request->max_salary,
            'ext_link' => $request->ext_link,
            'header_image' => $header_image_name,
            'company_ext_link' => $request->company_ext_link,
            'company_name' => $request->company_name,
            'company_website' => $request->company_website,
            'company_tagline' => $request->company_tagline,
            'company_video' => $request->company_video,
            'company_twitter' => $request->company_twitter,
            'company_logo' => $company_logo_name,
            'is_edit' => 0,
            'updated_at' => Carbon::now(),
        ]);

        return redirect()->route('job.show', $id)->with('success', 'Job updated successfully.');
    }

    public function recUpdateJob(Request $request, Job $job, $id)
    {
        $data = $job->where('id', $id);

        $jobDt = $job->where('id', $id)->first();

        $data->update([
            'is_edit' => 1
        ]);

        return redirect()->route('jobs.index')->with('success', 'Suggest Changing successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job, $id)
    {
        $job->where('id', $id)->delete();

        return redirect()->route('profile.show', Auth::user()->id)->with('success', 'Job deleted successfully.');
    }

    public function destroyDashboard(Job $job, $id)
    {
        $job->where('id', $id)->delete();

        return redirect()->route('jobs.index')->with('success', 'Job deleted successfully.');
    }
}
