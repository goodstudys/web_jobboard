<?php

namespace App\Http\Controllers;

use App\Category;
use App\Job;
use App\Models\AboutUs;
use App\Models\Company;
use App\Models\Faq;
use App\Models\Pricing;
use App\Models\PricingFeature;
use App\Models\PrivacyPolicy;
use App\Slider;
use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        $sliders = Slider::get();
        $categories = Category::with('jobCategory')->get();
        $jobs = Job::orderby('id', 'DESC')->limit(5)->get();

        return view('home.index', [
            'sliders' => $sliders,
            'categories' => $categories,
            'jobs' => $jobs,
        ]);
    }

    public function pricing()
    {
        $pricings = Pricing::get();
        $pricingFeature = PricingFeature::get();
        return view('pricing.index', [
            'pricings' => $pricings,
            'pricingFeature' => $pricingFeature,
        ]);
    }
    public function privacypolicy()
    {
        $privacypolicy = PrivacyPolicy::first();
        return view('privacyPolicy.index', [
            'privacypolicy' => $privacypolicy,
        ]);
    }
    public function faq()
    {
        $faq = Faq::get();
        return view('faq.index', [
            'faq' => $faq,
        ]);
    }
    public function companies()
    {
        $companies = Company::with('companyJob')->get();
        return view('company.index', [
            'companies' => $companies,
        ]);
    }
    public function aboutUs()
    {
        $aboutUs = AboutUs::first();
        $faq = Faq::get();
        return view('aboutUs.index', [
            'aboutUs' => $aboutUs,
            'faq' => $faq,
        ]);
    }
    public function searchJob()
    {
        $categories = Category::get();
        $jobs = Job::orderby('id', 'DESC')->paginate(5);
        $jumlah = Job::get();
        $feeder = [];
        return view('searchJob.index', [
            'categories' => $categories,
            'jobs' => $jobs,
            'jumlah' => $jumlah,
            'feeder' => $feeder,
        ]);
    }
    public function searchcat(Request $request)
    {
        $cat = Category::get();
        $getJob = Job::get();
        $feeder = [];
        $isi = [];
        if ($request->fullTime == null && $request->partTime == null && $request->freelance == null && $request->intership == null && $request->temporary == null) {
            array_push($feeder, ['Full Time']);
            array_push($feeder, ['Part Time']);
            array_push($feeder, ['Freelance']);
            array_push($feeder, ['Intership']);
            array_push($feeder, ['Temporary']);
        } else {
            if ($request->fullTime != null) {
                array_push($feeder, ['Full Time']);
                array_push($isi, '1');
            }
            if ($request->partTime != null) {
                array_push($feeder, ['Part Time']);
                array_push($isi, '2');
            }
            if ($request->freelance != null) {
                array_push($feeder, ['Freelance']);
                array_push($isi, '3');
            }
            if ($request->intership != null) {
                array_push($feeder, ['Intership']);
                array_push($isi, '4');
            }
            if ($request->temporary != null) {
                array_push($feeder, ['Temporary']);
                array_push($isi, '5');
            }
        }
        $ab;

        if ($request->searchlocation != null && $request->searchcategories != null && $request->searchkeyword != null) {
            $ab = Job::whereIn('type', $feeder)->where('category_id', $request->searchcategories)->where('location', 'LIKE', "%$request->searchlocation%")->where('title', 'LIKE', "%$request->searchkeyword%");
        } else if ($request->searchlocation != null && $request->searchcategories != null && $request->searchkeyword == null) {
            $ab = Job::whereIn('type', $feeder)->where('category_id', $request->searchcategories)->where('location', 'LIKE', "%$request->searchlocation%");
        } else if ($request->searchlocation != null && $request->searchcategories == null && $request->searchkeyword != null) {
            $ab = Job::whereIn('type', $feeder)->where('location', 'LIKE', "%$request->searchlocation%")->where('title', 'LIKE', "%$request->searchkeyword%");
        } else if ($request->searchlocation == null && $request->searchcategories == null && $request->searchkeyword == null) {
            $ab = Job::whereIn('type', $feeder);
        } else if ($request->searchlocation != null && $request->searchcategories == null && $request->searchkeyword == null) {
            $ab = Job::whereIn('type', $feeder)->where('location', 'LIKE', "%$request->searchlocation%");
        } else if ($request->searchlocation == null && $request->searchcategories != null && $request->searchkeyword == null) {
            $ab = Job::whereIn('type', $feeder)->where('category_id', $request->searchcategories);
        } else if ($request->searchlocation == null && $request->searchcategories == null && $request->searchkeyword != null) {
            $ab = Job::whereIn('type', $feeder)->where('title', 'LIKE', "%$request->searchkeyword%");
        } else if ($request->searchlocation == null && $request->searchcategories != null && $request->searchkeyword != null) {
            $ab = Job::whereIn('type', $feeder)->where('category_id', $request->searchcategories)->where('title', 'LIKE', "%$request->searchkeyword%");
        }

        $testing = $ab->paginate(5);
        $count = $ab->get();
        $fulltime  = false;
        $parttime = false;
        $freelance =false;
        $intership = false;
        $temporary = false;
        for ($i = 0; $i < count($isi); $i++) {
            if ($isi[$i] == '1') {
                $fulltime = true;
            }}
        for ($i = 0; $i < count($isi); $i++) {
            if ($isi[$i] == '2') {
                $parttime = true;
            }}
        for ($i = 0; $i < count($isi); $i++) {
            if ($isi[$i] == '3') {
                $freelance = true;
            }}
        for ($i = 0; $i < count($isi); $i++) {
            if ($isi[$i] == '4') {
                $intership = true;
            }}
        for ($i = 0; $i < count($isi); $i++) {
            if ($isi[$i] == '5') {
                $temporary = true;
            }}
        // foreach ($feeder as $key => $value) {
        //     if ($value == '2') {
        //         $parttime = true;
        //     } else {
        //         $parttime = false;
        //     }
        //     if ($value == '3') {
        //         $freelance = true;
        //     } else {
        //         $freelance = false;
        //     }
        //     if ($value == '4') {
        //         $intership = true;
        //     } else {
        //         $intership = false;
        //     }
        //     if ($value == '5') {
        //         $temporary = true;
        //     } else {
        //         $temporary = false;
        //     }
        // }
        return view('searchJob.index', [
            'categories' => $cat,
            'jobs' => $testing,
            'jumlah' => $count,
            'feeder' => $feeder,
            'fulltime' => $fulltime,
            'parttime' => $parttime,
            'freelance' => $freelance,
            'intership' => $intership,
            'temporary' => $temporary,
        ]);
        //  return \View::make('vacancies.empty')->with('vacancies', $vacancies);
    }
}
