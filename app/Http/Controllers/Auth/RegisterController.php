<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if ($data['level'] == '5' && $data['type'] == 'modal') {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8'],
            ]);
        } elseif ($data['level'] == '5' && $data['type'] == 'nonmodal') {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
        } else {
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
                'username' => ['required', 'string', 'max:255'],
                'companyName' => ['required', 'string', 'max:255'],
                'website' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if ($data['level'] == '5'&& $data['type'] == 'modal') {
            return User::create([
                'name' => $data['name'],
                'level' => $data['level'],
                'email' => $data['email'],
                'isCompany' => 0,
                'password' => Hash::make($data['password']),
                // 'rowPointer' => Uuid::uuid4()->getHex(),
                'created_at' => Carbon::now(),
            ]);
        } elseif ($data['level'] == '5' && $data['type'] == 'nonmodal') {
            return User::create([
                'name' => $data['name'],
                'username' => $data['username'],
                'level' => $data['level'],
                'email' => $data['email'],
                'isCompany' => 0,
                'password' => Hash::make($data['password']),
                // 'rowPointer' => Uuid::uuid4()->getHex(),
                'created_at' => Carbon::now(),
            ]);
        } else {
            return User::create([
                'name' => $data['name'],
                'username' => $data['username'],
                'company' => $data['companyName'],
                'website' => $data['website'],
                'address' => $data['address'],
                'level' => $data['level'],
                'email' => $data['email'],
                'isCompany' => 1,
                'password' => Hash::make($data['password']),
                'created_at' => Carbon::now(),
            ]);
        }

    }
}
