<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Candidate;
use App\Models\CandidateEducation;
use App\Models\CandidateExperience;
use App\Models\CandidateSkill;
use App\Models\CandidateWork;
use App\Resume;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Ramsey\Uuid\Uuid;

class CandidateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $candidates = User::where('level', 5)->get();

        return view('dashboard.candidates.index', ['candidates' => $candidates]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::get();

        return view('candidate.create', [
            'categories' => $categories,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'profession' => 'required',
            'address' => 'required',
            'expected_salary' => 'required',
            'age' => 'required',
            'phone' => 'required',
            'contents' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|max:40000',
            'place' => "required|array|min:1",
            'place.*' => "required|string|min:1",
            'program_study' => "required|array|min:1",
            'program_study.*' => "required|string|min:1",
            'degree' => "required|array|min:1",
            'degree.*' => "required|string|min:1",
            'datein' => "required|array|min:1",
            'datein.*' => "required|string|min:1",
            'dateout' => "required|array|min:1",
            'dateout.*' => "required|string|min:1",
            'description' => "required|array|min:1",
            'description.*' => "required|string|min:1",
            'workplace' => "required|array|min:1",
            'workplace.*' => "required|string|min:1",
            'profession1' => "required|array|min:1",
            'profession1.*' => "required|string|min:1",
            'datein1' => "required|array|min:1",
            'datein1.*' => "required|string|min:1",
            'dateout1' => "required|array|min:1",
            'dateout1.*' => "required|string|min:1",
            'description1' => "required|array|min:1",
            'description1.*' => "required|string|min:1",
            'skillname' => "required|array|min:1",
            'skillname.*' => "required|string|min:1",
            'percentage' => "required|array|min:1",
            'percentage.*' => "required|string|min:1",
            // 'type' => "required|array|min:1",
            // 'type.*' => "required|string|min:1",
        ]);

        $imageName = time() . '.' . request()->image->getClientOriginalExtension();

        request()->image->move(public_path() . '/upload/users/profile/photo/', $imageName);

        $id = Candidate::max('id') + 1;

        Candidate::insert([
            'id' => $id,
            'user_id' => Auth::user()->id,
            'name' => $request->name,
            'email' => $request->email,
            'profession' => $request->profession,
            'address' => $request->address,
            'expected_salary' => $request->expected_salary,
            'age' => $request->age,
            'phone' => $request->phone,
            'image' => $imageName,
            'description' => $request->contents,
            'website' => $request->website,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'linkedin' => $request->linkedin,
            'linkedin' => $request->linkedin,
            'rowPointer' => Uuid::uuid4()->getHex(),
            'created_at' => Carbon::now(),
        ]);

        $feeder1 = [];
        $place = $request->get('place');
        $program_study = $request->get('program_study');
        $degree = $request->get('degree');
        $datein = $request->get('datein');
        $dateout = $request->get('dateout');
        $description = $request->get('description');
        for ($i = 0; $i < count($place); $i++) {
            array_push($feeder1, [
                'candidate_id' => $id,
                'place' => $place[$i],
                'program_study' => $program_study[$i],
                'degree' => $degree[$i],
                'date_start' => $datein[$i],
                'date_end' => $dateout[$i],
                'description' => $description[$i],
            ]);
        }

        CandidateEducation::insert($feeder1);

        $feeder2 = [];
        $workplace = $request->get('workplace');
        $profession1 = $request->get('profession1');
        $datein1 = $request->get('datein1');
        $dateout1 = $request->get('dateout1');
        $description1 = $request->get('description1');
        for ($i = 0; $i < count($workplace); $i++) {
            array_push($feeder2, [
                'candidate_id' => $id,
                'work' => $workplace[$i],
                'profession' => $profession1[$i],
                'date_start' => $datein1[$i],
                'date_end' => $dateout1[$i],
                'description' => $description1[$i],
            ]);
        }

        CandidateExperience::insert($feeder2);

        $feeder3 = [];
        $skillname = $request->get('skillname');
        $percentage = $request->get('percentage');
        for ($i = 0; $i < count($skillname); $i++) {
            array_push($feeder3, [
                'candidate_id' => $id,
                'name' => $skillname[$i],
                'percentage' => $percentage[$i],
            ]);
        }

        CandidateSkill::insert($feeder3);

        $feeder4 = [];
        $type = $request->get('type');

        if ($type != null) {
            for ($i = 0; $i < count($type); $i++) {

                $number = (int) $i;

                $imagecounter = 'imagesss' . (string) $number;

                $imagerequest = $request->$imagecounter;

                $recentwork[$i] = time() . $number . '.' . $imagerequest->getClientOriginalExtension();

                $imagerequest->move(public_path() . '/upload/users/work/', $recentwork[$i]);

                array_push($feeder4, [
                    'candidate_id' => $id,
                    'type' => $type[$i],
                    'image' => $recentwork[$i],
                ]);
            }
        }

        CandidateWork::insert($feeder4);

        return redirect()->route('candidate.profile.show', Auth::user()->id)->with('success','Profile created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $candidate = Candidate::where('user_id', $id)->first();
        $user = User::where('id', $id)->first();
        if ($candidate != null) {
            return view('dashboard.candidates.show', [
                'candidate' => $candidate,
                'user' => $user,
            ]);
        } else {
            return redirect()->route('candidate.index')->with('error', 'user has not set the profile.');
        }

    }

    public function candidateProfile($id)
    {
        $candidate = Candidate::where('user_id', $id)->first();
        $check = Candidate::where('user_id', $id)->count();
        $checkResume = Resume::where('user_id', $id)->count();

        if ($candidate != null) {
            $skills = CandidateSkill::where('candidate_id', $candidate->id)->get();
            $education = CandidateEducation::where('candidate_id', $candidate->id)->get();
            $works = CandidateWork::where('candidate_id', $candidate->id)->get();
            $experiences = CandidateExperience::where('candidate_id', $candidate->id)->get();
        } else {
            $skills = null;
            $education = null;
            $works = null;
            $experiences = null;
        }

        return view('candidate.index', [
            'candidate' => $candidate,
            'check' => $check,
            'checkResume' => $checkResume,
            'skills' => $skills,
            'educations' => $education,
            'works' => $works,
            'experiences' => $experiences,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $candidate = Candidate::where('id', $id)->first();

        $skills = CandidateSkill::where('candidate_id', $candidate->id)->get();
        $education = CandidateEducation::where('candidate_id', $candidate->id)->get();
        $works = CandidateWork::where('candidate_id', $candidate->id)->get();
        $workcounter = CandidateWork::where('candidate_id', $candidate->id)->count();
        $experiences = CandidateExperience::where('candidate_id', $candidate->id)->get();

        return view('candidate.edit', [
            'candidate' => $candidate,
            'skills' => $skills,
            'educations' => $education,
            'works' => $works,
            'workcounter' => $workcounter,
            'experiences' => $experiences
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'profession' => 'required',
            'address' => 'required',
            'expected_salary' => 'required',
            'age' => 'required',
            'phone' => 'required',
            'contents' => 'required',
            'image' => 'mimes:jpeg,jpg,png|max:40000',
            'place'    => "required|array|min:1",
            'place.*'  => "required|string|min:1",
            'program_study'    => "required|array|min:1",
            'program_study.*'  => "required|string|min:1",
            'degree'    => "required|array|min:1",
            'degree.*'  => "required|string|min:1",
            'datein'    => "required|array|min:1",
            'datein.*'  => "required|string|min:1",
            'dateout'    => "required|array|min:1",
            'dateout.*'  => "required|string|min:1",
            'description'    => "required|array|min:1",
            'description.*'  => "required|string|min:1",
            'workplace'    => "required|array|min:1",
            'workplace.*'  => "required|string|min:1",
            'profession1'    => "required|array|min:1",
            'profession1.*'  => "required|string|min:1",
            'datein1'    => "required|array|min:1",
            'datein1.*'  => "required|string|min:1",
            'dateout1'    => "required|array|min:1",
            'dateout1.*'  => "required|string|min:1",
            'description1'    => "required|array|min:1",
            'description1.*'  => "required|string|min:1",
            'skillname'    => "required|array|min:1",
            'skillname.*'  => "required|string|min:1",
            'percentage'    => "required|array|min:1",
            'percentage.*'  => "required|string|min:1",
            // 'type'    => "required|array|min:1",
            // 'type.*'  => "required|string|min:1",
        ]);

        $candidate = Candidate::where('id', $id);
        $candidateDt = Candidate::where('id', $id)->first();

        if ($request->image == null) {

            $imageName = $candidateDt->image;

        } else {

            $imageName = time() . '.' . request()->image->getClientOriginalExtension();

            request()->image->move(public_path() . '/upload/users/profile/photo/', $imageName);
        }

        $candidate->update([
            'name' => $request->name,
            'email' => $request->email,
            'profession' => $request->profession,
            'address' => $request->address,
            'expected_salary' => $request->expected_salary,
            'age' => $request->age,
            'phone' => $request->phone,
            'image' => $imageName,
            'description' => $request->contents,
            'website' => $request->website,
            'facebook' => $request->facebook,
            'twitter' => $request->twitter,
            'instagram' => $request->instagram,
            'linkedin' => $request->linkedin,
            'linkedin' => $request->linkedin,
            'updated_at' => Carbon::now()
        ]);

        CandidateEducation::where('candidate_id', $candidateDt->id)->delete();

        $feeder1 = [];
        $place = $request->get('place');
        $program_study = $request->get('program_study');
        $degree = $request->get('degree');
        $datein = $request->get('datein');
        $dateout = $request->get('dateout');
        $description = $request->get('description');
        for($i=0;$i<count($place);$i++){
            array_push($feeder1, [
                'candidate_id' => $id,
                'place' => $place[$i],
                'program_study' => $program_study[$i],
                'degree' => $degree[$i],
                'date_start' => $datein[$i],
                'date_end' => $dateout[$i],
                'description' => $description[$i],
            ]);
        }

        CandidateEducation::insert($feeder1);

        CandidateExperience::where('candidate_id', $candidateDt->id)->delete();

        $feeder2 = [];
        $workplace = $request->get('workplace');
        $profession1 = $request->get('profession1');
        $datein1 = $request->get('datein1');
        $dateout1 = $request->get('dateout1');
        $description1 = $request->get('description1');
        for($i=0;$i<count($workplace);$i++){
            array_push($feeder2, [
                'candidate_id' => $id,
                'work' => $workplace[$i],
                'profession' => $profession1[$i],
                'date_start' => $datein1[$i],
                'date_end' => $dateout1[$i],
                'description' => $description1[$i],
            ]);
        }

        CandidateExperience::insert($feeder2);

        CandidateSkill::where('candidate_id', $candidateDt->id)->delete();

        $feeder3 = [];
        $skillname = $request->get('skillname');
        $percentage = $request->get('percentage');
        for($i=0;$i<count($skillname);$i++){
            array_push($feeder3, [
                'candidate_id' => $id,
                'name' => $skillname[$i],
                'percentage' => $percentage[$i]
            ]);
        }

        CandidateSkill::insert($feeder3);

        $work = CandidateWork::where('candidate_id', $candidateDt->id)->get();

        $feeder4 = [];
        $type = $request->get('type');

        if ($type != null) {
            for($i=0;$i<count($type);$i++){

                $number= (int)$i;

                $imagecounter = 'imagesss'.(string)$number;

                $imagerequest = $request->$imagecounter;

                if ($imagerequest != null) {
                    $recentwork[$i] = time() . $number . '.' . $imagerequest->getClientOriginalExtension();

                    $imagerequest->move(public_path() . '/upload/users/work/', $recentwork[$i]);
                } else {
                    $recentwork[$i] = $work[$i]->image;
                }

                array_push($feeder4, [
                    'candidate_id' => $id,
                    'type' => $type[$i],
                    'image' => $recentwork[$i]
                ]);
            }
        }

        CandidateWork::where('candidate_id', $candidateDt->id)->delete();

        CandidateWork::insert($feeder4);

        return redirect()->route('candidate.profile.show', Auth::user()->id)->with('success','Profile updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Candidate  $candidate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Candidate $candidate)
    {
        //
    }
}
