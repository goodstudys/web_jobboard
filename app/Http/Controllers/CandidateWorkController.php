<?php

namespace App\Http\Controllers;

use App\Models\CandidateWork;
use Illuminate\Http\Request;

class CandidateWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CandidateWork  $candidateWork
     * @return \Illuminate\Http\Response
     */
    public function show(CandidateWork $candidateWork)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CandidateWork  $candidateWork
     * @return \Illuminate\Http\Response
     */
    public function edit(CandidateWork $candidateWork)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CandidateWork  $candidateWork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CandidateWork $candidateWork)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CandidateWork  $candidateWork
     * @return \Illuminate\Http\Response
     */
    public function destroy(CandidateWork $candidateWork)
    {
        //
    }
}
