<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResumeEducation extends Model
{
    protected $fillable = [
        'resume_id',
        'place',
        'program_study',
        'degree',
        'date_start',
        'date_end',
        'description'
    ];
}
