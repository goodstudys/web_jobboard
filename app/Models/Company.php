<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'type',
        'description',
        'address',
        'website',
        'phone',
        'facebook',
        'twitter',
        'google',
        'instagram',
        'linkedin',
        'rowPointer'
    ];
    public function companyJob() {
        return $this->hasMany('App\Job', 'user_id','user_id');
    }
}
