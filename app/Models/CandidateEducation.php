<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CandidateEducation extends Model
{
    protected $fillable = [
        'candidate_id',
        'place',
        'program_study',
        'deegre',
        'date_start',
        'date_end',
        'description'
    ];
}
