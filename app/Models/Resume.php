<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'email',
        'profession',
        'addressID',
        'current_address',
        'photo',
        'video',
        'category_id',
        'content',
        'skills',
        'created_at',
        'updated_at'
    ];
}
