<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'id',
        'email',
        'type',
        'title',
        'location',
        'type',
        'category_id',
        'tags',
        'description',
        'applicant_email_or_website',
        'min_rate',
        'max_rate',
        'min_salary',
        'max_salary',
        'ext_link',
        'header_image',
        'company_ext_link',
        'company_name',
        'company_website',
        'company_tagline',
        'company_video',
        'company_twitter',
        'company_logo',
        'rowPointer',
        'is_edit',
        'created_at',
        'updated_at',
    ];
}
