<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'order_code',
        'bill_country',
        'bill_first_name',
        'bill_last_name',
        'bill_company_name',
        'bill_address',
        'bill_city',
        'ship_country',
        'ship_first_name',
        'ship_last_name',
        'ship_company_name',
        'ship_address',
        'ship_city',
        'quantity',
        'subtotal',
        'shipping',
        'discount',
        'total',
        'payment',
        'status',
        'date_created',
        'is_balance',
        'rowPointer'
    ];

    public $timestamps = false;
}
