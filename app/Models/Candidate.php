<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'image',
        'type',
        'description',
        'address',
        'website',
        'phone',
        'facebook',
        'twitter',
        'google',
        'instagram',
        'linkedin',
        'rowPointer'
    ];
}
