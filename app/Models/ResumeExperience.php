<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResumeExperience extends Model
{
    protected $fillable = [
        'resume_id',
        'work',
        'profession',
        'description',
        'date_start',
        'date_end'
    ];
}
