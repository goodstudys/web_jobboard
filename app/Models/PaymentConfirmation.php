<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentConfirmation extends Model
{
    protected $fillable = [
        'order_id',
        'bank',
        'cardholders_name',
        'total',
        'rowPointer'
    ];
}
