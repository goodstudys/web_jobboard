<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'id',
        'name',
        'rowPointer'
    ];
    public function jobCategory() {
        return $this->hasMany('App\Job', 'category_id','id');
    }
}
