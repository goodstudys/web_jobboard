<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PricingFeature extends Model
{
    protected $fillable = [
        'pricing_id',
        'name'
    ];

    public $timestamps = false;
}
