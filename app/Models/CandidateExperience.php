<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CandidateExperience extends Model
{
    protected $fillable = [
        'work',
        'profession',
        'description',
        'date_start',
        'date_end'
    ];
}
