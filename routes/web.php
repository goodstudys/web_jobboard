<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'WebController@index')->name('home.index');

Route::get('/search-job', 'WebController@searchJob')->name('searchJob.index');
Route::post('/search-job/search', 'WebController@searchcat')->name('searchcat.index');

Route::get('/find-a-candidate', function () {
    return view('findCandidate.index');
})->name('findCandidate.index');

Route::get('/register', function () {
    return view('auth.register');
})->name('user.reg');

Route::get('/about-us', 'WebController@aboutUs')->name('aboutUsWeb.index');

Route::get('/shop', function () {
    return view('shop.index');
})->name('shop.index');

Route::get('/companies', 'WebController@companies')->name('companies.index');

Route::get('/pricings', 'WebController@pricing')->name('pricings.index');

Route::get('/blog', function () {
    return view('blog.index');
})->name('blog.index');

Route::get('/faq', 'WebController@faq')->name('webFaq.index');

Route::get('/privacy-policy', 'WebController@privacypolicy')->name('privacyPolicy.index');

Route::get('/job/{id}', 'JobController@show')->name('job.show');
Route::get('/company/{id}', 'CompanyController@show')->name('company.show');

Auth::routes();

Route::group(['middleware' => 'auth'], function () {

    Route::get('/checkout', 'CheckoutController@index')->name('checkout.index');

});

// routing manager
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth' => 'dashboard']], function () {

    Route::get('/home', function () {
        return view('dashboard.home.index');
    })->name('dashboard.home.index');

    Route::resource('sliders', 'SliderController');
    Route::resource('category', 'CategoryController');
    Route::get('category/{id}/statusUpdate', 'CategoryController@statusUpdate')->name('category.statusUpdate');
    Route::resource('faq', 'FaqController');
    Route::resource('aboutUs', 'AboutUsController');
    Route::resource('pricing', 'PricingController');
    Route::resource('privacyPolice', 'PrivacyPolicyController');
    Route::resource('resume', 'ResumeController');
    Route::get('/candidate', 'CandidateController@index')->name('candidate.index');
    Route::get('/candidate/{id}/show', 'CandidateController@show')->name('candidate.show');
    Route::get('/company', 'CompanyController@index')->name('company.index');
    Route::get('/company/{id}/showProfile', 'CompanyController@showProfile')->name('company.showProfile');
    Route::get('/order', 'OrderController@index')->name('order.index');
    Route::get('/order/{id}/show', 'OrderController@show')->name('order.show');
    Route::get('/order/{id}/accept', 'OrderController@accept')->name('order.accept');
    Route::get('/order/{id}/cancel', 'OrderController@cancel')->name('order.cancel');

    Route::get('/resume', 'ResumeController@indexDashboard')->name('resume.indexDashboard');
    Route::get('/resume/show/{id}', 'ResumeController@showDashboard')->name('resume.showDashboard');

    Route::get('/jobs', 'JobController@index')->name('jobs.index');
    Route::get('/jobs/{id}/recUpdateJob', 'JobController@recUpdateJob')->name('dashboard.jobs.recUpdateJob');
    Route::post('/jobs/{id}/destroy', 'JobController@destroyDashboard')->name('dashboard.jobs.destroy');

});

// routing company
Route::group(['prefix' => 'company', 'middleware' => ['auth' => 'company']], function () {

//    Route::get('/advertisement/create', 'AdvertController@create')->name('advert.create');

    Route::get('/profile/create', 'CompanyController@create')->name('profile.create');
    Route::post('/profile/store', 'CompanyController@store')->name('profile.store');
    Route::get('/profile/{id}/edit', 'CompanyController@edit')->name('profile.edit');
    Route::post('/profile/{id}/update', 'CompanyController@update')->name('profile.update');
    Route::get('/profile/{id}', 'CompanyController@show')->name('profile.show');

    Route::get('/post-job/create', 'JobController@create')->name('job.create');
    Route::post('/post-job/store', 'JobController@store')->name('job.store');
    Route::get('/job/{id}/edit', 'JobController@edit')->name('job.edit');
    Route::post('/job/{id}/update', 'JobController@update')->name('job.update');
    Route::post('/job/{id}/destroy', 'JobController@destroy')->name('job.destroy');
    Route::get('/job/list-applicant/{id}', 'JobController@listCandidate')->name('job.listCandidate');
    Route::get('applicant/profile/{id}', 'CandidateController@candidateProfile')->name('applicant.profile.show');

    // Route::get('/post_job', function () {
    //     return view('job.create');
    // })->name('job.create');

    Route::get('/pricings/checkout', 'PricingController@checkout')->name('pricing.checkout');
    Route::post('/pricings/checkout/store', 'OrderController@store')->name('order.pricingStore');
    Route::post('/pricings/paymentconfirmation/store', 'PaymentConfirmationController@store')->name('paymentconfirm.store');

});

// routing trainer
Route::group(['prefix' => 'trainer', 'middleware' => ['auth' => 'trainer']], function () {

});

// routing user
Route::group(['prefix' => 'user', 'middleware' => ['auth' => 'user']], function () {

    Route::get('/profile/create', 'CandidateController@create')->name('candidate.profile.create');
    Route::post('/profile/store', 'CandidateController@store')->name('candidate.profile.store');
    Route::get('/profile/{id}', 'CandidateController@candidateProfile')->name('candidate.profile.show');
    Route::get('/profile/{id}/edit', 'CandidateController@edit')->name('candidate.profile.edit');
    Route::post('/profile/{id}/update', 'CandidateController@update')->name('candidate.profile.update');

    Route::get('/resume', 'ResumeController@index')->name('resume.index');
    Route::post('/resume/store', 'ResumeController@store')->name('resume.store');
    Route::post('/resume/{id}/update', 'ResumeController@update')->name('resume.update');

    Route::post('/apply/store', 'JobApplicantController@store')->name('apply.store');

});
