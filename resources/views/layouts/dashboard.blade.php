<!DOCTYPE html>
<html lang="en">

@include('partials.dashboard._head')

<body>
<div id="app">
    <div class="main-wrapper">

        @include('partials.dashboard._navbar')

        @include('partials.dashboard._sidebar')

        <!-- Main Content -->
        @yield('content')

        @include('partials.dashboard._footer')
    </div>
</div>

@include('partials.dashboard._script')
</body>
</html>
