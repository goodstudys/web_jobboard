<!DOCTYPE html>

<html lang="en">

@include('partials._head')

<body>

<!-- =============== Start of Header 1 Navigation =============== -->
@include('partials._header')
<!-- =============== End of Header 1 Navigation =============== -->





@yield('content')





<!-- =============== Start of Footer 1 =============== -->
@include('partials._footer')
<!-- =============== End of Footer 1 =============== -->





<!-- ===== Start of Back to Top Button ===== -->
<a href="#" class="back-top"><i class="fa fa-chevron-up"></i></a>
<!-- ===== End of Back to Top Button ===== -->





<!-- ===== Start of Login Pop Up div ===== -->
@include('partials._loginModal')
<!-- ===== End of Login Pop Up div ===== -->





<!-- ===== All Javascript at the bottom of the page for faster page loading ===== -->
@include('partials._script')

</body>

</html>
