@extends('layouts.main')
@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>faq</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">pages</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of FAQ Section ===== -->
<section class="ptb80" id="faq-page">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Start of First Row -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3 class="uppercase">Frequently Asked Questions</h3>
                        <input type="text" class="live-search-box form-control mt20"
                            placeholder="Search your Questions here.">
                    </div>
                </div>
                <!-- End of First Row -->



                <!-- Start of Second Row -->
                <div class="row mt40">
                    @foreach ($faq as $index => $item)
                    <div class="col-md-12 topic">
                        <!-- Question -->
                        <div class="open">
                            <h6 class="question" data-search-term="{{$index+1}}. {{$item->title}}">{{$index+1}}.
                                {{$item->title}}</h6>
                            <i class="fa fa-angle-down hidden-xs"></i>
                        </div>

                        <!-- Answer -->
                        <p class="answer" style="display: none;">{{$item->description}}</p>
                    </div>
                    @endforeach

                </div>
                <!-- End of Second Row -->

            </div>
        </div>
    </div>
</section>
<!-- ===== End of FAQ Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@stop
@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop