@extends('layouts.main')
@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>privacy policy</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">pages</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of Main Wrapper Section ===== -->
<section class="ptb80">
    <div class="container">
        @if ($privacypolicy == null)
        <h1>Oops! there is nothing here</h1>
        @else
        <div class="col-md-12">
            {!! preg_replace('#<script(.*?)>(.*?)</script(.*?)>#is', '', $privacypolicy->text) !!}
            <ul class="nopadding mt20">
                <li><i class="fa fa-map-marker"></i> {{$privacypolicy->location}}</li>
                <li class="pt10"><i class="fa fa-phone"></i> {{$privacypolicy->phone}}</li>
            </ul>
        </div>
        @endif
    </div>
</section>
<!-- ===== End of Main Wrapper Section ===== -->
@stop
@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop