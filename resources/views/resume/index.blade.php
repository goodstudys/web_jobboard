@extends('layouts.main')
@section('content')
<!-- ===== Start of Main Wrapper Section ===== -->
<section class="ptb80" id="post-job">
    <div class="container">

        @if ($check == 0)
        <h3 class="uppercase text-blue">Create Resume</h3>
        @else
        <h3 class="uppercase text-blue">Resume Page</h3>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($check == 0)
        <!-- Start of Post Resume Form -->
        <form action="{{ route('resume.store') }}" method="post" enctype="multipart/form-data"
            class="post-job-resume mt50">
            @csrf

            <!-- Start of Resume Details -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your name</label>
                        <input class="form-control" type="text" name="name" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your email</label>
                        <input class="form-control" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}"
                            type="email" name="email" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your profession</label>
                        <input class="form-control" type="text" placeholder='e.g. "Android App Developer"'
                            name="profession" required>
                    </div>

                    <!-- Form Group -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>address according to ID (KTP) </label>
                                <input class="form-control" type="text" name="addressid" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>current address </label>
                                <input class="form-control" type="text" name="currentaddress" required>
                            </div>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>your photo</label>

                                <!-- Upload Button -->
                                <div class="upload-file-btn">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input type="file" name="photo" accept=".jpg,.png,.gif" id="imgInp" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <img id="blah" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>video <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='Link to a Video about yourself'
                            name="video">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>interest</label>
                        <select name="category" class="selectpicker" data-size="5" data-container="body" required>
                            <option value="" selected disabled>Choose one</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>resume content</label>
                        <textarea class="tinymce" name="contents"></textarea>
                        <div class="text-danger pt20" style="padding-top: 20px;color: red" id="describeresume">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>Skills</label>
                            <input class="form-control" type="text" placeholder="Separate each skill with a comma"
                                name="skills" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <hr>
                            <label>Education Section</label>
                        </div>
                        <div id="buildyourform">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>School/University name</label>
                                        <input class="form-control" type="text" name="place[]" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Program Study</label>
                                        <input class="form-control" type="text" name="program_study[]" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Degree</label>
                                        <input class="form-control" type="text" name="degree[]" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Year In</label>
                                        <input class="form-control" type="text" name="datein[]" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Year Out</label>
                                        <input class="form-control" type="text" name="dateout[]" required>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <input type="button" value="Add" class="btn btn-primary" id="add">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description[]" id="" rows="5"
                                            required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <hr>
                            <label>Experience Section</label>
                        </div>
                        <div id="buildyourform1">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Workplace</label>
                                        <input class="form-control" type="text" name="workplace[]" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Profession</label>
                                        <input class="form-control" type="text" name="profession1[]" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>year In</label>
                                        <input class="form-control" type="text" name="datein1[]" required>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Year Out</label>
                                        <input class="form-control" type="text" name="dateout1[]" required>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label>&nbsp;</label>
                                        <input type="button" value="Add" class="btn btn-primary" id="add1" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea class="form-control" name="description1[]" id="" rows="5"
                                            required></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <textarea name="" id="descriptionresume" cols="30" rows="10" style="display: none"
                            required></textarea>

                        <!-- Form Group -->
                        <div class="form-group pt30 nomargin" id="last">
                            <button onclick="validasiResume()" class="btn btn-blue btn-effect">submit</button>
                        </div>

                    </div>
                </div>
                <!-- End of Resume Details -->

        </form>
        <!-- End of Post Resume Form -->
        @else
        <!-- Start of Post Resume Form -->
        <form action="{{ route('resume.update', $resume->id) }}" method="post" enctype="multipart/form-data"
            class="post-job-resume mt50">
            @csrf

            <!-- Start of Resume Details -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your name</label>
                        <input class="form-control" type="text" name="name" value="{{ $resume->name }}" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your email</label>
                        <input class="form-control" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}"
                            type="email" name="email" value="{{ $resume->email }}" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your profession</label>
                        <input class="form-control" type="text" placeholder='e.g. "Android App Developer"'
                            name="profession" value="{{ $resume->profession }}" required>
                    </div>

                    <!-- Form Group -->
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>address according to ID (KTP) </label>
                                <input class="form-control" type="text" name="addressid"
                                    value="{{ $resume->addressID }}" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>current address </label>
                                <input class="form-control" type="text" name="currentaddress"
                                    value="{{ $resume->current_address }}" required>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <!-- Form Group -->
                            <div class="form-group">
                                <label>your photo</label>

                                <!-- Upload Button -->
                                <div class="upload-file-btn">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input type="file" name="photo" accept=".jpg,.png,.gif" id="imgInp">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <img id="blah" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label>current photo</label>

                                <div class="row">
                                    <div class="col-md-8 col-xs-12">
                                        <img src="{{ asset('/upload/users/photo/'.$resume->photo) }}"
                                            class="img-responsive" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>video <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='Link to a Video about yourself'
                            name="video" value="{{ $resume->video }}">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>interest</label>
                        <select name="category" class="selectpicker" data-size="5" data-container="body" required>
                            <option value="" selected disabled>Choose one</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}"
                                {{ $resume->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>resume content</label>
                        <textarea class="tinymce" name="contents">{{ $resume->content }}</textarea>

                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>Skills</label>
                        <input class="form-control" type="text" placeholder="Separate each skill with a comma"
                            name="skills" value="{{ $resume->skills }}" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <hr>
                        <label>Education Section</label>
                    </div>
                    <div id="buildyourform">
                        @foreach($educations as $index => $education)
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>School/University name</label>
                                    <input class="form-control" type="text" name="place[]"
                                        value="{{ $education->place }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Program Study</label>
                                    <input class="form-control" type="text" name="program_study[]"
                                        value="{{ $education->program_study }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Degree</label>
                                    <input class="form-control" type="text" name="degree[]"
                                        value="{{ $education->degree }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Year In</label>
                                    <input class="form-control" type="text" name="datein[]"
                                        value="{{ $education->date_start }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Year Out</label>
                                    <input class="form-control" type="text" name="dateout[]"
                                        value="{{ $education->date_end }}" required>
                                </div>
                            </div>
                            @if ($index == 0)
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Add" class="btn btn-primary" id="add">
                                </div>
                            </div>
                            @else
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Remove" class="remove1 btn btn-danger">
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description[]" id="" rows="5"
                                        required>{{ $education->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <hr>
                        <label>Experience Section</label>
                    </div>
                    <div id="buildyourform1">
                        @foreach($experiences as $index => $experience)
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Workplace</label>
                                    <input class="form-control" type="text" name="workplace[]"
                                        value="{{ $experience->work }}" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Profession</label>
                                    <input class="form-control" type="text" name="profession1[]"
                                        value="{{ $experience->profession }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>year In</label>
                                    <input class="form-control" type="text" name="datein1[]"
                                        value="{{ $experience->date_start }}" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Year Out</label>
                                    <input class="form-control" type="text" name="dateout1[]"
                                        value="{{ $experience->date_end }}" required>
                                </div>
                            </div>
                            @if ($index == 0)
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Add" class="btn btn-primary" id="add1">
                                </div>
                            </div>
                            @else
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Remove" class="remove2 btn btn-danger">
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description1[]" id="" rows="5"
                                        required>{{ $experience->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <!-- Form Group -->
                    <div class="form-group pt30 nomargin" id="last">
                        <button class="btn btn-blue btn-effect">submit</button>
                    </div>

                </div>
            </div>
            <!-- End of Resume Details -->

        </form>
        <!-- End of Post Resume Form -->
        @endif

    </div>
</section>
<!-- ===== End of Main Wrapper Section ===== -->
@stop

@section('extra_scripts')
<script>
    function validasiResume() {
      //  var inpObj = document.getElementById("describetext").value;
        for (i=0; i < tinymce.editors.length; i++){
            var content = tinymce.editors[i].getContent(); // get the content

            $('#descriptionresume').val(content);
            if (document.getElementById("descriptionresume").value.length >7 ) {
                document.getElementById("describeresume").innerHTML = '';
            }else if(document.getElementById("descriptionresume").value.length >7 ){
                document.getElementById("describeresume").innerHTML = '';
            } else{
                alert('Please fill Out Resume Content field');
                document.getElementById("describeresume").innerHTML = 'Please fill Out Resume Content field';
            }
        }

        }
    $(document).ready(function() {
            $("#add").click(function() {
                var lastField = $("#buildyourform div:last");
                var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
                var fieldWrapper = $("<div class=\"row\">");
                fieldWrapper.data("idx", intId);
                var fInput = $("                                <div class=\"col-md-3\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>School/University name</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"place[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Program Study</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"program_study[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Degree</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"degree[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year In</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"datein[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year Out</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"dateout[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var fDesc = $("                                <div class=\"col-md-12\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Description</label>\n" +
                    "                                        <textarea class=\"form-control\" name=\"description[]\" id=\"\" rows=\"5\" required></textarea>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper.append(fInput);
                fieldWrapper.append(removeButton);
                fieldWrapper.append(fDesc);
                $("#buildyourform").append(fieldWrapper);
            });

            $(".remove1").click(function() {
                $(this).parent().parent().parent().remove();
            });

            $(".remove2").click(function() {
                $(this).parent().parent().parent().remove();
            });

            $("#add1").click(function() {
                var lastField1 = $("#buildyourform1 div:last");
                var intId1 = (lastField1 && lastField1.length && lastField1.data("idx") + 1) || 1;
                var fieldWrapper1 = $("<div class=\"row\">");
                fieldWrapper1.data("idx", intId1);
                var fInput1 = $("                                <div class=\"col-md-4\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Workplace</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"workplace[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-3\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Profession</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"profession1[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year In</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"datein1[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year Out</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"dateout1[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton1 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove1 btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var fDesc1 = $("                                <div class=\"col-md-12\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Description</label>\n" +
                    "                                        <textarea class=\"form-control\" name=\"description1[]\" id=\"\" rows=\"5\" required></textarea>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton1.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper1.append(fInput1);
                fieldWrapper1.append(removeButton1);
                fieldWrapper1.append(fDesc1);
                $("#buildyourform1").append(fieldWrapper1);
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            $("#imgInp").change(function() {
                readURL(this);
            });
        });
</script>
@stop
