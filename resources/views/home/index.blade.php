@extends('layouts.main')
@section('content')
<!-- ========== Start of Main Slider Section ========== -->
<section class="main2">

    <!-- ===== Start of Swiper Slider ===== -->
    <div class="swiper-container">
        <div class="swiper-wrapper">

            <!-- Start of Slide 1 -->
            @foreach($sliders as $slider)
            <div class="swiper-slide overlay-black"
                style="background: url({{ asset('upload/sliders/'. $slider->image) }}); background-size: cover; background-position: 50% 50%;">

                <!-- Start of Slider Content -->
                <div class="slider-content container">

                    <div class="col-md-6 col-md-offset-6 col-xs-12 text-center">

                        <div class="section-title">
                            <h2 class="text-white">{{ $slider->title }}</h2>
                        </div>

                        <p class="text-white">{{ $slider->subtitle }}</p>

                    </div>

                </div>
                <!-- End of Slider Content -->

            </div>
            @endforeach
            <!-- End of Slide 1 -->


        </div>
        <!-- End of Swiper Wrapper -->

        <!-- Navigation Buttons -->
        <div class="swiper-button-prev"><i class="fa fa-angle-left"></i></div>
        <div class="swiper-button-next"><i class="fa fa-angle-right"></i></div>

    </div>
    <!-- ===== End of Swiper Slider ===== -->

</section>
<!-- ========== End of Main Slider Section ========== -->





<!-- ===== Start of Job Search Section ===== -->
<section class="job-search ptb40">
    <div class="container">

        <!-- Start of Form -->
        <form class="job-search-form row" action="{{route('searchcat.index')}}" method="post">
            @csrf
            <!-- Start of keywords input -->
            <div class="col-md-3 col-sm-12 search-keywords">
                <label for="search-keywords">Keywords</label>
                <input type="text" name="searchkeyword" id="search-keywords" placeholder="Keywords">
            </div>

            <!-- Start of category input -->
            <div class="col-md-3 col-sm-12 search-categories">
                <label for="search-categories">Category</label>
                <select name="searchcategories" class="selectpicker" id="search-categories" data-live-search="true"
                    title="Any Category" data-size="5" data-container="body">
                    @foreach($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>

            <!-- Start of location input -->
            <div class="col-md-4 col-sm-12 search-location">
                <label for="search-location">Location</label>
                <input type="text" name="searchlocation" id="search-location" placeholder="Location">
            </div>

            <!-- Start of submit input -->
            <div class="col-md-2 col-sm-12 search-submit">
                <button type="submit" class="btn btn-blue btn-effect btn-large"><i
                        class="fa fa-search"></i>search</button>
            </div>

        </form>
        <!-- End of Form -->

    </div>
</section>
<!-- ===== End of Job Search Section ===== -->





<!-- ===== Start of Popular Categories Section ===== -->
<section class="ptb80" id="categories2">
    <div class="container">

        <div class="section-title">
            <h2>popular categories</h2>
        </div>

        <!-- Start of Row -->
        <div class="row nomargin">
            @foreach ($categories as $index =>$item)
            <div style="display: none">{{$urutan = $index+1}}</div>

            @if ($urutan %2 == 0)
            <!-- Start of Category Wrapper div -->
            <div class="col-md-6 col-xs-12 cat-wrapper">
                <ul>
                    <form action="{{route('searchcat.index')}}" id="formCat{{$item->id}}" method="post">
                        @csrf
                        <input type="hidden" name="searchcategories" value="{{$item->id}}">
                        <!-- Category -->
                        <li>
                            <a href="#"
                                onclick="event.preventDefault();document.getElementById('formCat{{$item->id}}').submit();">
                                <h4>{{$item->name}}</h4>
                            </a>
                            <span>({{count($item->jobCategory)}} open positions)</span>
                        </li>
                    </form>
                </ul>

            </div>
            <!-- End of Category Wrapper div -->
            @else
            <!-- Start of Category Wrapper div -->
            <div class="col-md-6 col-xs-12 cat-wrapper">

                <ul>
                    <form action="{{route('searchcat.index')}}" id="formCat{{$item->id}}" method="post">
                        @csrf
                        <input type="hidden" name="searchcategories" value="{{$item->id}}">
                        <!-- Category -->
                        <li>
                            <a href="#"
                                onclick="event.preventDefault();document.getElementById('formCat{{$item->id}}').submit();">
                                <h4>{{$item->name}}</h4>
                            </a>
                            <span>({{count($item->jobCategory)}} open positions)</span>
                        </li>
                    </form>
                </ul>

            </div>
            <!-- End of Category Wrapper div -->
            @endif

            @endforeach

        </div>
        <!-- End of Row -->

        <div class="col-md-12 mt40 text-center">
            <a href="{{route('searchJob.index')}}" class="btn btn-blue btn-effect nomargin">browse all</a>
        </div>

    </div>
</section>
<!-- ===== End of Popular Categories Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->





<!-- ===== Start of Latest Job Section ===== -->
<section class="search-jobs ptb80" id="version3">
    <div class="container">

        <div class="section-title">
            <h2>latest jobs</h2>
        </div>

        <!-- Start of Row -->
        <div class="row">

            <!-- Start of Job Post Main -->
            <div class="col-md-12 job-post-main">

                <!-- Start of Job Post Wrapper -->
                <div class="job-post-wrapper mt20">
                    @if ($jobs == null)
                    <h1>Oops! there is nothing here</h1>
                    @else
                    @foreach ($jobs as $item)
                    <!-- ===== Start of Single Job Post 1 ===== -->
                    <div class="single-job-post row shadow-hover">
                        <!-- Job Company -->
                        <div class="col-md-2 col-xs-3">
                            <div class="job-company">
                                <a href="#">
                                    {{-- @if ($item->company_logo != null)
                                    @foreach ($profile as $prof)
                                    @if ($item->user_id == $prof->id)
                                    <img src="{{ asset('upload/users/company/'.$prof->image) }}" width="65" alt="">
                                    @endif
                                    @endforeach
                                    @else
                                    <img src="{{asset('upload/users/company/job/logo/'.$item->company_logo)}}"
                                        width="65" alt="">
                                    @endif --}}
                                    @if ($item->company_logo != null)
                                    <img src="{{asset('upload/users/company/job/logo/'.$item->company_logo)}}"
                                        width="45" alt="">
                                    @endif
                                </a>
                            </div>
                        </div>

                        <!-- Job Title & Info -->
                        <div class="col-md-8 col-xs-6 ptb20">
                            <div class="job-title">
                                <a href="{{route('job.show',$item->id)}}">{{$item->title}}</a>
                            </div>
                            <div class="job-info">
                                @if ($item->company_name != null)
                                <span class="company"><i class="fa fa-building-o"></i>{{$item->company_name}}</span>
                                @endif
                                @if ($item->location != null)
                                <span class="location"><i class="fa fa-map-marker"></i>{{$item->location}}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Job Category -->
                        <div class="col-md-2 col-xs-3 ptb30">
                            <div class="job-category">
                                @if ($item->type == 'Full Time')
                                <a href="javascript:void(0)" class="btn btn-green btn-small btn-effect">full time</a>
                                @elseif ($item->type =="Part Time")
                                <a href="javascript:void(0)" class="btn btn-purple btn-small btn-effect">part time</a>
                                @elseif ($item->type =="Freelance")
                                <a href="javascript:void(0)" class="btn btn-blue btn-small btn-effect">freelancer</a>
                                @elseif ($item->type =="Internship")
                                <a href="javascript:void(0)" class="btn btn-orange btn-small btn-effect">intership</a>
                                @elseif ($item->type =="Temporary")
                                <a href="javascript:void(0)" class="btn btn-red btn-small btn-effect">temporary</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- ===== End of Single Job Post 1 ===== -->
                    @endforeach
                    @endif

                </div>
                <!-- End of Job Post Wrapper -->

                <!-- Start of Pagination -->
                @if (count($jobs) == 5)
                <div class="col-md-12 text-center mt40">
                    <a href="{{route('searchJob.index')}}" class="btn btn-blue btn-effect">show more</a>
                </div>
                @endif

                <!-- End of Pagination -->

            </div>
            <!-- End of Job Post Main -->

        </div>
        <!-- End of Row -->

    </div>
</section>
<!-- ===== End of Latest Job Section ===== -->

@stop
@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop