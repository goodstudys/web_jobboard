@extends('layouts.main')

@section('content')
    <section class="page-header">
        <div class="container">

            <!-- Start of Page Title -->
            <div class="row">
                <div class="col-md-12">
                    <h2>checkout</h2>
                </div>
            </div>
            <!-- End of Page Title -->

            <!-- Start of Breadcrumb -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">pricing</a></li>
                        <li class="active">checkout</li>
                    </ul>
                </div>
            </div>
            <!-- End of Breadcrumb -->

        </div>
    </section>
    <!-- =============== End of Page Header 1 Section =============== -->





    <!-- ===== Start of Shop Check Out Section ===== -->
    <section class="shop ptb80">
        <div class="container">

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if ($order == null)
                    <div class="row">

                        <!-- Start of Form -->
                        <form action="{{ route('order.pricingStore') }}" method="post">
                        @csrf
                        <!-- Start of Accordion Check Out -->
                            <div class="col-md-9">

                                <!-- Start of Panel Group -->
                                <div class="panel-group" id="accordion">

                                    <!-- ========== Start of Panel 1 ========== -->
                                    <div class="panel panel-default">

                                        <!-- Start of Panel Heading -->
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" id="billing_address">
                                                    Billing Address
                                                </a>
                                            </h4>
                                        </div>
                                        <!-- End of Panel Heading -->

                                        <!-- Start of Accordion Body -->
                                        <div id="collapseOne" class="accordion-body collapse" aria-expanded="false" role="menu">
                                            <div class="panel-body">
                                                <div class="col-md-12 mtb20">

                                                    <!-- Start of Row -->
                                                    <div class="row">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>Country</label>
                                                                <select class="form-control" name="bill_country">
                                                                    <option value="" disabled>Select a country</option>
                                                                    <option value="ID" selected>Indonesia</option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->

                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="form-group">

                                                            <div class="col-md-6">
                                                                <label>First Name</label>
                                                                <input type="text" value="{{ $user->name == null ? '' : $user->name }}" class="form-control" name="bill_first_name">
                                                            </div>

                                                            <div class="col-md-6">
                                                                <label>Last Name</label>
                                                                <input type="text" value="" class="form-control" name="bill_last_name">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>Company Name</label>
                                                                <input type="text" value="{{ $user->company == null ? '' : $user->company }}" class="form-control" name="bill_company_name">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>Address </label>
                                                                <input type="text" value="{{ $user->address == null ? '' : $user->address }}" class="form-control" name="bill_address">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>City </label>
                                                                <input type="text" value="" class="form-control" name="bill_city">
                                                                <input type="hidden" name="subtotal" value="{{ $pricing->price }}" readonly>
                                                                <input type="hidden" name="shipping" value="FREE" readonly>
                                                                <input type="hidden" name="total" value="{{ $pricing->price }}" readonly>
                                                                <input type="hidden" name="payment" value="BANK" readonly>
                                                                <input type="hidden" name="product" value="{{ $pricing->rowPointer }}" readonly>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="col-md-12">

                                                            <input type="button" value="Continue" class="btn btn-blue btn-effect" id="continue_btn">

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- End of Form -->


                                                </div>
                                            </div>
                                        </div>
                                        <!-- End of Accordion Body -->
                                    </div>
                                    <!-- ========== End of Panel 1 ========== -->


                                    <!-- ========== Start of Panel 3 ========== -->
                                    <div class="panel panel-default">

                                        <!-- Start Panel Heading -->
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" id="paymentreview">
                                                    Review &amp; Payment
                                                </a>
                                            </h4>
                                        </div>
                                        <!-- End Panel Heading -->


                                        <!-- Start of Accordion Body -->
                                        <div id="collapseThree" class="accordion-body collapse" aria-expanded="false" role="menu">
                                            <div class="panel-body">

                                                <!-- Start of Cart Table -->
                                                <div class="col-md-12 mt20">
                                                    <div class="table-responsive">

                                                        <table class="table cart">
                                                            <thead>
                                                            <tr>
                                                                <th class="cart-product-thumbnail">&nbsp;</th>
                                                                <th class="cart-product-name">Product</th>
                                                                <th class="cart-product-price">Unit Price</th>
                                                                <th class="cart-product-quantity">Quantity</th>
                                                                <th class="cart-product-subtotal">Total</th>
                                                            </tr>
                                                            </thead>

                                                            <tbody>

                                                            <tr class="cart-item">

                                                                <!-- Cart Product Thumbnail -->
                                                                <td class="cart-product-thumbnail">
                                                                    <a href="#">
                                                                        <img width="64" height="64" src="images/shop/product1.jpg" alt="">
                                                                    </a>
                                                                </td>

                                                                <!-- Cart Product Name -->
                                                                <td class="cart-product-name">
                                                                    <a href="#">{{ $pricing->name }}</a>
                                                                </td>

                                                                <!-- Cart Product Price -->
                                                                <td class="cart-product-price">
                                                                    <span class="amount">Rp. {{ number_format($pricing->price) }}</span>
                                                                </td>

                                                                <!-- Cart Product Quantity -->
                                                                <td class="cart-product-quantity">
                                                                    <span>1</span>
                                                                </td>

                                                                <!-- Cart Subtotal -->
                                                                <td class="cart-product-subtotal">
                                                                    <span class="amount">Rp. {{ number_format($pricing->price) }}</span>
                                                                </td>
                                                            </tr>

                                                            </tbody>
                                                        </table>

                                                    </div>
                                                </div>
                                                <!-- End of Cart Table -->


                                                <!-- Start of Cart Total -->
                                                <div class="col-md-12 cart-total clearfix mt20">
                                                    <div class="table-responsive">
                                                        <h4 class="pb30">Cart Totals</h4>

                                                        <!-- Start of Table -->
                                                        <table class="table">
                                                            <tbody>
                                                            <!-- Start of Table Row -->
                                                            <tr>
                                                                <td class="cart-product-name">
                                                                    <strong>Cart Subtotal</strong>
                                                                </td>
                                                                <td class="cart-product-name">
                                                                    <span class="amount">Rp. {{ number_format($pricing->price) }}</span>
                                                                </td>
                                                            </tr>

                                                            <!-- Start of Table Row -->
                                                            <tr>
                                                                <td class="cart-product-name">
                                                                    <strong>Shipping</strong>
                                                                </td>

                                                                <td class="cart-product-name">
                                                                    <span class="amount">Free Delivery</span>
                                                                </td>
                                                            </tr>

                                                            <!-- Start of Table Row -->
                                                            <tr>
                                                                <td class="cart-product-name">
                                                                    <strong>Total</strong>
                                                                </td>

                                                                <td class="cart-product-name">
                                                            <span class="amount text-blue lead">
                                                    <strong>Rp. {{ number_format($pricing->price) }}</strong>
                                                </span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                        <!-- End of Table -->

                                                    </div>
                                                </div>
                                                <!-- End of Cart Total -->

                                                <!-- Start of Form Div -->
                                                <div class="col-md-12 mtb20">
                                                    <h4 class="pb20">Payment</h4>

                                                    <!-- Start of Row -->
                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <input type="checkbox" checked="checked" id="bank">
                                                            <label for="bank">Direct Bank Transfer</label>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->

                                                    {{--                                        <!-- Start of Row -->--}}
                                                    {{--                                        <div class="row">--}}
                                                    {{--                                            <div class="col-md-12">--}}

                                                    {{--                                                <input type="checkbox" id="cheque">--}}
                                                    {{--                                                <label for="cheque">Cheque Payment</label>--}}

                                                    {{--                                            </div>--}}
                                                    {{--                                        </div>--}}
                                                    {{--                                        <!-- End of Row -->--}}

                                                    {{--                                        <!-- Start of Row -->--}}
                                                    {{--                                        <div class="row">--}}
                                                    {{--                                            <div class="col-md-12">--}}

                                                    {{--                                                <input type="checkbox" id="paypal">--}}
                                                    {{--                                                <label for="paypal">PayPal</label>--}}

                                                    {{--                                            </div>--}}
                                                    {{--                                        </div>--}}
                                                    {{--                                        <!-- End of Row -->--}}

                                                </div>
                                                <!-- End of Form Div -->

                                            </div>
                                        </div>
                                        <!-- End of Accordion Body -->

                                    </div>
                                    <!-- ========== Start of Panel 3 ========== -->

                                </div>
                                <!-- End of Panel Group -->

                                <div class="col-md-12 text-right">
                                    <input type="submit" value="Place Order" name="proceed" class="btn btn-blue btn-effect">
                                </div>

                            </div>
                            <!-- End of Accordion Check Out -->
                        </form>

                        <!-- Start of Cart Total -->
                        <div class="col-md-3 clearfix cart-total">
                            <div class="table-responsive">
                                <h4 class="pb30">Cart Totals</h4>

                                <!-- Start of Table -->
                                <table class="table">
                                    <tbody>
                                    <!-- Start of Table Row -->
                                    <tr>
                                        <td class="cart-product-name">
                                            <strong>Cart Subtotal</strong>
                                        </td>
                                        <td class="cart-product-name">
                                            <span class="amount">Rp. {{ number_format($pricing->price) }}</span>
                                        </td>
                                    </tr>

                                    <!-- Start of Table Row -->
                                    <tr>
                                        <td class="cart-product-name">
                                            <strong>Shipping</strong>
                                        </td>

                                        <td class="cart-product-name">
                                            <span class="amount">Free Delivery</span>
                                        </td>
                                    </tr>

                                    <!-- Start of Table Row -->
                                    <tr>
                                        <td class="cart-product-name">
                                            <strong>Total</strong>
                                        </td>

                                        <td class="cart-product-name">
                                        <span class="amount text-blue lead">
                                            <strong>Rp. {{ number_format($pricing->price) }}</strong>
                                        </span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- End of Table -->

                            </div>
                        </div>
                        <!-- End of Cart Total -->

                    </div>
            @else
                    <div class="row">


                        <!-- Start of Accordion Check Out -->
                        <div class="col-md-12">

                            <!-- Start of Panel Group -->
                            <div class="panel-group" id="accordion">

                                <!-- ========== Start of Panel 1 ========== -->
                                <div class="panel panel-default">

                                    <!-- Start of Panel Heading -->
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" id="paymentconfirmation">
                                                Payment Confirmation
                                            </a>
                                        </h4>
                                    </div>
                                    <!-- End of Panel Heading -->

                                    <!-- Start of Accordion Body -->
                                    <div id="collapseOne" class="accordion-body collapse" aria-expanded="false" role="menu">
                                        <div class="panel-body">
                                            <div class="col-md-12 mtb20">

                                                <!-- Start of Form -->
                                                <form action="{{ route('paymentconfirm.store') }}" method="post">
                                                    @csrf

                                                    <!-- Start of Row -->
                                                    <div class="row">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>Transfer to : (BCA) 501231232 a/n JobBoard</label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->

                                                    <!-- Start of Row -->
                                                    <div class="row">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>BANK</label>
                                                                <select class="form-control" name="bank">
                                                                    <option value="" selected disabled>Select a bank</option>
                                                                    <option value="BNI">BNI</option>
                                                                    <option value="BCA">BCA</option>
                                                                    <option value="Mandiri">Mandiri</option>
                                                                </select>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>Cardholder's name</label>
                                                                <input type="text" value="" class="form-control" name="cardholders_name">
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->


                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <label>Total</label>
                                                                <input type="text" value="" class="form-control" name="total">
                                                                <input type="hidden" name="order" value="{{ $order->rowPointer }}" readonly>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->

                                                    <!-- Start of Row -->
                                                    <div class="row mt15">
                                                        <div class="col-md-12">

                                                            <input type="submit" value="Submit" class="btn btn-blue btn-effect">

                                                        </div>
                                                    </div>
                                                    <!-- End of Row -->

                                                </form>
                                                <!-- End of Form -->


                                            </div>
                                        </div>
                                    </div>
                                    <!-- End of Accordion Body -->
                                </div>
                                <!-- ========== End of Panel 1 ========== -->

                            </div>
                            <!-- End of Panel Group -->

                            @if ($order == null)
                                <div class="col-md-12 text-right">
                                    <input type="submit" value="Place Order" name="proceed" class="btn btn-blue btn-effect">
                                </div>
                            @endif

                        </div>
                        <!-- End of Accordion Check Out -->

                    </div>
            @endif
        </div>
    </section>
    <!-- ===== End of Shop Check Out Section ===== -->





    <!-- ===== Start of Get Started Section ===== -->
    <section class="get-started ptb40">
        <div class="container">
            <div class="row ">

                <!-- Column -->
                <div class="col-md-10 col-sm-9 col-xs-12">
                    <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
                </div>

                <!-- Column -->
                <div class="col-md-2 col-sm-3 col-xs-12">
                    <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
                </div>

            </div>
        </div>
    </section>
    <!-- ===== End of Get Started Section ===== -->

@stop

@section('extra_scripts')
    <script>
        $(document).ready(function(){
            $("#billing_address").click();

            $("#paymentconfirmation").click();

            $("#continue_btn").click(function(){
                $("#paymentreview").click();
            });
        });
    </script>
@stop
