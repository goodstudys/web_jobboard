@extends('layouts.main')
@section('content')
<!-- ===== Start of Pricing Tables Section ===== -->
<section class="pricing-tables pb80">
    <div class="container">

        <!-- Start Row -->
        <div class="row">

            @foreach ($pricings as $item)
            <!-- Start of 1st Pricing Table -->
            <div class="col-md-4 col-xs-12 mt80">
                <div class="pricing-table shadow-hover">

                    <!-- Pricing Header -->
                    <div class="pricing-header">
                        <h2>{{$item->name}}</h2>
                    </div>

                    <!-- Pricing -->
                    <div class="pricing">
                        <span class="currency">Rp. </span>
                        <span class="amount">{{number_format($item->price)}}</span>
                        <span class="month"></span>
                    </div>

                    <!-- Pricing Body -->


                    <div class="pricing-body">
                        <ul class="list">
                            @foreach ($pricingFeature as $p)
                            @if ($item->id == $p->pricing_id)
                            <li>{{$p->name}}</li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Pricing Footer -->
                    @if (\Illuminate\Support\Facades\Auth::check())
                        @if (\Illuminate\Support\Facades\Auth::user()->level != 5)
                            <div class="pricing-footer">
                                <a href="{{ route('pricing.checkout', 'orderId='.$item->rowPointer) }}" class="btn btn-blue btn-effect">get started</a>
                            </div>
                        @endif
                    @else
                        <div class="pricing-footer">
                            <a href="{{ route('pricing.checkout', 'orderId='.$item->rowPointer) }}" class="btn btn-blue btn-effect">get started</a>
                        </div>
                    @endif

                </div>
            </div>
            <!-- End of 1st Pricing Table -->
            @endforeach



            {{-- <!-- Start of 2nd Pricing Table -->
            <div class="col-md-4 col-xs-12 mt80">
                <div class="pricing-table shadow-hover" id="popular">

                    <!-- Pricing Header -->
                    <div class="pricing-header">
                        <h2>corporate</h2>
                    </div>

                    <!-- Pricing -->
                    <div class="pricing">
                        <span class="currency">$</span>
                        <span class="amount">49</span>
                        <span class="month">month</span>
                    </div>

                    <!-- Pricing Body -->
                    <div class="pricing-body">
                        <ul class="list">
                            <li>Post up to 10 Jobs per Day</li>
                            <li>Edit Your Job Listing</li>
                            <li>See Job Stats</li>
                            <li>10 Email Support</li>
                        </ul>
                    </div>

                    <!-- Pricing Footer -->
                    <div class="pricing-footer">
                        <a href="#" class="btn btn-blue btn-effect">get started</a>
                    </div>

                </div>
            </div>
            <!-- End of 2nd Pricing Table -->


            <!-- Start of 3rd Pricing Table -->
            <div class="col-md-4 col-xs-12 mt80">
                <div class="pricing-table shadow-hover">

                    <!-- Pricing Header -->
                    <div class="pricing-header">
                        <h2>enterprise</h2>
                    </div>

                    <!-- Pricing -->
                    <div class="pricing">
                        <span class="currency">$</span>
                        <span class="amount">99</span>
                        <span class="month">month</span>
                    </div>

                    <!-- Pricing Body -->
                    <div class="pricing-body">
                        <ul class="list">
                            <li>Post up to 100 Jobs per Day</li>
                            <li>Edit Your Job Listing</li>
                            <li>See Job Stats</li>
                            <li>Unlimited Email Support</li>
                        </ul>
                    </div>

                    <!-- Pricing Footer -->
                    <div class="pricing-footer">
                        <a href="#" class="btn btn-blue btn-effect">get started</a>
                    </div>

                </div>
            </div>
            <!-- End of 3rd Pricing Table --> --}}

        </div>
        <!-- End Row -->

    </div>
</section>
<!-- ===== End of Pricing Tables Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@stop
@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop
