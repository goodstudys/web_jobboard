@extends('layouts.main')
@section('content')
    <!-- ===== Start of Main Wrapper Section ===== -->
    <section class="ptb80" id="post-job">
        <div class="container">

            <h3 class="uppercase text-blue">Edit Profile</h3>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
        @endif
        <!-- Start of Post Resume Form -->
            <form action="{{ route('candidate.profile.update', $candidate->id) }}" method="post" enctype="multipart/form-data" class="post-job-resume mt50">
            @csrf
            <!-- Start of Resume Details -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your name</label>
                            <input class="form-control" type="text" name="name" value="{{ $candidate->name }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your age</label>
                            <input class="form-control" type="text" name="age" value="{{ $candidate->age }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your email</label>
                            <input class="form-control" type="email" name="email" value="{{ $candidate->email }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your phone</label>
                            <input class="form-control" type="number" name="phone" value="{{ $candidate->phone }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your website</label>
                            <input class="form-control" type="text" name="website" value="{{ $candidate->website }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your profession</label>
                            <input class="form-control" type="text" placeholder='e.g. "Android App Developer"' name="profession" value="{{ $candidate->profession }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>expected salary</label>
                            <input class="form-control" type="number" name="expected_salary" value="{{ $candidate->expected_salary }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>your address </label>
                            <input class="form-control" type="text" name="address" value="{{ $candidate->address }}">
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <!-- Form Group -->
                                <div class="form-group">
                                    <label>your photo</label>

                                    <!-- Upload Button -->
                                    <div class="upload-file-btn">
                                        <span><i class="fa fa-upload"></i> Upload</span>
                                        <input type="file" name="image" accept=".jpg,.png,.gif" id="imgInp">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <img id="blah" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>current photo</label>

                                    <div class="row">
                                        <div class="col-md-8 col-xs-12">
                                            <img src="{{ asset('/upload/users/profile/photo/'.$candidate->image) }}" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>describe yourself</label>
                            <textarea class="tinymce" name="contents">{{ $candidate->description }}</textarea>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>facebook <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "web.facebook.com/myname"' name="facebook" value="{{ $candidate->facebook }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>twitter <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "twitter.com/myname"' name="twitter" value="{{ $candidate->twitter }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>instragram <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "www.instagram.com/myname"' name="instagram" value="{{ $candidate->instagram }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>linkedin <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "www.linkedin.com/company/myname"' name="linkedin" value="{{ $candidate->linkedin }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <hr>
                            <div class="row">
                                <div class="col-md-11">
                                    <label>Work Section <span>(add work will append new form)</span></label>
                                </div>
                                <div class="col-md-1">
                                    <input type="button" value="Add Work" class="btn btn-primary" id="add3">
                                </div>
                            </div>
                        </div>
                        <div id="buildyourform3">
                            @foreach($works as $index => $work)
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <input class="form-control" type="text" name="type[]" value="{{ $work->type }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>image</label>

                                            <!-- Upload Button -->
                                            <div class="upload-file-btn">
                                                <span><i class="fa fa-upload"></i> Upload</span>
                                                <input type="file" name="imagesss{{ $index }}" accept=".jpg,.png,.gif">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <input type="button" value="Remove" class="remove10 btn btn-danger">
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <hr>
                            <label>Skill Section</label>
                        </div>
                        <div id="buildyourform2">
                            @foreach($skills as $index => $skill)
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" type="text" name="skillname[]" value="{{ $skill->name }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>percentage</label>
                                            <input class="form-control" type="number" name="percentage[]" value="{{ $skill->percentage }}">
                                        </div>
                                    </div>
                                    @if ($index == 0)
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="button" value="Add" class="btn btn-primary" id="add2">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="button" value="Remove" class="remove11 btn btn-danger">
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <hr>
                            <label>Education Section</label>
                        </div>
                        <div id="buildyourform">
                            @foreach($educations as $index => $education)
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>School/University name</label>
                                            <input class="form-control" type="text" name="place[]" value="{{ $education->place }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Program Study</label>
                                            <input class="form-control" type="text" name="program_study[]" value="{{ $education->program_study }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Degree</label>
                                            <input class="form-control" type="text" name="degree[]" value="{{ $education->degree }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Year In</label>
                                            <input class="form-control" type="text" name="datein[]" value="{{ $education->date_start }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Year Out</label>
                                            <input class="form-control" type="text" name="dateout[]" value="{{ $education->date_end }}">
                                        </div>
                                    </div>
                                    @if ($index == 0)
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="button" value="Add" class="btn btn-primary" id="add">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="button" value="Remove" class="remove12 btn btn-danger">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="description[]" id="" rows="5">{{ $education->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <hr>
                            <label>Experience Section</label>
                        </div>
                        <div id="buildyourform1">
                            @foreach($experiences as $index => $experience)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Workplace</label>
                                            <input class="form-control" type="text" name="workplace[]" value="{{ $experience->work }}">
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Profession</label>
                                            <input class="form-control" type="text" name="profession1[]" value="{{ $experience->profession }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>year In</label>
                                            <input class="form-control" type="text" name="datein1[]" value="{{ $experience->date_start }}">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Year Out</label>
                                            <input class="form-control" type="text" name="dateout1[]" value="{{ $experience->date_end }}">
                                        </div>
                                    </div>
                                    @if ($index == 0)
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="button" value="Add" class="btn btn-primary" id="add1">
                                            </div>
                                        </div>
                                    @else
                                        <div class="col-md-1">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <input type="button" value="Remove" class="remove13 btn btn-danger">
                                            </div>
                                        </div>
                                    @endif
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" name="description1[]" id="" rows="5">{{ $experience->description }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Form Group -->
                        <div class="form-group pt30 nomargin" id="last">
                            <button class="btn btn-blue btn-effect">submit</button>
                        </div>

                    </div>
                </div>
                <!-- End of Resume Details -->

            </form>
            <!-- End of Post Resume Form -->

        </div>
    </section>
    <!-- ===== End of Main Wrapper Section ===== -->
@stop

@section('extra_scripts')
    <script>
        $(document).ready(function() {
            $('.remove10').click(function() {
                $(this).parent().parent().parent().remove();
            });

            $('.remove11').click(function() {
                $(this).parent().parent().parent().remove();
            });

            $('.remove12').click(function() {
                $(this).parent().parent().parent().remove();
            });

            $('.remove13').click(function() {
                $(this).parent().parent().parent().remove();
            });
            //education
            $("#add").click(function() {
                var lastField = $("#buildyourform div:last");
                var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
                var fieldWrapper = $("<div class=\"row\">");
                fieldWrapper.data("idx", intId);
                var fInput = $("                                <div class=\"col-md-3\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>School/University name</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"place[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Program Study</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"program_study[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Degree</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"degree[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year In</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"datein[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year Out</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"dateout[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var fDesc = $("                                <div class=\"col-md-12\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Description</label>\n" +
                    "                                        <textarea class=\"form-control\" name=\"description[]\" id=\"\" rows=\"5\"></textarea>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper.append(fInput);
                fieldWrapper.append(removeButton);
                fieldWrapper.append(fDesc);
                $("#buildyourform").append(fieldWrapper);
            });

            // experience
            $("#add1").click(function() {
                var lastField1 = $("#buildyourform1 div:last");
                var intId1 = (lastField1 && lastField1.length && lastField1.data("idx") + 1) || 1;
                var fieldWrapper1 = $("<div class=\"row\">");
                fieldWrapper1.data("idx", intId1);
                var fInput1 = $("                                <div class=\"col-md-4\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Workplace</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"workplace[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-3\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Profession</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"profession1[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year In</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"datein1[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year Out</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"dateout1[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton1 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove1 btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var fDesc1 = $("                                <div class=\"col-md-12\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Description</label>\n" +
                    "                                        <textarea class=\"form-control\" name=\"description1[]\" id=\"\" rows=\"5\"></textarea>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton1.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper1.append(fInput1);
                fieldWrapper1.append(removeButton1);
                fieldWrapper1.append(fDesc1);
                $("#buildyourform1").append(fieldWrapper1);
            });

            // skill
            $("#add2").click(function() {
                var lastField2 = $("#buildyourform2 div:last");
                var intId2 = (lastField2 && lastField2.length && lastField2.data("idx") + 1) || 1;
                var fieldWrapper2 = $("<div class=\"row\">");
                fieldWrapper2.data("idx", intId2);
                var fInput2 = $("                                <div class=\"col-md-9\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Name</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"skillname[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>percentage</label>\n" +
                    "                                        <input class=\"form-control\" type=\"number\" name=\"percentage[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton2 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove2 btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton2.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper2.append(fInput2);
                fieldWrapper2.append(removeButton2);
                $("#buildyourform2").append(fieldWrapper2);
            });

            // recent work
            var count = {{ (int)$workcounter }}
            $("#add3").click(function() {
                var lastField3 = $("#buildyourform3 div:last");
                var intId3 = (lastField3 && lastField3.length && lastField3.data("idx") + 1) || 1;
                var fieldWrapper3 = $("<div class=\"row\">");
                fieldWrapper3.data("idx", intId3);
                var fInput3 = $("                                <div class=\"col-md-9\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Type</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"type[]\">\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>image</label>\n" +
                    "\n" +
                    "                                        <!-- Upload Button -->\n" +
                    "                                        <div class=\"upload-file-btn\">\n" +
                    "                                            <span><i class=\"fa fa-upload\"></i> Upload</span>\n" +
                    "                                            <input type=\"file\" name=\"imagesss" + (count++) +"\" accept=\".jpg,.png,.gif\" >\n" +
                    "                                        </div>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton3 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove3 btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton3.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper3.append(fInput3);
                fieldWrapper3.append(removeButton3);
                $("#buildyourform3").append(fieldWrapper3);
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            $("#imgInp").change(function() {
                readURL(this);
            });
        });
    </script>
@stop
