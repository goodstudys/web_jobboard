@extends('layouts.main')
@section('content')
<!-- ===== Start of Main Wrapper Section ===== -->
<section class="ptb80" id="post-job">
    <div class="container">

        <h3 class="uppercase text-blue">Create Profile</h3>

        @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        <!-- Start of Post Resume Form -->
        <form action="{{ route('candidate.profile.store') }}" method="post" enctype="multipart/form-data"
            class="post-job-resume mt50">
            @csrf
            <!-- Start of Resume Details -->
            <div class="row">
                <div class="col-md-12">

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your name</label>
                        <input class="form-control" type="text" name="name" id="nameText" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your age</label>
                        <input class="form-control" type="number" name="age" id="ageText" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your email</label>
                        <input class="form-control" type="email" name="email"
                            pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" id="emailText" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your phone</label>
                        <input class="form-control" type="number" name="phone" id="phoneText" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your website <span>(optional)</span></label>
                        <input class="form-control" type="text" name="website">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your profession</label>
                        <input class="form-control" type="text" placeholder='e.g. "Android App Developer"'
                            name="profession" id="professionText" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>expected salary</label>
                        <input class="form-control" type="number" name="expected_salary" id="expectedsalaryText"
                            required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your address </label>
                        <input class="form-control" type="text" name="address" id="addressText" required>
                    </div>

                    <!-- Form Group -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>your photo</label>

                                <!-- Upload Button -->
                                <div class="upload-file-btn">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input type="file" name="image" accept=".jpg,.png,.gif" id="imgInp" required>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <img id="blah" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>describe yourself</label>
                        <textarea class="tinymce form-control" name="contents" id="describetext"></textarea>
                        <div class="text-danger pt20" style="padding-top: 20px;color: red" id="describe">
                        </div>
                    </div>
                    <!-- Form Group -->
                    <div class="form-group">
                        <label>facebook <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. "web.facebook.com/myname"'
                            name="facebook">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>twitter <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. "twitter.com/myname"' name="twitter">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>instragram <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. "www.instagram.com/myname"'
                            name="instagram">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>linkedin <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. "www.linkedin.com/company/myname"'
                            name="linkedin">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <hr>
                        <div class="row">
                            <div class="col-md-11">
                                <label>Work Section <span>(add work will append new form)</span></label>
                            </div>
                            <div class="col-md-1"><input type="text" style="display: none" id="buttonvalidation"
                                    required>
                                <input type="button" value="Add Work" class="btn btn-primary" id="add3">
                            </div>
                        </div>
                    </div>
                    <div id="buildyourform3">
                        <div class="row">

                            <div class="col-md-9">
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <label>Type</label>--}}
                                {{--                                        <input class="form-control" type="text" name="type[]">--}}
                                {{--                                    </div>--}}
                            </div>
                            <div class="col-md-2">
                                {{--                                    <div class="form-group">--}}
                                {{--                                        <label>image</label>--}}

                                {{--                                        <!-- Upload Button -->--}}
                                {{--                                        <div class="upload-file-btn">--}}
                                {{--                                            <span><i class="fa fa-upload"></i> Upload</span>--}}
                                {{--                                            <input type="file" name="imagesss" accept=".jpg,.png,.gif" >--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}
                            </div>

                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <hr>
                        <label>Skill Section</label>
                    </div>
                    <div id="buildyourform2">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="skillname[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>percentage</label>
                                    <input class="form-control" type="number" name="percentage[]" required>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Add" class="btn btn-primary" id="add2">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <hr>
                        <label>Education Section</label>
                    </div>
                    <div id="buildyourform">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>School/University name</label>
                                    <input class="form-control" type="text" name="place[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Program Study</label>
                                    <input class="form-control" type="text" name="program_study[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Degree</label>
                                    <input class="form-control" type="text" name="degree[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Year In</label>
                                    <input class="form-control" type="text" name="datein[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Year Out</label>
                                    <input class="form-control" type="text" name="dateout[]" required>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Add" class="btn btn-primary" id="add">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description[]" id="" rows="5"
                                        required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <hr>
                        <label>Experience Section</label>
                    </div>
                    <div id="buildyourform1">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Workplace</label>
                                    <input class="form-control" type="text" name="workplace[]" required>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Profession</label>
                                    <input class="form-control" type="text" name="profession1[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>year In</label>
                                    <input class="form-control" type="text" name="datein1[]" required>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Year Out</label>
                                    <input class="form-control" type="text" name="dateout1[]" required>
                                </div>
                            </div>
                            <div class="col-md-1">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="button" value="Add" class="btn btn-primary" id="add1">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description1[]" id="" rows="5"
                                        required></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <textarea name="" id="description" cols="30" rows="10" style="display: none" required></textarea>

                    <!-- Form Group -->
                    <div class="form-group pt30 nomargin" id="last">
                        <button onclick="myValidasi()" class="btn btn-blue btn-effect">submit</button>
                    </div>

                </div>
            </div>
            <!-- End of Resume Details -->

        </form>
        <!-- End of Post Resume Form -->
        {{-- <input id="id1" type="number" min="100" max="300" required>
        <button onclick="myFunction()">OK</button>
        <p id="demo"></p> --}}
    </div>
</section>
<!-- ===== End of Main Wrapper Section ===== -->
@stop

@section('extra_scripts')
<script>
    //     $("#run").click(function(){
// 	var content = document.getElementById("test").value;
//     (content.length < 1) ? alert("Has No Content"):"";

// });
    function myValidasi() {
        var inpObj = document.getElementById("describetext").value;
        var alertbuttonval;
        if (document.getElementById("buttonvalidation").value == '1') {
            alertbuttonval = '';
        } else {
            alertbuttonval = 'Please fill Out Work Section field';
        }
        for (i=0; i < tinymce.editors.length; i++){
            var content = tinymce.editors[i].getContent(); // get the content

            $('#description').val(content);
            if (document.getElementById("description").value.length >7 && document.getElementById("buttonvalidation").value == '1') {
                document.getElementById("describe").innerHTML = '';
            }else if(document.getElementById("description").value.length >7 && document.getElementById("buttonvalidation").value != '1'){
                document.getElementById("describe").innerHTML = '';
                alert(alertbuttonval);
            } else{
                alert('Please fill Out Describe yourself field\n' +alertbuttonval);
                document.getElementById("describe").innerHTML = 'Please fill Out Describe yourself field';
            }
        }

        } 
        $(document).ready(function() {
            //education
            $("#add").click(function() {
                var lastField = $("#buildyourform div:last");
                var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
                var fieldWrapper = $("<div class=\"row\">");
                fieldWrapper.data("idx", intId);
                var fInput = $("                                <div class=\"col-md-3\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>School/University name</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"place[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Program Study</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"program_study[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Degree</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"degree[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year In</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"datein[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year Out</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"dateout[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var fDesc = $("                                <div class=\"col-md-12\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Description</label>\n" +
                    "                                        <textarea class=\"form-control\" name=\"description[]\" id=\"\" rows=\"5\" required></textarea>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper.append(fInput);
                fieldWrapper.append(removeButton);
                fieldWrapper.append(fDesc);
                $("#buildyourform").append(fieldWrapper);
            });

            // experience
            $("#add1").click(function() {
                var lastField1 = $("#buildyourform1 div:last");
                var intId1 = (lastField1 && lastField1.length && lastField1.data("idx") + 1) || 1;
                var fieldWrapper1 = $("<div class=\"row\">");
                fieldWrapper1.data("idx", intId1);
                var fInput1 = $("                                <div class=\"col-md-4\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Workplace</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"workplace[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-3\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Profession</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"profession1[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year In</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"datein1[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Year Out</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"dateout1[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton1 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove1 btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var fDesc1 = $("                                <div class=\"col-md-12\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Description</label>\n" +
                    "                                        <textarea class=\"form-control\" name=\"description1[]\" id=\"\" rows=\"5\" required></textarea>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton1.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper1.append(fInput1);
                fieldWrapper1.append(removeButton1);
                fieldWrapper1.append(fDesc1);
                $("#buildyourform1").append(fieldWrapper1);
            });

            // skill
            $("#add2").click(function() {
                var lastField2 = $("#buildyourform2 div:last");
                var intId2 = (lastField2 && lastField2.length && lastField2.data("idx") + 1) || 1;
                var fieldWrapper2 = $("<div class=\"row\">");
                fieldWrapper2.data("idx", intId2);
                var fInput2 = $("                                <div class=\"col-md-9\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Name</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"skillname[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>percentage</label>\n" +
                    "                                        <input class=\"form-control\" type=\"number\" name=\"percentage[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton2 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove2 btn btn-danger\" />\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton2.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper2.append(fInput2);
                fieldWrapper2.append(removeButton2);
                $("#buildyourform2").append(fieldWrapper2);
            });

            // recent work
            var count = 0;
            $("#add3").click(function() {
                document.getElementById("buttonvalidation").value = '1';
                var lastField3 = $("#buildyourform3 div:last");
                var intId3 = (lastField3 && lastField3.length && lastField3.data("idx") + 1) || 1;
                var fieldWrapper3 = $("<div class=\"row\">");
                fieldWrapper3.data("idx", intId3);
                var fInput3 = $("                                <div class=\"col-md-9\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>Type</label>\n" +
                    "                                        <input class=\"form-control\" type=\"text\" name=\"type[]\" required>\n" +
                    "                                    </div>\n" +
                    "                                </div>\n" +
                    "\n" +
                    "                                <div class=\"col-md-2\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>image</label>\n" +
                    "\n" +
                    "                                        <!-- Upload Button -->\n" +
                    "                                        <div class=\"upload-file-btn\">\n" +
                    "                                            <span><i class=\"fa fa-upload\"></i> Upload</span>\n" +
                    "                                            <input type=\"file\" name=\"imagesss" + (count++) +"\" accept=\".jpg,.png,.gif\" required>\n" +
                    "                                        </div>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                var removeButton3 = $("                                <div class=\"col-md-1\">\n" +
                    "                                    <div class=\"form-group\">\n" +
                    "                                        <label>&nbsp;</label>\n" +
                    "                                        <input type=\"button\" value=\"Remove\" class=\"remove3 btn btn-danger\"/>\n" +
                    "                                    </div>\n" +
                    "                                </div>");
                removeButton3.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper3.append(fInput3);
                fieldWrapper3.append(removeButton3);
                $("#buildyourform3").append(fieldWrapper3);
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#blah').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            $("#imgInp").change(function() {
                readURL(this);
            });
        });
</script>
@stop
