@extends('layouts.main')
@section('content')
    <!-- ===== Start of Candidate Profile Header Section ===== -->
    <section class="profile-header">
    </section>
    <!-- ===== End of Candidate Header Section ===== -->





    <!-- ===== Start of Main Wrapper Candidate Profile Section ===== -->
    <section class="pb80" id="candidate-profile">
        <div class="container">
            @if ($check == 0)
                <div class="candidate-profile">
                    <div class="text-center">
                        <a href="{{ route('candidate.profile.create') }}" class="btn btn-blue btn-effect">Create your profile</a>
                    </div>
                </div>
            @else
                <!-- Start of Row -->
                    <div class="row candidate-profile">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        @if (\Illuminate\Support\Facades\Auth::user()->level == 5)
                                <div class="text-right">
                                    @if ($checkResume != 0)
                                        <a href="{{ route('resume.index') }}" class="btn btn-primary">Resume Edit</a>
                                    @endif
                                    <a href="{{ route('candidate.profile.edit', $candidate->id) }}" class="btn btn-primary">Profile Edit</a>
                                </div>
                        @endif

                        <!-- Start of Profile Picture -->
                        <div class="col-md-3 col-xs-12">
                            <div class="profile-photo">
                                <img src="{{ asset('/upload/users/profile/photo/'. $candidate->image) }}" class="img-responsive" alt="">
                            </div>

                            <!-- Start of Social Media Buttons -->
                            <ul class="social-btns list-inline text-center mt20">
                                <!-- Social Media -->
                                @if ($candidate->facebook != null)
                                    <li>
                                        <a href="{{ $candidate->facebook }}" class="social-btn-roll facebook transparent">
                                            <div class="social-btn-roll-icons">
                                                <i class="social-btn-roll-icon fa fa-facebook"></i>
                                                <i class="social-btn-roll-icon fa fa-facebook"></i>
                                            </div>
                                        </a>
                                    </li>
                                @endif

                            <!-- Social Media -->
                                @if ($candidate->twitter != null)
                                    <li>
                                        <a href="{{ $candidate->twitter }}" class="social-btn-roll twitter transparent">
                                            <div class="social-btn-roll-icons">
                                                <i class="social-btn-roll-icon fa fa-twitter"></i>
                                                <i class="social-btn-roll-icon fa fa-twitter"></i>
                                            </div>
                                        </a>
                                    </li>
                                @endif

                            <!-- Social Media -->
                                @if ($candidate->email != null)
                                    <li>
                                        <a href="{{ $candidate->email }}" class="social-btn-roll google-plus transparent">
                                            <div class="social-btn-roll-icons">
                                                <i class="social-btn-roll-icon fa fa-google-plus"></i>
                                                <i class="social-btn-roll-icon fa fa-google-plus"></i>
                                            </div>
                                        </a>
                                    </li>
                                @endif

                            <!-- Social Media -->
                                @if ($candidate->instagram != null)
                                    <li>
                                        <a href="{{ $candidate->instagram }}" class="social-btn-roll instagram transparent">
                                            <div class="social-btn-roll-icons">
                                                <i class="social-btn-roll-icon fa fa-instagram"></i>
                                                <i class="social-btn-roll-icon fa fa-instagram"></i>
                                            </div>
                                        </a>
                                    </li>
                                @endif

                            <!-- Social Media -->
                                @if ($candidate->linkedin != null)
                                    <li>
                                        <a href="{{ $candidate->linkedin }}" class="social-btn-roll linkedin transparent">
                                            <div class="social-btn-roll-icons">
                                                <i class="social-btn-roll-icon fa fa-linkedin"></i>
                                                <i class="social-btn-roll-icon fa fa-linkedin"></i>
                                            </div>
                                        </a>
                                    </li>
                                @endif

                            </ul>
                            <!-- End of Social Media Buttons -->

                        </div>
                        <!-- End of Profile Picture -->

                        <!-- Start of Profile Description -->
                        <div class="col-md-9 col-xs-12">
                            <div class="profile-descr">

                                <!-- Profile Title -->
                                <div class="profile-title">
                                    <h2 class="capitalize">{{ $candidate->name }}</h2>
                                    <h5 class="pt10">{{ $candidate->profession }}</h5>
                                </div>

                                <!-- Profile Details -->
                                <div class="profile-details mt20">
                                    {!! preg_replace('#<script(.*?)>(.*?)</script(.*?)>#is', '', $candidate->description) !!}
                                </div>

                                <ul class="profile-info mt20 nopadding">
                                    <li>
                                        <i class="fa fa-map-marker"></i>
                                        <span>{{ $candidate->address }}</span>
                                    </li>

                                    @if ($candidate->website != null)
                                        <li>
                                            <i class="fa fa-globe"></i>
                                            <a href="#">{{ $candidate->website }}</a>
                                        </li>
                                    @endif

                                    <li>
                                        <i class="fa fa-money"></i>
                                        <span>Rp. {{number_format($candidate->expected_salary, 2)}}</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-birthday-cake"></i>
                                        <span>{{ $candidate->age }} years-old</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-phone"></i>
                                        <span>{{ $candidate->phone }}</span>
                                    </li>

                                    <li>
                                        <i class="fa fa-envelope"></i>
                                        <a href="#">{{ $candidate->email }}</a>
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <!-- End of Profile Description -->

                    </div>
                    <!-- End of Row -->
            @endif

            @if ($check != 0)
                <!-- Start of Row -->
                    <div class="row skills mt40">

                        <div class="col-md-12 text-center">
                            <h3 class="pb40">My Skills</h3>
                        </div>

                        <!-- Start of Skill Bars Wrapper -->
                        <div class="col-md-6 col-xs-12 mt20">
                            <!-- Start of Skill Bar -->
                            @foreach($skills as $skill)
                                <div class="skillbar clearfix " data-percent="{{ $skill->percentage }}%">
                                    <div class="skillbar-title"><span>{{ $skill->name }}</span></div>
                                    <div class="skillbar-bar">100</div>
                                    <div class="skill-bar-percent">{{ $skill->percentage }}%</div>
                                </div>
                            @endforeach
                            <!-- End Skill Bar -->
                        </div>
                        <!-- End of Skill Bars Wrapper -->


{{--                        <!-- Start of Skill Bars Wrapper -->--}}
{{--                        <div class="col-md-6 col-xs-12 mt20">--}}
{{--                            <!-- Start of Skill Bar -->--}}
{{--                            <div class="skillbar clearfix " data-percent="75%">--}}
{{--                                <div class="skillbar-title"><span>PHP</span></div>--}}
{{--                                <div class="skillbar-bar"></div>--}}
{{--                                <div class="skill-bar-percent">75%</div>--}}
{{--                            </div>--}}
{{--                            <!-- End Skill Bar -->--}}

{{--                            <!-- Start of Skill Bar -->--}}
{{--                            <div class="skillbar clearfix " data-percent="65%">--}}
{{--                                <div class="skillbar-title"><span>MySql</span></div>--}}
{{--                                <div class="skillbar-bar"></div>--}}
{{--                                <div class="skill-bar-percent">65%</div>--}}
{{--                            </div>--}}
{{--                            <!-- End Skill Bar -->--}}

{{--                            <!-- Start of Skill Bar -->--}}
{{--                            <div class="skillbar clearfix " data-percent="65%">--}}
{{--                                <div class="skillbar-title"><span>Wordpress</span></div>--}}
{{--                                <div class="skillbar-bar"></div>--}}
{{--                                <div class="skill-bar-percent">65%</div>--}}
{{--                            </div>--}}
{{--                            <!-- End Skill Bar -->--}}
{{--                        </div>--}}
{{--                        <!-- End of Skill Bars Wrapper -->--}}

                    </div>
                    <!-- End of Row -->
            @endif

        </div>
    </section>
    <!-- ===== End of Candidate Profile Section ===== -->

    @if ($check != 0)
        <!-- ===== Start of Portfolio Section ===== -->
        <section class="portfolio ptb80">
            <div class="container">

                <div class="row">
                    <h3 class="text-center pb60">Recent Work</h3>

                    <!-- Filter Buttons -->
                    <ul class="list-inline text-center uppercase" id="portfolio-sorting">
                        <li><a href="#0" data-filter="*" class="current">all</a></li>
                        @foreach ($works as $work)
                            <li><a href="#0" data-filter=".{{ $work->type }}" class="current">{{ $work->type }}</a></li>
                        @endforeach
                    </ul>
                </div>

                <!-- Start of Portfolio Grid -->
                <div class="row portfolio-grid mt40">

                    <!-- Portfolio Item -->
                    @foreach($works as $work)
                        <div class="element col-md-4 col-sm-6 col-xs-6 portfolio-cat1">
                            <figure>
                                <a href="{{ asset('/upload/users/work/'. $work->image) }}" class="hover-zoom">
                                    <img src="{{ asset('/upload/users/work/'. $work->image) }}" class="img-responsive" alt="">
                                </a>
                            </figure>
                        </div>
                    @endforeach

                </div>
                <!-- End of Portfolio Grid -->

                <div class="row">
                    <div class="col-md-12 text-center mt20">
{{--                        <a href="#" class="btn btn-blue btn-effect">show more</a>--}}
                    </div>
                </div>

            </div>
        </section>
        <!-- ===== End of Portfolio Section ===== -->





        <!-- ===== Start of Education Section ===== -->
        <section class="education ptb80">
            <div class="container">

                <div class="col-md-12 text-center">
                    <h3 class="pb60">Education</h3>
                </div>

                <!-- Start of Education Column -->
                @foreach($educations as $education)
                <div class="col-md-12">
                    <div class="item-block shadow-hover">

                        <!-- Start of Education Header -->
                            <div class="education-header clearfix">
                                <img src="images/companies/envato.svg" alt="">
                                <div>
                                    <h4>{{ $education->degree }} <small>- {{ $education->program_study }}</small></h4>
                                    <h5>{{ $education->place }}</h5>
                                </div>
                                <h6 class="time">{{ $education->date_start }} - {{ $education->date_end }}</h6>
                            </div>
                        <!-- End of Education Header -->

                        <!-- Start of Education Body -->
                        <div class="education-body">
                            <p>{{ $education->description }}</p>
                        </div>
                        <!-- End of Education Body -->

                    </div>
                </div>
                @endforeach
                <!-- End of Education Column -->

            </div>
        </section>
        <!-- ===== End of Education Section ===== -->





        <!-- ===== Start of Work Experience Section ===== -->
        <section class="work-experience ptb80">
            <div class="container">

                <div class="col-md-12 text-center">
                    <h3 class="pb60">Work Experience</h3>
                </div>

                <!-- Start of Work Experience Column -->
                @foreach($experiences as $experience)
                    <div class="col-md-12">
                        <div class="item-block shadow-hover">

                            <!-- Start of Work Experience Header -->
                            <div class="experience-header clearfix">
                                <img src="images/companies/envato.svg" alt="">
                                <div>
                                    <h4>{{ $experience->work }}</h4>
                                    <h5><small>{{ $experience->profession }}</small></h5>
                                </div>
                                <h6 class="time">{{ $experience->date_start }} - {{ $experience->date_end }}</h6>
                            </div>
                            <!-- End of Work Experience Header -->

                            <!-- Start of Work Experience Body -->
                            <div class="experience-body">
                                <p>{{ $experience->description }}</p>
                            </div>
                            <!-- End of Work Experience Body -->

                        </div>
                    </div>
                @endforeach
                <!-- End of Work Experience Column -->


            </div>
        </section>
        <!-- ===== End of Work Experience Section ===== -->
        @else
        <section class="education ptb80">
            <div class="container">
                <div class="text-center">
                    <h1>Oops! there is nothing here</h1>
                </div>
            </div>
        </section>
    @endif


    <!-- ===== Start of Get Started Section ===== -->
    <section class="get-started ptb40">
        <div class="container">
            <div class="row ">

                <!-- Column -->
                <div class="col-md-10 col-sm-9 col-xs-12">
                    <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
                </div>

                <!-- Column -->
                <div class="col-md-2 col-sm-3 col-xs-12">
                    <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
                </div>

            </div>
        </div>
    </section>
    <!-- ===== End of Get Started Section ===== -->
@stop
