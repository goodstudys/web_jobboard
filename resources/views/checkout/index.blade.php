@extends('layouts.main')

@section('content')
    <section class="page-header">
        <div class="container">

            <!-- Start of Page Title -->
            <div class="row">
                <div class="col-md-12">
                    <h2>Advertisement</h2>
                </div>
            </div>
            <!-- End of Page Title -->

            <!-- Start of Breadcrumb -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">Advertisement</a></li>
                        <li class="active">create</li>
                    </ul>
                </div>
            </div>
            <!-- End of Breadcrumb -->

        </div>
    </section>
    <!-- =============== End of Page Header 1 Section =============== -->





    <!-- ===== Start of Shop Check Out Section ===== -->
    <section class="shop ptb80">
        <div class="container">
            <div class="row">


                <!-- Start of Accordion Check Out -->
                <div class="col-md-12">

                    <!-- Start of Panel Group -->
                    <div class="panel-group" id="accordion">

                        <!-- ========== Start of Panel 1 ========== -->
                        <div class="panel panel-default">

                            <!-- Start of Panel Heading -->
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" id="paymentconfirmation">
                                        Payment Confirmation
                                    </a>
                                </h4>
                            </div>
                            <!-- End of Panel Heading -->

                            <!-- Start of Accordion Body -->
                            <div id="collapseOne" class="accordion-body collapse" aria-expanded="false" role="menu">
                                <div class="panel-body">
                                    <div class="col-md-12 mtb20">

                                        <!-- Start of Form -->
                                        <form action="{{ route('paymentconfirm.store') }}" method="post">
                                        @csrf

                                        <div class="row">
                                            <div class="form-group">

                                                <div class="col-md-12">
                                                    <label>Transfer to : (BCA) 501231232 a/n JobBoard</label>
                                                </div>

                                            </div>
                                        </div>

                                        <!-- Start of Row -->
                                            <div class="row">
                                                <div class="form-group">

                                                    <div class="col-md-12">
                                                        <label>BANK</label>
                                                        <select class="form-control" name="bank">
                                                            <option value="" selected disabled>Select a bank</option>
                                                            <option value="BNI">BNI</option>
                                                            <option value="BCA">BCA</option>
                                                            <option value="Mandiri">Mandiri</option>
                                                        </select>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- End of Row -->


                                            <!-- Start of Row -->
                                            <div class="row mt15">
                                                <div class="form-group">

                                                    <div class="col-md-12">
                                                        <label>Cardholder's name</label>
                                                        <input type="text" value="" class="form-control" name="cardholders_name">
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- End of Row -->


                                            <!-- Start of Row -->
                                            <div class="row mt15">
                                                <div class="form-group">

                                                    <div class="col-md-12">
                                                        <label>Total</label>
                                                        <input type="text" value="" class="form-control" name="total">
                                                        <input type="hidden" name="order" readonly>
                                                    </div>

                                                </div>
                                            </div>
                                            <!-- End of Row -->

                                            <!-- Start of Row -->
                                            <div class="row mt15">
                                                <div class="col-md-12">

                                                    <input type="submit" value="Submit" class="btn btn-blue btn-effect">

                                                </div>
                                            </div>
                                            <!-- End of Row -->

                                        </form>
                                        <!-- End of Form -->


                                    </div>
                                </div>
                            </div>
                            <!-- End of Accordion Body -->
                        </div>
                        <!-- ========== End of Panel 1 ========== -->

                    </div>
                    <!-- End of Panel Group -->

                </div>
                <!-- End of Accordion Check Out -->

            </div>
        </div>
    </section>
    <!-- ===== End of Shop Check Out Section ===== -->





    <!-- ===== Start of Get Started Section ===== -->
    <section class="get-started ptb40">
        <div class="container">
            <div class="row ">

                <!-- Column -->
                <div class="col-md-10 col-sm-9 col-xs-12">
                    <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
                </div>

                <!-- Column -->
                <div class="col-md-2 col-sm-3 col-xs-12">
                    <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
                </div>

            </div>
        </div>
    </section>
    <!-- ===== End of Get Started Section ===== -->$

@stop

@section('extra_scripts')
    <script>
        $(document).ready(function(){
            $("#paymentconfirmation").click();
        });
    </script>
@stop
