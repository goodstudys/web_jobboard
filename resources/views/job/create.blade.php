@extends('layouts.main')
@section('content')
<!-- ===== Start of Main Wrapper Section ===== -->
<section class="ptb80" id="post-job">
    <div class="container">

        <h3 class="uppercase text-blue">Post a Job
            <div class="text-right">
                <button class="btn btn-blue btn-effect">Your Balance : {{ $balance != null ? $balance->balance : 0 }}</button>
            </div>
        </h3>

        @if ($message = Session::get('success'))
        <div class="alert alert-success" style="margin-top: 10px;">
            <p>{{ $message }}</p>
        </div>
        @endif

        @if ($message = Session::get('error'))
            <div class="alert alert-danger" style="margin-top: 10px;">
                <p>{{ $message }}</p>
            </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger" style="margin-top: 10px;">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <!-- Start of Post Job Form -->
        <form action="{{ route('job.store') }}" enctype="multipart/form-data" method="post" class="post-job-resume mt50">
            <!-- Start of Job Details -->
            <div class="row">
                <div class="col-md-12">
                @csrf

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>your email</label>
                        <input class="form-control" type="email" name="email" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>job title</label>
                        <input class="form-control" type="text" name="title" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>location <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. "Paris, France"' name="location">
                        <span class="form-msg">Leave this blank if the Location is not important.</span>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>job type</label>
                        <select name="type" title="Choose Type" class="selectpicker" data-size="5" data-container="body"
                            required>
                            <option value="Full Time">Full Time</option>
                            <option value="Part Time">Part Time</option>
                            <option value="Freelance">Freelance</option>
                            <option value="Internship">Internship</option>
                            <option value="Temporary">Temporary</option>
                        </select>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>job category</label>
                        <select name="category_id" title="Choose Category" class="selectpicker" data-size="5"
                            data-container="body" required>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>job tags <span>(optional)</span></label>
                        <input class="form-control" type="text"
                            placeholder='e.g. Wordpress Developer, Android Developer' name="tags">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>job description <span>(optional)</span></label>
                        <textarea class="tinymce" name="description"></textarea>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>application email or website</label>
                        <input class="form-control" type="text" placeholder='Enter your email address or a website URL'
                            name="applicant_email_or_website" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>minimum rate per hour ($) <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. 10$' name="min_rate">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>maximum rate per hour ($) <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. 10$' name="max_rate">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>minimum salary ($) <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. 1000$' name="min_salary">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>maximum salary ($) <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. 5000$' name="max_salary">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>external link <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='http://apply-for-job.com' name="ext_link">
                    </div>

                    <!-- Form Group -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>header image <span>(optional)</span></label>

                                <!-- Upload Button -->
                                <div class="upload-file-btn">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input class="form-control" type="file" name="header_image" accept=".jpg,.png" id="imgInp">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <img id="blah" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                        </div>
                    </div>

                </div>
            </div>
            <!-- End of Job Details -->




            <!-- Start of Company Details -->
            <div class="row mt30">
                <div class="col-md-12">
                    <h3 class="capitalize pb20">company details</h3>

                    <!-- Form Group -->
                    <div class="form-group mt30">
                        <label>external link <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='http://apply-for-job.com'
                            name="company_ext_link">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>company name</label>
                        <input class="form-control" type="text" placeholder='Enter the name of your Company'
                            name="company_name" required>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>website <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='http://your-company-website.com'
                            name="company_website">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>tagline <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='Briefly describe your Company'
                            name="company_tagline">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>Video <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='e.g. youtube.com' name="company_video">
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label>Twitter username <span>(optional)</span></label>
                        <input class="form-control" type="text" placeholder='@yourcompany' name="company_twitter">
                    </div>

                    <!-- Form Group -->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>company logo <span>(optional)</span></label>

                                <!-- Upload Button -->
                                <div class="upload-file-btn">
                                    <span><i class="fa fa-upload"></i> Upload</span>
                                    <input type="file" name="company_logo" accept=".jpg,.png" id="imgInp1">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <img id="blah1" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group pt30 nomargin" id="last">
                        <button class="btn btn-blue btn-effect">submit</button>
                    </div>


                </div>
            </div>
            <!-- End of Company Details -->


        </form>
        <!-- End of Post Job Form -->

    </div>
</section>
<!-- ===== End of Main Wrapper Section ===== -->
@stop

@section('extra_scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });

        function readURL1(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah1').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp1").change(function() {
            readURL1(this);
        });
    </script>
@stop
