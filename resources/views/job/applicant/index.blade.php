@extends('layouts.main')

@section('content')
    <!-- =============== Start of Page Header 1 Section =============== -->
    <section class="page-header" id="find-candidate">
        <div class="container">

            <!-- Start of Page Title -->
            <div class="row">
                <div class="col-md-12">
                    <h2>list applicant</h2>
                </div>
            </div>
            <!-- End of Page Title -->

            <!-- Start of Breadcrumb -->
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="#">home</a></li>
                        <li class="active">job</li>
                    </ul>
                </div>
            </div>
            <!-- End of Breadcrumb -->

        </div>
    </section>
    <!-- =============== End of Page Header 1 Section =============== -->





    <!-- ===== Start of Main Wrapper Section ===== -->
    <section class="find-candidate ptb80">
        <div class="container">

            <!-- Start of Form -->
            <form class="row" action="#" method="get">

                <!-- Start of keywords input -->
                <div class="col-md-6 col-md-offset-2 col-sm-6 col-sm-offset-2 col-xs-8">
                    <label for="search-keywords">Keywords</label>
                    <input type="text" name="search-keywords" id="search-keywords" class="form-control" placeholder="Keywords">
                </div>

                <!-- Start of submit input -->
                <div class="col-md-2 col-sm-2 col-xs-4">
                    <button type="submit" class="btn btn-blue btn-effect"><i class="fa fa-search"></i>search</button>
                </div>

            </form>
            <!-- End of Form -->


            <!-- Start of Row -->
            <div class="row mt60">

                <!-- Start of Candidate Main -->
                <div class="col-md-12 candidate-main">

                    <!-- Start of Candidates Wrapper -->
                    <div class="candidate-wrapper">

                        <!-- ===== Start of Single Candidate 1 ===== -->
                        @foreach($applicants as $applicant)
                            <div class="single-candidate row nomargin">

                                <!-- Candidate Image -->
                                <div class="col-md-2 col-xs-3">
                                    <div class="candidate-img">
                                        <a href="candidate-profile-1.html">
                                            <img src="{{ asset('/upload/users/profile/photo/'. $applicant->image) }}" class="img-responsive" alt="">
                                        </a>
                                    </div>
                                </div>

                                <!-- Start of Candidate Name & Info -->
                                <div class="col-md-8 col-xs-6 ptb20">

                                    <!-- Candidate Name -->
                                    <div class="candidate-name">
                                        <a href="{{ route('applicant.profile.show', $applicant->jobappluser) }}"><h5>{{ $applicant->name }}</h5></a>
                                    </div>

                                    <!-- Candidate Info -->
                                    <div class="candidate-info mt5">
                                        <ul class="list-inline">
                                            <li>
                                                <span><i class="fa fa-money"></i>Rp. {{ number_format($applicant->expected_salary) }}</span>
                                            </li>

                                            <li>
                                                <span><i class="fa fa-map-marker"></i>{{ $applicant->address }}</span>
                                            </li>

                                            <li>
                                                <span><i class="fa fa-briefcase"></i>{{ $applicant->profession }}</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- End of Candidate Name & Info -->

                                <!-- CTA -->
                                <div class="col-md-2 col-xs-3">
                                    <div class="candidate-cta ptb30">
                                        <a href="candidate-profile-1.html" class="btn btn-blue btn-small btn-effect">hire me</a>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                        <!-- ===== End of Single Candidate 1 ===== -->

                    </div>
                    <!-- End of Candidates Wrapper -->

                    <!-- Start of Pagination -->
                    <div class="col-md-12 mt10">
                        {{ $applicants->links('partials._paginate')}}
                    </div>
                    <!-- End of Pagination -->

                </div>
                <!-- End of Candidate Main -->

            </div>
            <!-- End of Row -->

        </div>
    </section>
    <!-- ===== End of Main Wrapper Section ===== -->
@stop
