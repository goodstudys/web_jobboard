<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">Simplyyours</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">CR</a>
        </div>
        <ul class="sidebar-menu">
            <li><a class="nav-link" href="#"><i class="fas fa-fire"></i><span>Dashboard</span></a></li>
            <li><a class="nav-link" href="{{ route('order.index') }}"><i class="fab fa-first-order-alt"></i><span>Order</span></a></li>
            <li class="menu-header">Management Web</li>
            <li><a class="nav-link" href="{{ route('sliders.index') }}"><i class="fas fa-columns"></i><span>Slider</span></a></li>
            <li><a class="nav-link" href="{{ route('category.index') }}"><i class="fas fa-ellipsis-h"></i><span>Category</span></a></li>
            <li><a class="nav-link" href="{{ route('pricing.index') }}"><i class="fas fa-money-check-alt"></i><span>Pricing</span></a></li>
            <li><a class="nav-link" href="{{ route('aboutUs.index') }}"><i class="fas fa-users"></i><span>About Us Content</span></a></li>
            <li><a class="nav-link" href="{{ route('faq.index') }}"><i class="fas fa-tasks"></i><span>Faq Content</span></a></li>
            <li><a class="nav-link" href="{{ route('privacyPolice.index') }}"><i class="fas fa-lock"></i><span>Privacy Policy Content</span></a></li>
            <li class="menu-header">Management User</li>
            <li><a class="nav-link" href="{{ route('candidate.index') }}"><i class="fas fa-user"></i><span>Candidates</span></a></li>
            <li><a class="nav-link" href="{{ route('resume.indexDashboard') }}"><i class="fas fa-file"></i><span>Resume Candidate</span></a></li>
            <li><a class="nav-link" href="{{ route('company.index') }}"><i class="fas fa-building"></i><span>Companies</span></a></li>
            <li><a class="nav-link" href="{{ route('jobs.index') }}"><i class="fab fa-black-tie"></i><span>Jobs</span></a></li>
        </ul>

        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
            <a href="{{ route('home.index') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Go to Website
            </a>
        </div>
    </aside>
</div>
