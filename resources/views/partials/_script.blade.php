<script src="{{ asset('js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
<script src="{{ asset('js/swiper.min.js') }}"></script>
<script src="{{ asset('js/jquery.ajaxchimp.js') }}"></script>
<script src="{{ asset('js/jquery.countTo.js') }}"></script>
<script src="{{ asset('js/jquery.inview.min.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/jquery.easypiechart.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('js/countdown.js') }}"></script>
<script src="{{ asset('js/isotope.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@yield('extra_scripts')