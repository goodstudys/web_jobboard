<div class="cd-user-modal">
    <!-- this is the entire modal form, including the background -->
    <div class="cd-user-modal-container">
        <!-- this is the container wrapper -->
        <ul class="cd-switcher nav-tabs">
            <li class="active"><a href="#cd-login" data-toggle="tab">Sign in</a></li>
            <li><a href="#cd-signup" data-toggle="tab">New account</a></li>
        </ul>
        <div class="tab-content" id="tabs">
            <div id="cd-login" class="tab-pane">
                <!-- log in form -->
                <form class="cd-form" method="POST" action="{{ route('login') }}">
                    @csrf
                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signin-email">E-mail</label>
                        <input class="full-width has-padding has-border @error('email') is-invalid @enderror"
                            id="signin-email" type="email" placeholder="E-mail" name="email" value="{{ old('email') }}">
    
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signin-password">{{ __('Password') }}</label>
                        <input class="full-width has-padding has-border @error('password') is-invalid @enderror"
                            id="signin-password" type="password" placeholder="Password" name="password">
    
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="fieldset">
                        <input type="checkbox" id="remember-me" {{ old('remember') ? 'checked' : '' }}>
                        <label for="remember-me">Remember me</label>
                    </p>
                    <p class="fieldset">
                        <button type="submit" value="Login" class="btn btn-blue btn-effect"
                            style="background: #29b1fd;">Login</button>
                    </p>
                </form>
            </div>
            <!-- cd-login -->
    
            <div id="cd-signup" class="tab-pane">
                <!-- sign up form -->
                <form class="cd-form" method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <input name="level" value="5" style="display: none">
                    <input name="type" value="modal" style="display: none">
                    <p class="fieldset">
                        <label class="image-replace cd-username" for="signup-username">Username</label>
                        <input class="full-width has-padding has-border" id="signup-username" name="name" type="text"
                            placeholder="Username" required>
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signup-email">E-mail</label>
                        <input class="full-width has-padding has-border" id="signup-email"
                            pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" name="email" type="email"
                            placeholder="E-mail" required>
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signup-password">Password</label>
                        <input class="full-width has-padding has-border" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                            id="signup-password" name="password" type="password" placeholder="Password" required>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </p>
                    <p class="fieldset">
                        <input type="checkbox" id="accept-terms" name="checkbox" required>
                        {{-- <input type="text" name="zz" id="acceptterms"> --}}
                        <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
                        <label id="describeRegister"></label>
                        <p id="describeRegister"></p>
                        {{-- <div class="text-danger pt20" style="padding-top: 20px;color: red" id="describeRegister">
                        </div> --}}
                    </p>
                    <p class="fieldset">
                        <button class="btn btn-blue btn-effect"
                            onclick="if(!this.form.checkbox.checked){alert('You must agree to the terms first.');return false}"
                            type="submit" value="Create account" style="background: #29b1fd;">Create Account</button>
                    </p>
                </form>
            </div>
            <!-- cd-signup -->
        </div>

    </div>
    <!-- cd-user-modal-container -->
</div>
<!-- cd-user-modal -->

{{-- <!-- Side Modal Top Right Success-->
<div class="modal fade right" id="modalCoupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-side modal-top-right modal-notify modal-success" role="document">
        <!--Content-->
        <div class="modal-content">
            <!--Header-->
            <div class="modal-header">
                <p class="heading lead">Modal Success</p>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="white-text">&times;</span>
                </button>
            </div>

            <!--Body-->
            <div class="modal-body">
                <div class="text-center">
                    <i class="fa fa-check fa-4x mb-3 animated rotateIn"></i>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit iusto nulla aperiam blanditiis
                        ad consequatur in dolores culpa, dignissimos, eius non possimus fugiat. Esse ratione fuga,
                        enim, ab officiis totam.
                    </p>
                </div>
            </div>

            <!--Footer-->
            <div class="modal-footer justify-content-center">
                <a role="button" class="btn btn-primary-modal">Get it now <i class="fa fa-diamond ml-1"></i></a>
                <a role="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">No,
                    thanks</a>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Side Modal Top Right Success--> --}}
<!-- Modal -->
<div class="modal fade" id="modalCoupon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">Login Failed!</h4>
            </div>
            <div class="modal-body">
                <p class="fieldset">
                    @error('email')
                    {{-- <label class="image-replace cd-email" for="signup-email">E-mail</label><br> --}}
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </p>
                <p class="fieldset">

                    @error('password')
                    {{-- <label class="image-replace cd-password" for="signup-password">Password</label><br> --}}
                    <span class="invalid-feedback" role="alert" style="color: red">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-blue" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->