<!-- =============== Start of Header 4 Navigation =============== -->
<header class="sticky">
    <nav class="navbar navbar-default navbar-static-top fluid_header centered">
        <div class="container">

            <!-- Logo -->
            <div class="col-md-2 col-sm-6 col-xs-8 nopadding">
                <a class="navbar-brand nomargin" href="{{ route('home.index') }}"><img src="{{ asset('images/logoJobboard.svg') }}" alt="logo" style="height: 38px;"></a>
                <!-- INSERT YOUR LOGO HERE -->
            </div>

            <!-- ======== Start of Main Menu ======== -->
            <div class="col-md-10 col-sm-6 col-xs-4 nopadding">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle toggle-menu menu-right push-body" data-toggle="collapse" data-target="#main-nav" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Start of Main Nav -->
                <div class="collapse navbar-collapse cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="main-nav">
                    <ul class="nav navbar-nav pull-right">

                        <!-- Mobile Menu Title -->
                        <li class="mobile-title">
                            <h4>main menu</h4></li>

                        <!-- Simple Menu Item -->
                        <li class="simple-menu">
                            <a href="{{ route('home.index') }}" role="button">home</a>
                        </li>

                        <li class="simple-menu">
                            <a href="{{ route('searchJob.index') }}" role="button">jobs</a>
                        </li>

                        <li class="simple-menu">
                            <a href="{{ route('companies.index') }}" role="button">companies</a>
                        </li>

                        <li class="simple-menu">
                            <a href="{{ route('pricings.index') }}" role="button">pricing</a>
                        </li>
                        @if (\Illuminate\Support\Facades\Auth::check())
                            @if (\Illuminate\Support\Facades\Auth::user()->level == 5)
                                <li class="simple-menu">
                                    <a href="{{ route('resume.index') }}" role="button">resume</a>
                                </li>

                                <li class="simple-menu">
                                    <a href="{{ route('candidate.profile.show', Auth::user()->id) }}" role="button">profile</a>
                                </li>
                            @endif

                            @if (\Illuminate\Support\Facades\Auth::user()->level == 3)
                                <li class="simple-menu">
                                    <a href="{{ route('job.create') }}" role="button">Post A Job</a>
                                </li>

                                <li class="simple-menu">
                                    <a href="{{ route('profile.show', Auth::user()->id) }}" role="button">company profile</a>
                                </li>
                            @endif
                        @endif

{{--                        <!-- Simple Menu Item -->--}}
{{--                        <li class="dropdown simple-menu">--}}
{{--                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">For Candidates<i class="fa fa-angle-down"></i></a>--}}
{{--                            <ul class="dropdown-menu" role="menu">--}}
{{--                                <li><a href="search-jobs-1.html">search jobs 1</a></li>--}}
{{--                                <li><a href="search-jobs-2.html">search jobs 2</a></li>--}}
{{--                                <li><a href="search-jobs-3.html">search jobs 3</a></li>--}}
{{--                                <li><a href="search-jobs-4.html">search jobs 4</a></li>--}}
{{--                                <li><a href="submit-resume.html">submit resume</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}

{{--                        <!-- Simple Menu Item -->--}}
{{--                        <li class="dropdown simple-menu">--}}
{{--                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">for employers<i class="fa fa-angle-down"></i></a>--}}
{{--                            <ul class="dropdown-menu" role="menu">--}}
{{--                                <li><a href="find-candidate-1.html">find a candidate 1</a></li>--}}
{{--                                <li><a href="find-candidate-2.html">find a candidate 2</a></li>--}}
{{--                                <li><a href="post-job.html">post a job</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}

{{--                        <!-- Simple Menu Item -->--}}
{{--                        <li class="dropdown simple-menu active">--}}
{{--                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">elements<i class="fa fa-angle-down"></i></a>--}}
{{--                            <ul class="dropdown-menu">--}}
{{--                                <!-- Dropdown Submenu -->--}}
{{--                                <li class="dropdown-submenu">--}}
{{--                                    <a href="#">headers<i class="fa fa-angle-right"></i></a>--}}
{{--                                    <ul class="dropdown-menu">--}}
{{--                                        <li><a href="header1.html">header 1 - default</a></li>--}}
{{--                                        <li><a href="header2.html">header 2 - logo top</a></li>--}}
{{--                                        <li><a href="header3.html">header 3 - top bar</a></li>--}}
{{--                                        <li><a href="header4.html">header 4 - sticky</a></li>--}}
{{--                                    </ul>--}}
{{--                                </li>--}}

{{--                                <!-- Dropdown Submenu -->--}}
{{--                                <li class="dropdown-submenu">--}}
{{--                                    <a href="#">footers<i class="fa fa-angle-right"></i></a>--}}
{{--                                    <ul class="dropdown-menu">--}}
{{--                                        <li><a href="footer1.html">default</a></li>--}}
{{--                                        <li><a href="footer2.html">light</a></li>--}}
{{--                                        <li><a href="footer3.html">dark</a></li>--}}
{{--                                        <li><a href="footer4.html">simple</a></li>--}}
{{--                                    </ul>--}}
{{--                                </li>--}}

{{--                                <!-- Dropdown Submenu -->--}}
{{--                                <li class="dropdown-submenu">--}}
{{--                                    <a href="#">page headers<i class="fa fa-angle-right"></i></a>--}}
{{--                                    <ul class="dropdown-menu">--}}
{{--                                        <li><a href="page-header1.html">default</a></li>--}}
{{--                                        <li><a href="page-header2.html">light</a></li>--}}
{{--                                        <li><a href="page-header3.html">dark</a></li>--}}
{{--                                        <li><a href="page-header4.html">parallax</a></li>--}}
{{--                                    </ul>--}}
{{--                                </li>--}}

{{--                                <li><a href="buttons.html">buttons</a></li>--}}
{{--                                <li><a href="pricing-tables.html">pricing tables</a></li>--}}
{{--                                <li><a href="typography.html">typography</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}

                        <!-- Login Menu Item -->
                        @if (\Illuminate\Support\Facades\Auth::check())
                            @if (\Illuminate\Support\Facades\Auth::user()->level == 3)
                            @endif
                            @if (\Illuminate\Support\Facades\Auth::user()->level == 1 || \Illuminate\Support\Facades\Auth::user()->level == 2)
                                <li class="logout-btn">
                                    <a href="{{ route('dashboard.home.index') }}">Dashboard
                                    </a>
                                </li>
                            @else
                                <li class="logout-btn">
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            @endif
                        @else
                            <li class="menu-item login-btn">
                                <a id="modal_trigger" href="javascript:void(0)" role="button"><i class="fa fa-lock"></i>login</a>
                            </li>
                        @endif

                    </ul>
                </div>
                <!-- End of Main Nav -->
            </div>
            <!-- ======== End of Main Menu ======== -->

        </div>
    </nav>
</header>
<!-- =============== End of Header 4 Navigation =============== -->
