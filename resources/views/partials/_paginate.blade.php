@if ($paginator->hasPages())
    <ul class="pagination list-inline text-center">
        @foreach ($elements as $element)
            @if (is_string($element))
                <li class="page-number disabled"><span class="page-number ">{{ $element }}</span></li>
            @endif
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><a href="javascript:void(0)">{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}">Next</a></li>
        @else
            <li><a href="#" disabled="">Next</a></li>
        @endif
    </ul>
@endif
