<footer class="footer2">

    <!-- ===== Start of Footer Information & Links Section ===== -->
    <div class="footer-info ptb80">
        <div class="container">

            <!-- 1st Footer Column -->
            <div class="col-md-3 col-sm-6 col-xs-6 footer-about">

                <!-- Your Logo Here -->
                <a href="{{route('home.index')}}">
                    <img src="{{asset('images/logoJobboard.svg')}}" alt="">
                </a>

                <!-- Small Description -->
                <p class="pt40">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                    has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type changed.</p>

                <!-- Info -->
                <ul class="nopadding">
                    <li><i class="fa fa-map-marker"></i>New York City, USA</li>
                    <li><i class="fa fa-phone"></i>(123) 456 789 0012</li>
                    <li><i class="fa fa-envelope-o"></i>youremail@simplyyours.com</li>
                </ul>
            </div>

            <!-- 2nd Footer Column -->
            <div class="col-md-3 col-sm-6 col-xs-6 footer-links">
                <h3>useful links</h3>

                <!-- Links -->
                <ul class="nopadding">
                    <li><a href="{{ route('searchJob.index') }}"><i class="fa fa-angle-double-right"></i>find jobs</a>
                    </li>
                    <li><a href="{{ route('webFaq.index') }}"><i class="fa fa-angle-double-right"></i>FAQ</a></li>
                    @if (Auth::guest())
                    <li><a href="{{ route('login') }}"><i class="fa fa-angle-double-right"></i>login</a></li>
                    <li><a href="{{ route('register') }}"><i class="fa fa-angle-double-right"></i>register</a></li>
                    <li><a href="{{ route('password.request') }}"><i class="fa fa-angle-double-right"></i>Forgot
                            password</a></li>
                    @endif
                    <li><a href="{{ route('privacyPolicy.index') }}"><i class="fa fa-angle-double-right"></i>privacy
                            policy</a></li>
                    <li><a href="{{ route('aboutUsWeb.index') }}"><i class="fa fa-angle-double-right"></i>About Us</a>
                    </li>

                </ul>
            </div>

            <!-- 3rd Footer Column -->
            <div class="col-md-3 col-sm-6 col-xs-6 footer-posts">
            </div>

            <!-- 4th Footer Column -->
            <div class="col-md-3 col-sm-6 col-xs-6 footer-newsletter">
                <h3>newsletter</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                    industry's standard dummy text ever since the 1500s.</p>

                <!-- Subscribe Form -->
                <form action="#" class="form-inline mailchimp mtb30" novalidate>

                    <!-- Form -->
                    <div class="form-group">
                        <div class="input-group">
                            <input type="email" name="EMAIL" class="form-control" id="mc-email" placeholder="Your Email"
                                autocomplete="off">
                            <label for="mc-email"></label>
                            <button type="submit" class="btn btn-blue btn-effect">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
    <!-- ===== End of Footer Information & Links Section ===== -->


    <!-- ===== Start of Footer Copyright Section ===== -->
    <div class="copyright ptb40">
        <div class="container">

            <div class="col-md-6 col-sm-6 col-xs-12">
                <span>Copyright &copy; <a href="http://mygoodnews.id/">Goodnews</a>. All Rights Reserved</span>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12">
                <!-- Start of Social Media Buttons -->
                <ul class="social-btns list-inline text-right">
                    <!-- Social Media -->
                    <li>
                        <a href="#" class="social-btn-roll facebook">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-facebook"></i>
                                <i class="social-btn-roll-icon fa fa-facebook"></i>
                            </div>
                        </a>
                    </li>

                    <!-- Social Media -->
                    <li>
                        <a href="#" class="social-btn-roll twitter">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-twitter"></i>
                                <i class="social-btn-roll-icon fa fa-twitter"></i>
                            </div>
                        </a>
                    </li>

                    <!-- Social Media -->
                    <li>
                        <a href="#" class="social-btn-roll google-plus">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-google-plus"></i>
                                <i class="social-btn-roll-icon fa fa-google-plus"></i>
                            </div>
                        </a>
                    </li>

                    <!-- Social Media -->
                    <li>
                        <a href="#" class="social-btn-roll instagram">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-instagram"></i>
                                <i class="social-btn-roll-icon fa fa-instagram"></i>
                            </div>
                        </a>
                    </li>

                    <!-- Social Media -->
                    <li>
                        <a href="#" class="social-btn-roll linkedin">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-linkedin"></i>
                                <i class="social-btn-roll-icon fa fa-linkedin"></i>
                            </div>
                        </a>
                    </li>

                    <!-- Social Media -->
                    <li>
                        <a href="#" class="social-btn-roll rss">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-rss"></i>
                                <i class="social-btn-roll-icon fa fa-rss"></i>
                            </div>
                        </a>
                    </li>
                </ul>
                <!-- End of Social Media Buttons -->
            </div>

        </div>
    </div>
    <!-- ===== End of Footer Copyright Section ===== -->

</footer>