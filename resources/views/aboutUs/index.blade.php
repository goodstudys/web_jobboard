@extends('layouts.main')
@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>about us</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">pages</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of About Us Section ===== -->
<section class="about-us ptb80">
    <div class="container">
        @if ($aboutUs == null)
            <h1>Oops! there is nothing here</h1>
        @else
            <div class="row">

                <!-- Start of First Column -->
                <div class="col-md-6 col-xs-12">
                    <h3 class="text-blue">About Simplyyours</h3>
                    <p class="pt30">{!! preg_replace('#<script(.*?)>(.*?)</script(.*?)>#is', '', $aboutUs->text) !!}</p>

                </div>
                <!-- End of First Column -->

                <!-- Start of Second Column -->
                <div class="col-md-6 col-xs-12 about-video">
                    <!-- Vimeo Video -->
                    {{-- <video width="600" controls>
                        <source src="{{asset('/upload/aboutus/'.$aboutUs->video)}}" type="video/mp4">
                    Your browser does not support HTML5 video.
                    </video> --}}
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="{{asset('/upload/aboutus/'.$aboutUs->video)}}"
                                allowfullscreen=""></iframe>
                    </div>
                </div>
                <!-- End of Second Column -->

            </div>
        @endif
    </div>
</section>
<!-- ===== End of About Us Section ===== -->





<!-- ===== Start of About Process Section ===== -->
<section class="about-process ptb80">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="section-title">
                    <h2>how it works</h2>
                </div>
            </div>

            <!-- Start of First Column -->
            <div class="col-md-4 col-xs-12 text-center">
                <div class="process-icon">
                    <i class="fa fa-pencil" aria-hidden="true"></i>
                </div>

                <h5 class="uppercase text-blue pt40">step 1</h5>
                <h3 class="pb20">Register</h3>
                <p>Start by creating an account on our awesome platform</p>
            </div>
            <!-- End of First Column -->


            <!-- Start of Second Column -->
            <div class="col-md-4 col-xs-12 text-center">
                <div class="process-icon">
                    <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                </div>

                <h5 class="uppercase text-blue pt40">step 2</h5>
                <h3 class="pb20">Submit Resume</h3>
                <p>Fill out our forms and submit your resume right away</p>
            </div>
            <!-- End of Second Column -->


            <!-- Start of Third Column -->
            <div class="col-md-4 col-xs-12 text-center">
                <div class="process-icon">
                    <i class="fa fa-money" aria-hidden="true"></i>
                </div>

                <h5 class="uppercase text-blue pt40">step 3</h5>
                <h3 class="pb20">Start Working</h3>
                <p>Start your new career by working with one of the most successful companies</p>
            </div>
            <!-- End of Third Column -->

        </div>
    </div>
</section>
<!-- ===== End of About Process Section ===== -->





<!-- ===== Start of FAQ Section ===== -->
<section class="ptb80" id="faq-page">
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="section-title">
                    <h2>frequently asked questions</h2>
                </div>
            </div>
            @foreach ($faq as $index => $item)
            <!-- Start of Topic 1 -->
            <div class="col-md-12 topic">
                <!-- Question -->
                <div class="open">
                    <h6 class="question" data-search-term="{{$index+1}}. {{$item->title}}">{{$index+1}}.
                        {{$item->title}}</h6>
                    <i class="fa fa-angle-down hidden-xs"></i>
                </div>

                <!-- Answer -->
                <p class="answer" style="display: none;">{{$item->description}}</p>
            </div>
            <!-- End of Topic 1 -->
            @endforeach

        </div>
    </div>
</section>
<!-- ===== End of FAQ Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->

@stop
@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop
