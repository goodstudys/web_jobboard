@extends('layouts.main')
@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>search jobs</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">jobs</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of Main Wrapper Section ===== -->
<section class="search-jobs ptb80" id="version2">
    <div class="container">

        <!-- Start of Row -->
        <div class="row">

            <!-- ===== Start of Job Sidebar ===== -->
            <div class="col-md-4 col-xs-12 job-post-sidebar">
                <form action="{{route('searchcat.index')}}" method="POST">
                    @csrf
                    <!-- Search Location -->
                    <div class="search-location">
                        <input type="text" name="searchlocation" class="form-control" placeholder="Location">
                    </div>
                    @if (count($feeder) != 0)
                    <!-- Job Types -->
                    <div class="job-types mt30">
                        <h4>Job Type</h4>

                        <ul class="list-inline mt20">
                            <li>

                                @if ($fulltime == true)
                                <input type="checkbox" name="fullTime" value="1" id="full" checked>
                                <label for="full">Full Time</label>
                                @else
                                <input type="checkbox" name="fullTime" value="1" id="fullTime">
                                <label for="fullTime">Full Time</label>
                                @endif


                            </li>
                            <li>
                                @if ($parttime == true)
                                <input type="checkbox" name="partTime" value="2" id="part" checked>
                                <label for="part">Part Time</label>
                                @else
                                <input type="checkbox" name="partTime" value="2" id="partTime">
                                <label for="partTime">Part Time</label>
                                @endif

                            </li>
                            <li>
                                @if ($freelance == true)
                                <input type="checkbox" name="freelance" value="3" id="free" checked>
                                <label for="free">Freelance</label>
                                @else
                                <input type="checkbox" name="freelance" value="3" id="freelance">
                                <label for="freelance">Freelance</label>
                                @endif

                            </li>
                            <li>
                                @if ($intership == true)
                                <input type="checkbox" id="intership" value="4" name="inter" checked>
                                <label for="inter">Intership</label>
                                @else
                                <input type="checkbox" id="intership" value="4" name="intership">
                                <label for="intership">Intership</label>
                                @endif
                            </li>
                            <li>
                                @if ($temporary == true)
                                <input type="checkbox" id="temporary" value="5" name="temp" checked>
                                <label for="temp">Temporary</label>
                                @else
                                <input type="checkbox" id="temporary" value="5" name="temporary">
                                <label for="temporary">Temporary</label>
                                @endif
                            </li>
                        </ul>
                    </div>
                    @else
                    <!-- Job Types -->
                    <div class="job-types mt30">
                        <h4>Job Type</h4>

                        <ul class="list-inline mt20">
                            <li>
                                <input type="checkbox" name="fullTime" value="1" id="fullTime">
                                <label for="fullTime">Full Time</label>
                            </li>
                            <li>
                                <input type="checkbox" name="partTime" value="2" id="partTime">
                                <label for="partTime">Part Time</label>
                            </li>
                            <li>
                                <input type="checkbox" name="freelance" value="3" id="freelance">
                                <label for="freelance">Freelance</label>
                            </li>
                            <li>
                                <input type="checkbox" id="intership" value="4" name="intership">
                                <label for="intership">Intership</label>
                            </li>
                            <li>
                                <input type="checkbox" id="temporary" value="5" name="temporary">
                                <label for="temporary">Temporary</label>
                            </li>
                        </ul>
                    </div>
                    @endif

                    <!-- Job Types -->
                    @if ($categories != null)
                    <div class="job-categories mt30">
                        <h4 class="pb20">Categories</h4>

                        <select name="searchcategories" class="selectpicker" id="search-categories"
                            data-live-search="true" title="Any Category" data-size="5" data-container="body">
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    @endif
                    <!-- Job Types -->
                    {{-- <div class="job-types mt30">

                    <ul class="list-inline">
                        <li>
                            <input type="checkbox" id="salary">
                            <label for="salary">Filter by Salary</label>
                        </li>

                        <li>
                            <input type="checkbox" id="rate">
                            <label for="rate">filter by Rate</label>
                        </li>
                    </ul>
                </div> --}}
                    <div class="job-types mt30 mb30">

                        <ul class="list-inline">
                            <button type="submit" class="btn btn-blue btn-effect pull-center"> Search</button>
                        </ul>
                    </div>


                    <!-- Advertisment -->
                    <div class="job-advert mt30">
                        <a href="#">
                            <img src="{{asset('images/img/advert.jpg')}}" class="img-responsive" alt="">
                        </a>
                    </div>
                </form>
            </div>
            <!-- ===== End of Job Sidebar ===== -->


            <!-- ===== Start of Job Post Main ===== -->
            <div class="col-md-8 col-xs-12 job-post-main">
                <h4>We found {{count($jumlah)}} matches.</h4>

                <!-- Start of Job Post Wrapper -->
                <div class="job-post-wrapper mt20">
                    @foreach ($jobs as $item)
                    <!-- ===== Start of Single Job Post 1 ===== -->
                    <div class="single-job-post row nomargin">
                        <!-- Job Company -->
                        <div class="col-md-2 col-xs-3 nopadding">
                            <div class="job-company">
                                <a href="#">
                                    {{-- @if ($item->company_logo != null)
                                    @foreach ($profile as $prof)
                                    @if ($item->user_id == $prof->id)
                                    <img src="{{ asset('upload/users/company/'.$prof->image) }}" width="65" alt="">
                                    @endif
                                    @endforeach
                                    @else
                                    <img src="{{asset('upload/users/company/job/logo/'.$item->company_logo)}}"
                                        width="65" alt="">
                                    @endif --}}
                                    @if ($item->company_logo != null)
                                    <img src="{{asset('upload/users/company/job/logo/'.$item->company_logo)}}"
                                        width="45" alt="">
                                    @endif
                                </a>
                            </div>
                        </div>

                        <!-- Job Title & Info -->
                        <div class="col-md-8 col-xs-6 ptb20">
                            <div class="job-title">
                                <a href="{{route('job.show',$item->id)}}">{{$item->title}}</a>
                            </div>

                            <div class="job-info">
                                @if ($item->company_name != null)
                                <span class="company"><i class="fa fa-building-o"></i>{{$item->company_name}}</span>
                                @endif
                                @if ($item->location != null)
                                <span class="location"><i class="fa fa-map-marker"></i>{{$item->location}}</span>
                                @endif
                            </div>
                        </div>

                        <!-- Job Category -->
                        <div class="col-md-2 col-xs-3 ptb30">
                            <div class="job-category">
                                @if ($item->type == 'Full Time')
                                <a href="javascript:void(0)" class="btn btn-green btn-small btn-effect">full time</a>
                                @elseif ($item->type =="Part Time")
                                <a href="javascript:void(0)" class="btn btn-purple btn-small btn-effect">part time</a>
                                @elseif ($item->type =="Freelance")
                                <a href="javascript:void(0)" class="btn btn-blue btn-small btn-effect">freelancer</a>
                                @elseif ($item->type =="Internship")
                                <a href="javascript:void(0)" class="btn btn-orange btn-small btn-effect">intership</a>
                                @elseif ($item->type =="Temporary")
                                <a href="javascript:void(0)" class="btn btn-red btn-small btn-effect">temporary</a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!-- ===== End of Single Job Post 1 ===== -->
                    @endforeach


                </div>
                <!-- End of Job Post Wrapper -->

                <!-- Start of Pagination -->
                {{$jobs->links('searchJob.pagin')}}
                {{-- <ul class="pagination list-inline text-center">
                    <li class="active"><a href="javascript:void(0)">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">Next</a></li>
                </ul> --}}
                <!-- End of Pagination -->

            </div>
            <!-- ===== End of Job Post Main ===== -->

        </div>
        <!-- End of Row -->

    </div>
</section>
<!-- ===== End of Main Wrapper Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@stop

@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop