@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
          <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Create About Us Content</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">About Us Content</a></div>
          <div class="breadcrumb-item">Create About Us Content</div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Create About Us Content</h2>
        <p class="section-lead">
          On this page you can create a about us content and fill in all fields.
        </p>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Write Your Content</h4>
              </div>
              <div class="card-body">
                  <form action="{{ route('aboutUs.store') }}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                          <div class="col-sm-12 col-md-7">
                              <input type="text" class="form-control" value="About Us" readonly>
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                          <div class="col-sm-12 col-md-7">
                            <textarea class="form-control" style="height: 200px;" name="text"></textarea>
                              {{-- <textarea class="summernote-simple" name="text"></textarea> --}}
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Video</label>
                          <div class="col-sm-12 col-md-7">
                              <input type="file" name="video"/>
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                          <div class="col-sm-12 col-md-7">
                              <button class="btn btn-primary">Create Content</button>
                          </div>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
