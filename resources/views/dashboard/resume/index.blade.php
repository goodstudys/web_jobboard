@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>List Resume</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item"><a href="#">List Resume</a></div>
                {{--                    <div class="breadcrumb-item">DataTables</div>--}}
            </div>
        </div>

        @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
        @endif
        @if ($message = Session::get('error'))
        <div class="alert alert-info">
            <p>{{ $message }}</p>
        </div>
        @endif
        <div class="section-body">
            <h2 class="section-title">List Resume
            </h2>
            <p class="section-lead">
                List User Resume
            </p>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>List Resume</h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-striped" id="table-1">
                                    <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Name</th>
                                            <th>Profession</th>
                                            <th>Email</th>
                                            <th>Address According To ID (KTP)</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($resume as $index => $item)
                                        <tr>
                                            <td class="text-center">{{ $index+1 }}</td>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->profession }}</td>
                                            <td>{{ $item->email }}</td>
                                            <td>{{ $item->addressID }}</td>
                                            <td>
                                                <form action="{{ route('resume.destroy', $item->id) }}"
                                                    method="post">
                                                    <a href="{{ route('resume.showDashboard', $item->id) }}"
                                                        class="btn btn-info">View</a>

                                                    @csrf
                                                    @method('DELETE')

                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>

                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop