@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>Resume</h1>
            <div class="section-header-breadcrumb">
                <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                <div class="breadcrumb-item">List Resume</div>
                <div class="breadcrumb-item">Show Resume</div>
            </div>
        </div>
        <div class="section-body">
            <h2 class="section-title">Hi, {{$resume->name}}!</h2>

            <div class="row mt-sm-4">
                <div class="col-12 col-md-12 col-lg-5">
                    <div class="card profile-widget">
                        <div class="profile-widget-header">
                            <img alt="image" src="{{ asset('/upload/users/photo/'.$resume->photo) }}"
                                class="rounded-circle profile-widget-picture">
                        </div>
                        <div class="profile-widget-description">
                            <div class="profile-widget-name">{{$resume->name}} <div
                                    class="text-muted d-inline font-weight-normal">
                                    <div class="slash"></div> {{$resume->profession}}
                                </div>
                            </div>
                            {!!$resume->content!!}
                        </div>
                        <div class="card-footer text-center">
                            <a href="{{$resume->facebook}}" class="btn btn-social-icon btn-facebook mr-1">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <a href="{{$resume->twitter}}" class="btn btn-social-icon btn-twitter mr-1">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#" class="btn btn-social-icon btn-github mr-1">
                                <i class="fab fa-github"></i>
                            </a>
                            <a href="{{$resume->instagram}}" class="btn btn-social-icon btn-instagram">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-12 col-lg-7">
                    <div class="card">
                        <form method="post" class="needs-validation" novalidate="">
                            <div class="card-header">
                                <h4>Resume Data</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6 col-12">
                                        <label>Name</label>
                                        <input type="text" class="form-control" value="{{$resume->name}}" required="">
                                    </div>
                                    <div class="form-group col-md-6 col-12">
                                        <label>Username</label>
                                        <input type="text" class="form-control" value="{{$resume->name}}" required="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-7 col-12">
                                        <label>Email</label>
                                        <input type="email" class="form-control" value="{{$resume->email}}" required="">
                                    </div>
                                    <div class="form-group col-md-5 col-12">
                                        <label>Skills</label>
                                        <input type="tel" class="form-control" value="{{$resume->skills}}">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>Address According To ID (KTP)</label>
                                        <textarea class="form-control"
                                            style="height: 200px;">{{$resume->addressID}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form method="post" class="needs-validation" novalidate="">
                            <div class="card-header">
                                <h4>Education Section</h4>
                            </div>
                            @foreach ($educations as $item)
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-5 col-12">
                                        <label>School/University Name</label>
                                        <input type="text" class="form-control" value="{{$item->place}}" required="">
                                    </div>
                                    <div class="form-group col-md-3 col-12">
                                        <label>Program Study</label>
                                        <input type="text" class="form-control" value="{{$item->program_study}}" required="">
                                    </div>
                                    <div class="form-group col-md-2 col-12">
                                        <label>Degree</label>
                                        <input type="text" class="form-control" value="{{$item->degree}}" required="">
                                    </div>
                                    <div class="form-group col-md-1 col-12">
                                        <label>Year In</label>
                                        <input type="text" class="form-control" value="{{$item->date_start}}" required="">
                                    </div>
                                    <div class="form-group col-md-1 col-12">
                                        <label>Year Out</label>
                                        <input type="text" class="form-control" value="{{$item->date_end}}" required="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>description</label>
                                        <textarea class="form-control"
                                            style="height: 200px;">{{$item->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            @endforeach

                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <form method="post" class="needs-validation" novalidate="">
                            <div class="card-header">
                                <h4>Experience Section</h4>
                            </div>
                            @foreach ($experiences as $item)
                            <div class="card-body">
                                <div class="row">
                                    <div class="form-group col-md-6 col-12">
                                        <label>Work</label>
                                        <input type="text" class="form-control" value="{{$item->work}}" required="">
                                    </div>
                                    <div class="form-group col-md-4 col-12">
                                        <label>Profession</label>
                                        <input type="text" class="form-control" value="{{$item->profession}}" required="">
                                    </div>
                                    <div class="form-group col-md-1 col-12">
                                        <label>Year In</label>
                                        <input type="text" class="form-control" value="{{$item->date_start}}" required="">
                                    </div>
                                    <div class="form-group col-md-1 col-12">
                                        <label>Year Out</label>
                                        <input type="text" class="form-control" value="{{$item->date_end}}" required="">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-12">
                                        <label>description</label>
                                        <textarea class="form-control"
                                            style="height: 200px;">{{$item->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            @endforeach
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop