@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
          <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Create Privacy Policy Content</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Privacy Policy</a></div>
          <div class="breadcrumb-item">Create Privacy Policy Content</div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Create Privacy Policy Content</h2>
        <p class="section-lead">
          On this page you can create a privacy policy content and fill in all fields.
        </p>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Write Your Content</h4>
              </div>
              <div class="card-body">
                  <form action="{{ route('privacyPolice.store') }}" method="post">
                      @csrf
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                          <div class="col-sm-12 col-md-7">
                              <input type="text" class="form-control" value="Privacy Policy" readonly>
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Content</label>
                          <div class="col-sm-12 col-md-7">
                              <textarea class="summernote-simple" name="text"></textarea>
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Location</label>
                          <div class="col-sm-12 col-md-7">
                              <input type="text" class="form-control" name="location">
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Phone</label>
                          <div class="col-sm-12 col-md-7">
                              <input type="number" class="form-control" name="phone">
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                          <div class="col-sm-12 col-md-7">
                              <button class="btn btn-primary">Create Content</button>
                          </div>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
