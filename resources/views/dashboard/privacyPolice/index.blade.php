@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Privacy Policy Content</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Privacy Policy Content</a></div>
                    {{--                    <div class="breadcrumb-item">DataTables</div>--}}
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">Privacy Policy Content
                    <div class="float-right">
                        <a href="{{ route('privacyPolice.create') }}" class="btn btn-primary">Create Privacy Policy Content</a>
                    </div>
                </h2>
                <p class="section-lead">
                    Privacy Policy Content will be display on the privacy police page
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Privacy Policy Content</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Content</th>
                                            <th>Location</th>
                                            <th>Phone</th>
                                            <th width="20%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($privacypolices as $index => $privacypolice)
                                            <tr>
                                                <td class="text-center">{{ $index+1 }}</td>
                                                <td>{{ \Illuminate\Support\Str::limit(strip_tags($privacypolice->text), '50', ' ...') }}</td>
                                                <td>{{ $privacypolice->location }}</td>
                                                <td>{{ $privacypolice->phone }}</td>
                                                <td>
                                                    <form action="{{ route('privacyPolice.destroy', $privacypolice->id) }}" method="post">
                                                        <a href="{{ route('privacyPolice.edit', $privacypolice->id) }}" class="btn btn-warning">Edit</a>

                                                        @csrf
                                                        @method('DELETE')

                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
