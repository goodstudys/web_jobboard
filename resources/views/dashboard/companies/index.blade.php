@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>List Companies</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">List Companies</a></div>
                    {{--                    <div class="breadcrumb-item">DataTables</div>--}}
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if ($message = Session::get('error'))
            <div class="alert alert-info">
                <p>{{ $message }}</p>
            </div>
        @endif
            <div class="section-body">
                <h2 class="section-title">List Companies
                </h2>
                <p class="section-lead">
                    List User Companies
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>List Companies</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Name</th>
                                            <th>Username</th>
                                            <th>Email</th>
                                            <th>Company</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($companies as $index => $company)
                                                <tr>
                                                    <td class="text-center">{{ $index+1 }}</td>
                                                    <td>{{ $company->name }}</td>
                                                    <td>{{ $company->username }}</td>
                                                    <td>{{ $company->email }}</td>
                                                    <td>{{ $company->company }}</td>
                                                    <td>
                                                        <a href="{{ route('company.showProfile', $company->id) }}" class="btn btn-info">View</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
