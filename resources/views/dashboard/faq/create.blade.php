@extends('layouts.dashboard')

@section('content')
<div class="main-content">
    <section class="section">
      <div class="section-header">
        <div class="section-header-back">
          <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Create Faq Content</h1>
        <div class="section-header-breadcrumb">
          <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
          <div class="breadcrumb-item"><a href="#">Faq Content</a></div>
          <div class="breadcrumb-item">Create Faq Content</div>
        </div>
      </div>

      <div class="section-body">
        <h2 class="section-title">Create Faq Content</h2>
        <p class="section-lead">
          On this page you can create a faq content and fill in all fields.
        </p>

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h4>Write Your Content</h4>
              </div>
              <div class="card-body">
                  <form action="{{ route('faq.store') }}" method="post">
                      @csrf
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                          <div class="col-sm-12 col-md-7">
                              <input type="text" class="form-control" name="title">
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description</label>
                          <div class="col-sm-12 col-md-7">
                            <textarea class="form-control" style="height: 200px;" name="description"></textarea>
                              {{-- <textarea class="summernote-simple" name="description"></textarea> --}}
                          </div>
                      </div>
                      <div class="form-group row mb-4">
                          <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                          <div class="col-sm-12 col-md-7">
                              <button class="btn btn-primary">Create Faq</button>
                          </div>
                      </div>
                  </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
@stop
