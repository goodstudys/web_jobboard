@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Category</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Category</a></div>
                    {{--                    <div class="breadcrumb-item">DataTables</div>--}}
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">Category
                    <div class="float-right">
                        <button class="btn btn-primary" id="modal-5">Create Category</button>
                    </div>
                </h2>
                <p class="section-lead">
                    Category will be needed for company to create job and for applicant search job more specific
                </p>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Category</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th width="25%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($categories as $index => $category)
                                                <tr>
                                                    <td class="text-center">{{ $index+1 }}</td>
                                                    <td>{{ $category->name }}</td>
                                                    @if ($category->isActive === 1)
                                                        <td><div class="badge badge-success">Aktif</div></td>
                                                    @else
                                                        <td><div class="badge badge-danger">Non Aktif</div></td>
                                                    @endif
                                                    <td>

                                                        <form action="{{ route('category.destroy', $category->id) }}" method="post">
                                                            <input type="button" class="btn btn-warning" data-toggle="modal" data-target="#editModal{{ $category->id }}" value="Edit">
                                                            @csrf
                                                            @method('DELETE')

                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                            @if ($category->isActive === 1)
                                                                <a href="{{ route('category.statusUpdate', $category->id) }}" class="btn btn-outline-danger">Non Aktif</a>
                                                            @else
                                                                <a href="{{ route('category.statusUpdate', $category->id) }}" class="btn btn-outline-success">Aktif</a>
                                                            @endif
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <form class="modal-part" id="modal-category-part" action="{{ route('category.store') }}" method="post">
            @csrf
            <div class="form-group">
                <label>Category</label>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Category name" name="name">
                </div>
            </div>
        </form>

        @foreach($categories as $category)
            <div class="modal fade" id="editModal{{ $category->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Edit Category</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form action="{{ route('category.update', $category->id) }}" method="post">
                            @csrf
                            @method('PUT')
                            <div class="modal-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Category name" name="name" value="{{ $category->name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Save changes</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
