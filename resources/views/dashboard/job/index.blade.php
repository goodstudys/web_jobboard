@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>List Job</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">List Job</a></div>
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            @if ($message = Session::get('error'))
                <div class="alert alert-info">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">List Job
                </h2>
                <p class="section-lead">
                    List Company Job
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>List Job</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Company</th>
                                            <th>Title</th>
                                            <th>Job Type</th>
                                            <th>Min Salary</th>
                                            <th>Max Salary</th>
                                            <th width="33%">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($jobs as $index => $job)
                                            <tr>
                                                <td class="text-center">{{ $index+1 }}</td>
                                                <td>{{ $job->company_name }}</td>
                                                <td>{{ $job->title }}</td>
                                                <td>{{ $job->type }}</td>
                                                <td>Rp. {{ number_format($job->min_salary) }}</td>
                                                <td>Rp. {{ number_format($job->max_salary) }}</td>
                                                <td>
                                                    <form action="{{ route('dashboard.jobs.destroy', $job->id) }}" method="post">
                                                        <a href="{{ route('job.show', $job->id) }}" class="btn btn-info">View</a>
                                                        <a href="{{ route('dashboard.jobs.recUpdateJob', $job->id) }}" class="btn btn-warning">Suggest Changing</a>

                                                        @csrf

                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
