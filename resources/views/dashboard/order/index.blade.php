@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Order Balance</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Order Balance</a></div>
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">Order Balance
                </h2>
                <p class="section-lead">
                    List order balance company
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Order</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Date</th>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>Company</th>
                                            <th>Total</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($orders as $index => $order)
                                            <tr>
                                                <td class="text-center">{{ $index+1 }}</td>
                                                <td>{{ $order->date_created }}</td>
                                                <td>{{ $order->bill_first_name }}</td>
                                                <td>{{ $order->bill_last_name }}</td>
                                                <td>{{ $order->bill_company_name }}</td>
                                                <td>Rp. {{ number_format($order->total) }}</td>
                                                @if ($order->status == 'PENDING')
                                                    <td><div class="badge badge-warning">{{ $order->status }}</div></td>
                                                @elseif ($order->status == 'SUCCESS')
                                                    <td><div class="badge badge-success">{{ $order->status }}</div></td>
                                                @elseif ($order->status == 'CANCEL')
                                                    <td><div class="badge badge-danger">{{ $order->status }}</div></td>
                                                @endif
                                                <td>
                                                    @if ($order->status == 'PENDING')
                                                        <a href="{{ route('order.show', $order->id) }}" class="btn btn-info">View</a>
                                                        <a href="{{ route('order.accept', $order->id) }}" class="btn btn-success">Accept</a>
                                                        <a href="{{ route('order.cancel', $order->id) }}" class="btn btn-danger">Reject</a>
                                                    @elseif ($order->status == 'SUCCESS' || $order->status == 'CANCEL')
                                                        <a href="{{ route('order.show', $order->id) }}" class="btn btn-info">View</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
