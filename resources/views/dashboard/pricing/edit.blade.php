@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <div class="section-header-back">
                    <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
                </div>
                <h1>Edit Pricing</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Pricing</a></div>
                    <div class="breadcrumb-item">Edit Pricing</div>
                </div>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="section-body">
                <h2 class="section-title">Edit Pricing</h2>
                <p class="section-lead">
                    On this page you can update a pricing content and fill in all fields.
                </p>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Update Your Pricing</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('pricing.update', $pricing->id) }}" method="post">
                                    @csrf
                                    @method('PUT')
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" name="name" value="{{ $pricing->name }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Balance</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" name="balance" value="{{ $pricing->balance }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Price</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" class="form-control" name="price" value="{{ $pricing->price }}">
                                        </div>
                                    </div>
                                    <div id="buildyourform">
                                        @foreach($features as $key => $feature)
                                            <div class="form-group row mb-4">
                                                @if ($key == 0)
                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Feature</label>
                                                @else
                                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                                @endif
                                                <div class="col-sm-12 col-md-6">
                                                    <input type="text" class="form-control" name="features[]" value="{{ $feature->name }}">
                                                    <input type="hidden" class="form-control" name="ids[]" value="{{ $feature->id }}" readonly="">
                                                </div>
                                                <div class="col-md-1">
                                                    @if ($key == 0)
                                                        <input type="button" value="Add" class="add btn btn-primary btn-md" id="add" />
                                                    @else
                                                        <input type="button" value="Delete" class="remove btn btn-danger btn-md"/>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Update Pricing</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('extra_scripts')
    <script>
        $(document).ready(function() {
            $("#add").click(function() {
                var lastField = $("#buildyourform div:last");
                var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
                var fieldWrapper = $("<div class=\"form-group row mb-4\"/>");
                fieldWrapper.data("idx", intId);
                var fLabel = $("<label class=\"col-form-label text-md-right col-12 col-md-3 col-lg-3\"></label>");
                var fInput = $("                                            <div class=\"col-sm-12 col-md-6\">\n" +
                    "                                                <input type=\"text\" class=\"form-control\" name=\"features[]\">\n" +
                    "                                            </div>");
                var removeButton = $("                                        <div class=\"col-md-1\">\n" +
                    "                                            <input type=\"button\" value=\"Delete\" class=\"remove btn btn-danger btn-md\"/>\n" +
                    "                                        </div>");
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper.append(fLabel);
                fieldWrapper.append(fInput);
                fieldWrapper.append(removeButton);
                $("#buildyourform").append(fieldWrapper);
            });

            $(".remove").click(function() {
                $(this).parent().parent().remove();
            });
        });
    </script>
@stop
