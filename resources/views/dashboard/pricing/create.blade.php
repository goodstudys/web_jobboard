@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <div class="section-header-back">
                    <a href="features-posts.html" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
                </div>
                <h1>Create New Pricing</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Pricing</a></div>
                    <div class="breadcrumb-item">Create New Pricing</div>
                </div>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="section-body">
                <h2 class="section-title">Create New Pricing</h2>
                <p class="section-lead">
                    On this page you can create a new pricing content and fill in all fields.
                </p>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Write Your New Pricing</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('pricing.store') }}" method="post">
                                    @csrf
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Name</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" name="name">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Balance</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" class="form-control" name="balance">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Price</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" class="form-control" name="price">
                                        </div>
                                    </div>
                                    <div id="buildyourform">
                                        <div class="form-group row mb-4">
                                            <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Feature</label>
                                            <div class="col-sm-12 col-md-6">
                                                <input type="text" class="form-control" name="features[]">
                                            </div>
                                            <div class="col-md-1">
                                                <input type="button" value="Add" class="add btn btn-primary btn-md" id="add" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Create Pricing</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@section('extra_scripts')
    <script>
        $(document).ready(function() {
            $("#add").click(function() {
                var lastField = $("#buildyourform div:last");
                var intId = (lastField && lastField.length && lastField.data("idx") + 1) || 1;
                var fieldWrapper = $("<div class=\"form-group row mb-4\"/>");
                fieldWrapper.data("idx", intId);
                var fLabel = $("<label class=\"col-form-label text-md-right col-12 col-md-3 col-lg-3\"></label>");
                var fInput = $("                                            <div class=\"col-sm-12 col-md-6\">\n" +
                    "                                                <input type=\"text\" class=\"form-control\" name=\"features[]\">\n" +
                    "                                            </div>");
                var removeButton = $("                                        <div class=\"col-md-1\">\n" +
                    "                                            <input type=\"button\" value=\"Delete\" class=\"remove btn btn-danger btn-md\"/>\n" +
                    "                                        </div>");
                removeButton.click(function() {
                    $(this).parent().remove();
                });
                fieldWrapper.append(fLabel);
                fieldWrapper.append(fInput);
                fieldWrapper.append(removeButton);
                $("#buildyourform").append(fieldWrapper);
            });
        });
    </script>
@stop
