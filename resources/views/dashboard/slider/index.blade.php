@extends('layouts.dashboard')

@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Slider</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="#">Dashboard</a></div>
                    <div class="breadcrumb-item"><a href="#">Slider</a></div>
{{--                    <div class="breadcrumb-item">DataTables</div>--}}
                </div>
            </div>

            @if ($message = Session::get('success'))
                <div class="alert alert-success">
                    <p>{{ $message }}</p>
                </div>
            @endif
            <div class="section-body">
                <h2 class="section-title">Slider
                    <div class="float-right">
                        <a href="{{ route('sliders.create') }}" class="btn btn-primary">Create Slider</a>
                    </div>
                </h2>
                <p class="section-lead">
                    Slider will be display on the top of the website index
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Slider</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-1">
                                        <thead>
                                        <tr>
                                            <th class="text-center" width="8%">
                                                #
                                            </th>
                                            <th>Title</th>
                                            <th width="50%">Subtitle</th>
                                            <th>Image</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($sliders as $index => $slider)
                                                <tr>
                                                    <td class="text-center">{{ $index+1 }}</td>
                                                    <td>{{ $slider->title }}</td>
                                                    <td>{{ $slider->subtitle }}</td>
                                                    <td><img src="{{ asset('upload/sliders/'. $slider->image) }}" alt="" style="width: 100%; max-width: 100px; height: auto;"></td>
                                                    <td>
                                                        <form action="{{ route('sliders.destroy', $slider->id) }}" method="post">
                                                            <a href="{{ route('sliders.edit', $slider->id) }}" class="btn btn-warning">Edit</a>

                                                            @csrf
                                                            @method('DELETE')

                                                            <button type="submit" class="btn btn-danger">Delete</button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
