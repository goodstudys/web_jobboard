@extends('layouts.main')

@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>register</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">pages</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of Login - Register Section ===== -->
<section class="ptb80" id="register">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Start of Nav Tabs -->
                <ul class="nav nav-tabs" role="tablist">

                    <!-- Personal Account Tab -->
                    <li role="presentation" class="active">
                        <a href="#personal" aria-controls="personal" role="tab" data-toggle="tab" aria-expanded="true">
                            <h6>Personal Account</h6>
                            <span>I'm looking for a job</span>
                        </a>
                    </li>

                    <!-- Company Account Tab -->
                    <li role="presentation" class="">
                        <a href="#company" aria-controls="company" role="tab" data-toggle="tab" aria-expanded="false">
                            <h6>Company Account</h6>
                            <span>We are hiring</span>
                        </a>
                    </li>
                </ul>
                <!-- End of Nav Tabs -->



                <!-- Start of Tab Content -->
                <div class="tab-content ptb60">

                    <!-- Start of Tabpanel for Personal Account -->
                    <div role="tabpanel" class="tab-pane active" id="personal">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form action="{{Route('register')}}" method="POST">
                                    @csrf
                                    <input type="text" name="level" style="display: none" value="5">
                                    <input type="text" name="type" style="display: none" value="nonmodal">
                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" name="name" required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username" required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="email" class="form-control"
                                            pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" name="email"
                                            required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" onkeyup='check()' id="password"
                                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                            name="password" required>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group mb30">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" id="confirm_password"
                                            onkeyup='check()' pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                            name="password_confirmation" required>
                                        <input type="text" name="zz" id="matchpass" style="display: none" required>
                                        <span id='message'></span>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group text-center">
                                        <input type="checkbox" id="agree" name="agree" required>
                                        <label for="agree">Agree with the <a href="#">Terms and Conditions</a></label>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group text-center nomargin">
                                        <button type="submit"
                                            onclick="if(!this.form.agree.checked){alert('You must agree to the terms first.');return false}"
                                            class="btn btn-blue btn-effect">create account</button>
                                    </div>
                                </form>


                            </div>
                        </div>
                    </div>
                    <!-- End of Tabpanel for Personal Account -->

                    <!-- Start of Tabpanel for Company Account -->
                    <div role="tabpanel" class="tab-pane" id="company">
                        <form action="{{Route('register')}}" method="post">
                            @csrf
                            <input type="text" name="level" style="display: none" value="3">
                            <div class="row">

                                <!-- Start of the First Column -->
                                <div class="col-md-6">

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" class="form-control" name="username" required>
                                        @error('username')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>E-mail</label>
                                        <input type="email" class="form-control"
                                            pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}" name="email"
                                            required>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" id="passwordd" onkeyup="checkcek()"
                                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
                                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                            name="password" required>

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" id="confirm_passwordd"
                                            pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" onkeyup="checkcek()"
                                            title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
                                            name="password_confirmation" required>
                                        <input type="text" name="zz" id="matchpassd" style="display: none" required>
                                        <span id='messaged'></span>
                                        @error('password_confirmation')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <!-- End of the First Column -->

                                <!-- Start of the Second Column -->
                                <div class="col-md-6">

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" name="name" required>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Company Name</label>
                                        <input type="text" class="form-control" name="companyName" required>
                                        @error('companyName')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Website</label>
                                        <input type="text" class="form-control" name="website" required>
                                        @error('website')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <label>Address</label>
                                        <input type="text" class="form-control" name="address" required>
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <!-- End of the Second Column -->
                            </div>

                            <div class="row mt20">
                                <div class="col-md-12 text-center">

                                    <!-- Form Group -->
                                    <div class="form-group">
                                        <input type="checkbox" id="agree2" name="agree2" required>
                                        <label for="agree2">Agree with the <a href="#">Terms and Conditions</a></label>
                                    </div>

                                    <!-- Form Group -->
                                    <div class="form-group nomargin">
                                        <button type="submit"
                                            onclick="if(!this.form.agree2.checked){alert('You must agree to the terms first.');return false}"
                                            class="btn btn-blue btn-effect">create account</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- End of Tabpanel for Company Account -->

                </div>
                <!-- End of Tab Content -->

            </div>
        </div>
    </div>
</section>
<!-- ===== End of Login - Register Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@endsection
@section('extra_scripts')
<script>
    var check = function() {
  if (document.getElementById('password').value ==
    document.getElementById('confirm_password').value) {
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'matching';
    document.getElementById('matchpass').value = 'matching';
  } else {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'not matching';
    document.getElementById('matchpass').value = '';
  }
}
var checkcek = function() {
  if (document.getElementById('passwordd').value ==
    document.getElementById('confirm_passwordd').value) {
    document.getElementById('messaged').style.color = 'green';
    document.getElementById('messaged').innerHTML = 'matching';
    document.getElementById('matchpassd').value = 'matching';
  } else {
    document.getElementById('messaged').style.color = 'red';
    document.getElementById('messaged').innerHTML = 'not matching';
    document.getElementById('matchpassd').value = '';
  }
}
</script>
@stop