@extends('layouts.main')

@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>login</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="{{route('home.index')}}">home</a></li>
                    <li class="active">Login</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of Login - Register Section ===== -->
<section class="ptb80" id="login">
    <div class="container">
        <div class="col-md-6 col-md-offset-3 col-xs-12">

            <!-- Start of Login Box -->
            <div class="login-box">

                <div class="login-title">
                    <h4>login to Simplyyours</h4>
                </div>

                <!-- Start of Login Form -->
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <!-- Form Group -->
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                            placeholder="Your Email" name="email" value="{{ old('email') }}" required
                            autocomplete="email" autofocus>

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input id="password" type="password"
                            class="form-control @error('password') is-invalid @enderror" placeholder="Your Password"
                            name="password" required autocomplete="current-password">

                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <!-- Form Group -->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-6">

                                <input type="checkbox" name="remember" id="remember-me2"
                                    {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember-me2">Remember me?</label>

                            </div>

                            @if (Route::has('password.request'))
                            <div class="col-xs-6 text-right">
                                <a href="{{ route('password.request') }}">Forgot password?</a>
                            </div>
                            @endif
                        </div>
                    </div>

                    <!-- Form Group -->
                    <div class="form-group text-center">
                        <button class="btn btn-blue btn-effect">Login</button>
                        <a href="{{route('register')}}" class="btn btn-blue btn-effect">signup</a>
                    </div>

                </form>
                <!-- End of Login Form -->
            </div>
            <!-- End of Login Box -->

        </div>
    </div>
</section>
<!-- ===== End of Login - Register Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@endsection
