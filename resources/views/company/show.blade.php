@extends('layouts.main')
@section('content')
<!-- ===== Start of Candidate Profile Header Section ===== -->
<section class="profile-header">
</section>
<!-- ===== End of Candidate Header Section ===== -->





<!-- ===== Start of Main Wrapper Company Profile Section ===== -->
<section class="pb80" id="company-profile">
    <div class="container">
        @if ($check == 0)
        <div class="company-profile">
            <div class="text-center">
                <a href="{{ route('profile.create') }}" class="btn btn-blue btn-effect">Create your profile</a>
            </div>
        </div>
        @else
        <!-- Start of Row -->
        <div class="row company-profile">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            @if (\Illuminate\Support\Facades\Auth::check())
            @if (\Illuminate\Support\Facades\Auth::user()->level == 3)
            <div class="text-right">
                {{--                        <a href="{{ route('advert.create') }}" class="btn
                btn-primary">Advertisement</a>--}}
                <a href="{{ route('profile.edit', $data->id) }}" class="btn btn-primary">Edit Profile</a>
            </div>
            @endif
            @endif
            <!-- Start of Profile Picture -->
            <div class="col-md-3 col-xs-12">
                <div class="profile-photo">
                    <img src="{{ asset('upload/users/company/'.$data->image) }}" class="img-responsive" alt="">
                </div>

                <!-- Start of Social Media Buttons -->
                <ul class="social-btns list-inline text-center mt20">
                    <!-- Social Media -->
                    @if ($data->facebook != null)
                    <li>
                        <a href="{{ $data->facebook }}" class="social-btn-roll facebook transparent">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-facebook"></i>
                                <i class="social-btn-roll-icon fa fa-facebook"></i>
                            </div>
                        </a>
                    </li>
                    @endif

                    <!-- Social Media -->
                    @if ($data->twitter != null)
                    <li>
                        <a href="{{ $data->twitter }}" class="social-btn-roll twitter transparent">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-twitter"></i>
                                <i class="social-btn-roll-icon fa fa-twitter"></i>
                            </div>
                        </a>
                    </li>
                    @endif

                    <!-- Social Media -->
                    @if ($data->google != null)
                    <li>
                        <a href="{{ $data->google }}" class="social-btn-roll google-plus transparent">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-google-plus"></i>
                                <i class="social-btn-roll-icon fa fa-google-plus"></i>
                            </div>
                        </a>
                    </li>
                    @endif

                    <!-- Social Media -->
                    @if ($data->instagram != null)
                    <li>
                        <a href="{{ $data->instagram }}" class="social-btn-roll instagram transparent">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-instagram"></i>
                                <i class="social-btn-roll-icon fa fa-instagram"></i>
                            </div>
                        </a>
                    </li>
                    @endif

                    <!-- Social Media -->
                    @if ($data->linkedin != null)
                    <li>
                        <a href="{{ $data->linkedin }}" class="social-btn-roll linkedin transparent">
                            <div class="social-btn-roll-icons">
                                <i class="social-btn-roll-icon fa fa-linkedin"></i>
                                <i class="social-btn-roll-icon fa fa-linkedin"></i>
                            </div>
                        </a>
                    </li>
                    @endif

                </ul>
                <!-- End of Social Media Buttons -->

            </div>
            <!-- End of Profile Picture -->

            <!-- Start of Profile Description -->
            <div class="col-md-9 col-xs-12">
                <div class="profile-descr">

                    <!-- Profile Title -->
                    <div class="profile-title">
                        <h2 class="capitalize">{{ $data->name }}</h2>
                        <h5 class="pt10">{{ $data->type }}</h5>
                    </div>

                    <!-- Profile Details -->
                    <div class="profile-details mt20">
                        {!! preg_replace('#<script(.*?)>(.*?)</script(.*?)>#is', '', $data->description) !!}
                    </div>

                    <ul class="profile-info mt20 nopadding">
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <span>{{ $data->address }}</span>
                        </li>

                        @if ($data->website != null)
                        <li>
                            <i class="fa fa-globe"></i>
                            <a href="#">{{ $data->website }}</a>
                        </li>
                        @endif

                        <li>
                            <i class="fa fa-phone"></i>
                            <span>{{ $data->phone }}</span>
                        </li>
                    </ul>

                </div>
            </div>
            <!-- End of Profile Description -->

        </div>
        <!-- End of Row -->
        @endif
    </div>
</section>
<!-- ===== End of Company Profile Section ===== -->




@if ($check != 0)
<!-- ===== Start of Related Company Jobs Section ===== -->
<section class="company-jobs ptb80">
    <div class="container">
        @if ($jobCheck == 0)
        <div class="text-center">
            <a href="{{ route('job.create') }}" class="btn btn-blue btn-effect">Post Your Job Now</a>
        </div>
        @else
        <div class="row">
            <div class="col-md-12 text-center">
                <h3 class="pb60">Our Latest Jobs</h3>
            </div>
        </div>

        @foreach ($jobs as $item)
        <!-- ===== Start of Single Job Post 1 ===== -->
        <div class="single-job-post row shadow-hover">
            <!-- Job Company -->
            <div class="col-md-2 col-xs-3 nopadding">
                <div class="job-company">
                    <a href="#">
                        @if ($item->company_logo == null)
                        <img src="{{ asset('upload/users/company/'.$data->image) }}" width="40" alt="">
                        @else
                        <img src="{{asset('/upload/users/company/job/logo/'.$item->company_logo)}}" width="40" alt="">
                        @endif
                    </a>
                </div>
            </div>

            <!-- Job Title & Info -->
            @if ($item->is_edit == 1)
                <div class="col-md-6 col-xs-6 ptb20">
            @else
                <div class="col-md-8 col-xs-6 ptb20">
            @endif
                <div class="job-title">
                    <a href="{{route('job.show',$item->id)}}">{{$item->title}}</a>
                </div>
            </div>

            <!-- Job Category -->
            @if ($item->is_edit == 1)
                 <div class="col-md-1 col-xs-3 ptb30">
            @else
                 <div class="col-md-2 col-xs-3 ptb30">
            @endif
                <div class="job-category">
                    @if ($item->type == 'Full Time')
                    <a href="javascript:void(0)" class="btn btn-green btn-small btn-effect">full time</a>
                    @elseif ($item->type =="Part Time")
                    <a href="javascript:void(0)" class="btn btn-purple btn-small btn-effect">part time</a>
                    @elseif ($item->type =="Freelance")
                    <a href="javascript:void(0)" class="btn btn-blue btn-small btn-effect">freelancer</a>
                    @elseif ($item->type =="Internship")
                    <a href="javascript:void(0)" class="btn btn-orange btn-small btn-effect">intership</a>
                    @elseif ($item->type =="Temporary")
                    <a href="javascript:void(0)" class="btn btn-red btn-small btn-effect">temporary</a>
                    @endif

                </div>
            </div>

            @if ($item->is_edit == '1')
                <div class="col-md-3 col-xs-3 ptb30">
                    <div class="job-category">
                        <a href="javascript:void(0)" class="btn btn-orange btn-small btn-effect">recommended change</a>
                    </div>
                </div>
            @endif
        </div>
        <!-- ===== End of Single Job Post 1 ===== -->
        @endforeach

        <div class="row mt30">
            @if (count($jobs) >= 10)
            <div class="col-md-12 text-center">
                <a href="{{route('searchJob.index')}}" class="btn btn-blue btn-effect">show all</a>
            </div>
            @endif
        </div>
        @endif

    </div>
</section>
<!-- ===== End of Related Company Jobs Section ===== -->
@endif





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@stop