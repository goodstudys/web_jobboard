@extends('layouts.main')
@section('content')
<!-- =============== Start of Page Header 1 Section =============== -->
<section class="page-header">
    <div class="container">

        <!-- Start of Page Title -->
        <div class="row">
            <div class="col-md-12">
                <h2>companies</h2>
            </div>
        </div>
        <!-- End of Page Title -->

        <!-- Start of Breadcrumb -->
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">home</a></li>
                    <li class="active">companies</li>
                </ul>
            </div>
        </div>
        <!-- End of Breadcrumb -->

    </div>
</section>
<!-- =============== End of Page Header 1 Section =============== -->





<!-- ===== Start of Main Wrapper Companies Section ===== -->
<section class="ptb80" id="companies">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Start of Company Letters -->
                <div class="company-letters">
                    <a href="#A">A</a><a href="#B">B</a>
                    <a href="#C">C</a>
                    <a href="#D">D</a>
                    <a href="#E">E</a>
                    <a href="#F">F</a>
                    <a href="#G">G</a>
                    <a href="#H">H</a>
                    <a href="#I">I</a>
                    <a href="#J">J</a>
                    <a href="#K">K</a>
                    <a href="#L">L</a>
                    <a href="#M">M</a>
                    <a href="#N">N</a>
                    <a href="#O">O</a>
                    <a href="#P">P</a>
                    <a href="#Q">Q</a>
                    <a href="#R">R</a>
                    <a href="#S">S</a>
                    <a href="#T">T</a>
                    <a href="#U">U</a>
                    <a href="#V">V</a>
                    <a href="#W">W</a>
                    <a href="#X">X</a>
                    <a href="#Y">Y</a>
                    <a href="#Z">Z</a>
                </div>
                <!-- End of Company Letters -->

                <!-- Start of Companies -->
                <div class="row companies-grid mt60">

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="A" class="company-letter">
                            <h4>A</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'a' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="B" class="company-letter">
                            <h4>B</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'b' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="C" class="company-letter">
                            <h4>C</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'c' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="D" class="company-letter">
                            <h4>D</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'd' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="E" class="company-letter">
                            <h4>E</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'e' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="F" class="company-letter">
                            <h4>F</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'f' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="G" class="company-letter">
                            <h4>G</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'g' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="H" class="company-letter">
                            <h4>H</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'h' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="I" class="company-letter">
                            <h4>I</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'i' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="J" class="company-letter">
                            <h4>J</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'j' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="K" class="company-letter">
                            <h4>K</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'k' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="L" class="company-letter">
                            <h4>L</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'l' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="M" class="company-letter">
                            <h4>M</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'm' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="N" class="company-letter">
                            <h4>N</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'n' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="O" class="company-letter">
                            <h4>O</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">

                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'o' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="P" class="company-letter">
                            <h4>P</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'p' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="Q" class="company-letter">
                            <h4>Q</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'q' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="R" class="company-letter">
                            <h4>R</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'r' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="S" class="company-letter">
                            <h4>S</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 's' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="T" class="company-letter">
                            <h4>T</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 't' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="U" class="company-letter">
                            <h4>U</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'u' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="V" class="company-letter">
                            <h4>V</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'v' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="W" class="company-letter">
                            <h4>W</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'w' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="X" class="company-letter">
                            <h4>X</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'x' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="Y" class="company-letter">
                            <h4>Y</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'y' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                    <!-- Company Group -->
                    <div class="col-md-4 col-sm-6 col-xs-12 company-group">
                        <div id="Z" class="company-letter">
                            <h4>Z</h4>
                        </div>

                        <!-- Companies -->
                        <ul class="nopadding">
                            @foreach ($companies as $item)
                            @if (strtolower($item->name[0]) == 'z' )
                            <li class="company-name">
                                <a href="{{ route('company.show', $item->user_id) }}">{{$item->name}}
                                    ({{count($item->companyJob)}})</a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>

                </div>
                <!-- End of Companies -->

            </div>
        </div>
    </div>
</section>
<!-- ===== End of Main Wrapper Companies Section ===== -->





<!-- ===== Start of Get Started Section ===== -->
<section class="get-started ptb40">
    <div class="container">
        <div class="row ">

            <!-- Column -->
            <div class="col-md-10 col-sm-9 col-xs-12">
                <h3 class="text-white">20,000+ People trust Simplyyours! Be one of them today.</h3>
            </div>

            <!-- Column -->
            <div class="col-md-2 col-sm-3 col-xs-12">
                <a href="{{route('login')}}" class="btn btn-blue btn-effect">get start now</a>
            </div>

        </div>
    </div>
</section>
<!-- ===== End of Get Started Section ===== -->
@stop
@section('extra_scripts')
<script>
    $('#modal_trigger').on('click', function() {
    // Find which button was clicked, so we know which tab to target
    var tabTarget = $(this).data('tab');

    // Manually show the modal, since we are not doing it via data-toggle now
   // $('.cd-user-modal').modal('show');

    // Now show the selected tab
    $('#cd-login').tab('show');
    });
    @error('email')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror

    @error('password')
        $(document).ready(function(){
            $('#modalCoupon').modal({
            show: 'true'
            });
        });
    @enderror
</script>
@stop