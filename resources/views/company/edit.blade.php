@extends('layouts.main')
@section('content')
    <!-- ===== Start of Main Wrapper Section ===== -->
    <section class="ptb80" id="post-job">
        <div class="container">

            <h3 class="uppercase text-blue">Edit Company Profile</h3>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
        @endif

        <!-- Start of Post Resume Form -->
            <form action="{{ route('profile.update', $company->id) }}" enctype="multipart/form-data" method="post" class="post-job-resume mt50">
            @csrf
            <!-- Start of Resume Details -->
                <div class="row">
                    <div class="col-md-12">

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>company name</label>
                            <input class="form-control" type="text" name="name" value="{{ $company->name }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>company type</label>
                            <input class="form-control" type="text" name="type" placeholder='e.g. "Marketplace"' value="{{ $company->type }}" required>
                        </div>

                        <!-- Form Group -->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>company logo</label>

                                    <!-- Upload Button -->
                                    <div class="upload-file-btn">
                                        <span><i class="fa fa-upload"></i> Upload</span>
                                        <input type="file" name="image" accept=".jpg,.png,.gif" id="imgInp">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <img id="blah" src="{{ asset('images/defaultpreview.png') }}" alt="your image" width="200px"/>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>current logo</label>

                                    <div class="row">
                                        <div class="col-md-8 col-xs-12">
                                            <img src="{{ asset('upload/users/company/'.$company->image) }}" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>description</label>
                            <textarea class="tinymce" name="description">{{ $company->description }}</textarea>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>location</label>
                            <input class="form-control" type="text" placeholder='e.g. "Surabaya, Indonesia"' name="address" value="{{ $company->address }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>website <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "www.mycompany.com"' name="website" value="{{ $company->website }}">
                            <span class="form-msg">Leave this blank if the website is not important.</span>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>phone</label>
                            <input class="form-control" type="text" name="phone" value="{{ $company->phone }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>facebook <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "web.facebook.com/mycompany"' name="facebook" value="{{ $company->facebook }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>twitter <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "twitter.com/mycompany"' name="twitter" value="{{ $company->twitter }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>google <span>(optional)</span></label>
                            <input class="form-control" type="email" placeholder='e.g. "mycompany@gmail.com"' name="google" value="{{ $company->google }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>instragram <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "www.instagram.com/mycompany"' name="instagram" value="{{ $company->instagram }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group">
                            <label>linkedin <span>(optional)</span></label>
                            <input class="form-control" type="text" placeholder='e.g. "www.linkedin.com/company/mycompany"' name="linkedin" value="{{ $company->linkedin }}">
                        </div>

                        <!-- Form Group -->
                        <div class="form-group pt30 nomargin" id="last">
                            <button class="btn btn-blue btn-effect">submit</button>
                        </div>

                    </div>
                </div>
                <!-- End of Resume Details -->

            </form>
            <!-- End of Post Resume Form -->

        </div>
    </section>
    <!-- ===== End of Main Wrapper Section ===== -->
@stop

@section('extra_scripts')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imgInp").change(function() {
            readURL(this);
        });
    </script>
@stop
