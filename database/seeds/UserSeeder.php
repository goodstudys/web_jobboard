<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'manager',
                'username' => 'manager',
                'level' => 1,
                'company' => null,
                'website' => null,
                'address' => null,
                'email' => 'manager@goodnews.id',
                'password' => Hash::make('manager'),
                'isCompany' => 0
            ],
            [
                'name' => 'admin',
                'username' => 'admin',
                'level' => 2,
                'company' => null,
                'website' => null,
                'address' => null,
                'email' => 'admin@goodnews.id',
                'password' => Hash::make('admin'),
                'isCompany' => 0
            ],
            [
                'name' => 'company',
                'username' => 'company',
                'level' => 3,
                'company' => 'MyCompany',
                'website' => 'www.mycompany.com',
                'address' => 'Surabaya',
                'email' => 'company@goodnews.id',
                'password' => Hash::make('company'),
                'isCompany' => 1
            ],
            [
                'name' => 'trainer',
                'username' => 'trainer',
                'level' => 4,
                'company' => 'MyCompany',
                'website' => 'www.mycompany.com',
                'address' => 'Surabaya',
                'email' => 'trainer@goodnews.id',
                'password' => Hash::make('trainer'),
                'isCompany' => 1
            ],
            [
                'name' => 'user',
                'username' => 'user',
                'level' => 5,
                'company' => null,
                'website' => null,
                'address' => null,
                'email' => 'user@goodnews.id',
                'password' => Hash::make('user'),
                'isCompany' => 0
            ]
        ]);
    }
}
