<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('order_code');
            $table->string('bill_country');
            $table->string('bill_first_name');
            $table->string('bill_last_name');
            $table->string('bill_company_name')->nullable();
            $table->string('bill_address');
            $table->string('bill_city');
            $table->string('ship_country')->nullable();
            $table->string('ship_first_name')->nullable();
            $table->string('ship_last_name')->nullable();
            $table->string('ship_company_name')->nullable();
            $table->string('ship_address')->nullable();
            $table->string('ship_city')->nullable();
            $table->integer('quantity');
            $table->string('subtotal');
            $table->string('shipping');
            $table->string('discount')->nullable();
            $table->string('total');
            $table->string('payment')->nullable();
            $table->string('status')->nullable();
            $table->dateTime('date_created');
            $table->integer('is_balance')->nullable();
            $table->string('rowPointer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
