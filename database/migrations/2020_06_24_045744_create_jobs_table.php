<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('email');
            $table->string('title');
            $table->string('location')->nullable();
            $table->string('type');
            $table->integer('category_id');
            $table->string('tags')->nullable();
            $table->text('description')->nullable();
            $table->string('applicant_email_or_website');
            $table->string('min_rate')->nullable();
            $table->string('max_rate')->nullable();
            $table->string('min_salary')->nullable();
            $table->string('max_salary')->nullable();
            $table->string('ext_link')->nullable();
            $table->string('header_image')->nullable();
            $table->string('company_ext_link')->nullable();
            $table->string('company_name')->nullable();
            $table->string('company_website')->nullable();
            $table->string('company_tagline')->nullable();
            $table->string('company_video')->nullable();
            $table->string('company_twitter')->nullable();
            $table->string('company_logo')->nullable();
            $table->string('rowPointer');
            $table->integer('is_edit');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
